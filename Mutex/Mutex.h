#ifndef OSEFMUTEX_H
#define OSEFMUTEX_H

#include <pthread.h>

/// POSIX mutex façade provides simple creation and access
namespace OSEF
{
    class Mutex
    {
    public:
        Mutex();
        virtual ~Mutex();

        bool lock();
        bool unlock();

        Mutex(const Mutex&) = delete;  // copy constructor
        Mutex& operator=(const Mutex&) = delete;  // copy assignment
        Mutex(Mutex&&) = delete;  // move constructor
        Mutex& operator=(Mutex&&) = delete;  // move assignment

    private:
        bool initialize();

        pthread_mutex_t ptmutex;  ///< POSIX mutex descriptor
        bool initialized{false};
    };
}  // namespace OSEF

#endif /* OSEFMUTEX_H */
