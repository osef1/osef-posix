#include "Mutex.h"
#include "Debug.h"

/// initializes mutex descriptor
OSEF::Mutex::Mutex()
    :ptmutex() {}

OSEF::Mutex::~Mutex()
{
    if (initialized)
    {
        if (pthread_mutex_trylock(&ptmutex) != 0)
        {
            DERR("mutex was locked before destruction");
        }

        if (not unlock())
        {
            DERR("error unlocking mutex");
        }

        if (pthread_mutex_destroy(&ptmutex) != 0)
        {
            DERR("error destroying mutex");
        }
    }
}

bool OSEF::Mutex::initialize()
{
    if (not initialized)
    {
        pthread_mutexattr_t mutexAttribute;
        if (pthread_mutexattr_init(&mutexAttribute) == 0)
        {
            if (pthread_mutexattr_settype(&mutexAttribute, PTHREAD_MUTEX_ERRORCHECK) == 0)
            {
                if (pthread_mutex_init(&ptmutex, &mutexAttribute) == 0)
                {
                    initialized = true;
                }
                else
                {
                    DERR("error initializing mutex");
                }
            }
            else
            {
                DERR("error setting mutex type");
            }

            if (pthread_mutexattr_destroy(&mutexAttribute) != 0)
            {
                DERR("error destroying mutex attribute");
            }
        }
        else
        {
            DERR("error initializing mutex attribute");
        }
    }

    return initialized;
}

/// locks mutex and returns true upon success
bool OSEF::Mutex::lock()
{
    bool ret = false;

    if (initialize() && (pthread_mutex_lock(&ptmutex) == 0))
    {
        ret = true;
    }

    return ret;
}

/// unlocks mutex and returns true upon success
bool OSEF::Mutex::unlock()
{
    bool ret = false;

    if (initialize() && (pthread_mutex_unlock(&ptmutex) == 0))
    {
        ret = true;
    }

    return ret;
}
