#include "MutexP.h"
#include "Mutex.h"

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(NoLoop,
                             MutexP,
                             testing::Values(MutexParam({0})));

    INSTANTIATE_TEST_SUITE_P(DoubleLoop,
                             MutexP,
                             testing::Values(MutexParam({2})));

    INSTANTIATE_TEST_SUITE_P(LongLoop,
                             MutexP,
                             testing::Values(MutexParam({1000})));

    TEST_P(MutexP, doNothing)
    {
        OSEF::Mutex mutex;
    }

    TEST_P(MutexP, lockFirst)
    {
        OSEF::Mutex mutex;

        //  must succeed as mutex is initially unlocked
        EXPECT_TRUE(mutex.lock());
    }

    TEST_P(MutexP, unlockFirst)
    {
        OSEF::Mutex mutex;

        //  must fail as mutex is initially unlocked
        EXPECT_FALSE(mutex.unlock());
    }

    TEST_P(MutexP, nominal)
    {
        OSEF::Mutex mutex;

        //  lock/unlock loop must always succeed
        for (size_t i = 0; i < GetParam().loops; i++)
        {
            EXPECT_TRUE(mutex.lock());
            EXPECT_TRUE(mutex.unlock());
        }
    }

    TEST_P(MutexP, relock)
    {
        OSEF::Mutex mutex;

        EXPECT_TRUE(mutex.lock());

        //  locking an already locked mutex must always fail
        for (size_t i = 0; i < GetParam().loops; i++)
        {
            EXPECT_FALSE(mutex.lock());

            EXPECT_TRUE(mutex.unlock());
            EXPECT_TRUE(mutex.lock());
        }
    }

    TEST_P(MutexP, reunlock)
    {
        OSEF::Mutex mutex;

        // unlocking an already unlocked mutex must always fail
        for (size_t i = 0; i < GetParam().loops; i++)
        {
            EXPECT_FALSE(mutex.unlock());

            EXPECT_TRUE(mutex.lock());
            EXPECT_TRUE(mutex.unlock());
        }
    }
}  // namespace OSEF
