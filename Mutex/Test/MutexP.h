#ifndef OSEFMUTEXP_H
#define OSEFMUTEXP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        uint32_t loops;
    }MutexParam;

    class MutexP : public ::testing::TestWithParam<MutexParam>
    {
        protected:
            MutexP() {}
            ~MutexP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMUTEXP_H
