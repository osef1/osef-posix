#include "RandomizerP.h"
#include "Randomizer.h"

#include <math.h>  // fabs

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(million, RandomizerP, testing::Values(RandomizerTestParam({1000000, 0.0006, 0.0007})));

    uint32_t getOnes(const uint64_t& r)
    {
        uint32_t ones = 0;

        uint64_t i = r;

        while (i)
        {
            ones += i & 1;
            i >>= 1;
        }

        return ones;
    }

    TEST_P(RandomizerP, getUInt8)
    {
        Randomizer randomizer;

        size_t ones = 0;
        size_t samples = 0;

        do
        {
            ones += getOnes(randomizer.getUInt8());
            samples++;
        }while (samples < GetParam().sampleSize);

        const double monobitFrequency = static_cast<double>(ones) / (static_cast<double>(samples) * 8);
        EXPECT_GE(monobitFrequency, 0.5 - GetParam().monobitFrequencyPrecision);
        EXPECT_LE(monobitFrequency, 0.5 + GetParam().monobitFrequencyPrecision);
    }

    TEST_P(RandomizerP, getUInt16)
    {
        Randomizer randomizer;

        size_t ones = 0;
        size_t samples = 0;

        do
        {
            ones += getOnes(randomizer.getUInt16());
            samples++;
        }while (samples < GetParam().sampleSize);

        const double monobitFrequency = static_cast<double>(ones) / (static_cast<double>(samples) * 16);
        EXPECT_GE(monobitFrequency, 0.5 - GetParam().monobitFrequencyPrecision);
        EXPECT_LE(monobitFrequency, 0.5 + GetParam().monobitFrequencyPrecision);
    }

    TEST_P(RandomizerP, getUInt32)
    {
        Randomizer randomizer;

        size_t ones = 0;
        size_t samples = 0;

        do
        {
            ones += getOnes(randomizer.getUInt32());
            samples++;
        }while (samples < GetParam().sampleSize);

        const double monobitFrequency = static_cast<double>(ones) / (static_cast<double>(samples) * 32);
        EXPECT_GE(monobitFrequency, 0.5 - GetParam().monobitFrequencyPrecision);
        EXPECT_LE(monobitFrequency, 0.5 + GetParam().monobitFrequencyPrecision);
    }

    TEST_P(RandomizerP, getUInt64)
    {
        Randomizer randomizer;

        size_t ones = 0;
        size_t samples = 0;

        do
        {
            ones += getOnes(randomizer.getUInt64());
            samples++;
        }while (samples < GetParam().sampleSize);

        const double monobitFrequency = static_cast<double>(ones) / (static_cast<double>(samples) * 64);
        EXPECT_GE(monobitFrequency, 0.5 - GetParam().monobitFrequencyPrecision);
        EXPECT_LE(monobitFrequency, 0.5 + GetParam().monobitFrequencyPrecision);
    }
}  // namespace OSEF
