#ifndef OSEFRANDOMIZERP_H
#define OSEFRANDOMIZERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t sampleSize;
        double monobitFrequencyPrecision;
        double characterFrequencyPrecision;
    }RandomizerTestParam;

    class RandomizerP : public ::testing::TestWithParam<RandomizerTestParam>
    {
        protected:
            RandomizerP() {}
            ~RandomizerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFRANDOMIZERP_H
