#include "Randomizer.h"
#include "Debug.h"

#include <cstdlib>  // srandom
#include <ctime>  // time

OSEF::Randomizer::Randomizer()
{
    if (not seed())
    {
        DERR("Failed to seed randomizer");
    }
}

bool OSEF::Randomizer::seed() const
{
    bool ret = false;

    timespec ts{};
    if (clock_gettime(0 /*CLOCK_REALTIME*/, &ts) == 0)
    {
        if ((ts.tv_nsec > -1L) && (ts.tv_nsec <= timespec({0, RAND_MAX}).tv_nsec ))
        {
            srandom(static_cast<uint32_t>(ts.tv_nsec));
            ret = true;
        }
        else
        {
            DERR("incoherent time value");
        }
    }
    else
    {
        DERR("error getting time to seed randomizer");
    }

    return ret;
}

uint8_t OSEF::Randomizer::getUInt8() const
{
    return static_cast<uint8_t>(static_cast<uint64_t>(random())&0xffU);
}

uint16_t OSEF::Randomizer::getUInt16() const
{
    return static_cast<uint16_t>(static_cast<uint64_t>(random())&0xffffU);
}

uint32_t OSEF::Randomizer::getUInt32() const
{
    return getUInt16() + (static_cast<uint32_t>(getUInt16()) << 16U);
}

uint64_t OSEF::Randomizer::getUInt64() const
{
    return getUInt32() + (static_cast<uint64_t>(getUInt32()) << 32U);
}
