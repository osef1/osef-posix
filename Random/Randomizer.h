#ifndef OSEFRANDOMIZER_H
#define OSEFRANDOMIZER_H

#include <cstdint>  // int32_t

namespace OSEF
{
    class Randomizer
    {
    public:
        Randomizer();
        virtual ~Randomizer() = default;

        uint8_t getUInt8() const;
        uint16_t getUInt16() const;
        uint32_t getUInt32() const;
        uint64_t getUInt64() const;

        Randomizer(const Randomizer&) = delete;  // copy constructor
        Randomizer& operator=(const Randomizer&) = delete;  // copy assignment
        Randomizer(Randomizer&&) = delete;  // move constructor
        Randomizer& operator=(Randomizer&&) = delete;  // move assignment

    private:
        bool seed() const;
    };
}  // namespace OSEF

#endif /* OSEFRANDOMIZER_H */
