#!/bin/bash

cd build/release

r="-r ../../../File/Test/OSEF_File_UT_read"
o="-o ../../../File/Test/OSEF_File_UT_overwrite"
a="-a ../../../File/Test/OSEF_File_UT_append"

./osef-gtest --gtest_output="xml:OSEF_POSIX_GTest_Report.xml" $r $o $a
