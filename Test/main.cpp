#include "AppendFileP.h"
#include "OverwriteFileP.h"
#include "ReadFileP.h"
#include "MutexP.h"
#include "TimeOutP.h"
#include "ThreaderP.h"

#include <getopt.h>

namespace OSEF
{
    void displayTestParametersUsage(const std::string& exeName)
    {
        std::cout << "Usage:   " << exeName << " [-option] [argument]" << std::endl;
        std::cout << "option:  " << std::endl;
        std::cout << "         " << "-f  Not a file test file name" << std::endl;
        std::cout << "         " << "-r  Read test file name" << std::endl;
        std::cout << "         " << "-o  Overwrite test file name" << std::endl;
        std::cout << "         " << "-a  Append test file name" << std::endl;
        std::cout << "         " << "-s  seconds" << std::endl;
        std::cout << "         " << "-n  nanoseconds" << std::endl;
    }

    bool getTestParameters(const int32_t& argc, char** argv)
    {
        bool ret = true;
        int option_index = 0;
        static struct option long_options[] ={ {"gtest_output", required_argument, 0, 0}, {} };

        if (argc > 1)
        {
            int32_t opt = -1;
            do
            {
                opt = getopt_long(argc, argv, "f:r:o:a:s:n:", long_options, &option_index);

                if (opt != -1)
                {
                    switch (opt)
                    {
                        case 0:
                            break;
                        case 'f':
                            OSEF::ReadFileP::testParam.noFilename = optarg;
                            break;
                        case 'r':
                            OSEF::ReadFileP::testParam.readFilename = optarg;
                            break;
                        case 'o':
                            OSEF::OverwriteFileP::testParam.overwriteFilename = optarg;
                            break;
                        case 'a':
                            OSEF::AppendFileP::testParam.appendFilename = optarg;
                            break;
                        case 's':
                            OSEF::TimeOutP::testParam.userTestTime.tv_sec = std::stol(optarg);
                            break;
                        case 'n':
                            OSEF::TimeOutP::testParam.userTestTime.tv_nsec = std::stol(optarg);
                            break;
                        default:
                            displayTestParametersUsage(argv[0UL]);
                            ret = false;
                            break;
                    }
                }
            }while (opt != -1);
        }

        OSEF::TimeOutP::adjustAutoTime();

        return ret;
    }
}  // namespace OSEF

int main(int argc, char** argv)
{
    int ret = -1;

    if (OSEF::getTestParameters(argc, argv))
    {
        ::testing::InitGoogleTest(&argc, argv);
        ret = RUN_ALL_TESTS();
    }

    return ret;
}
