#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/e2a3a156/AppendFileP.o \
	${OBJECTDIR}/_ext/e2a3a156/OverwriteFileP.o \
	${OBJECTDIR}/_ext/e2a3a156/ReadFileP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQBlockingStringReceiverP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQDirectoryP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingStringReceiverP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingStringSenderP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingTypeReceiverP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingTypeSenderP.o \
	${OBJECTDIR}/_ext/d4ae5662/MsgQTestTools.o \
	${OBJECTDIR}/_ext/cc91ad71/MutexP.o \
	${OBJECTDIR}/_ext/4285ee2f/RandomizerP.o \
	${OBJECTDIR}/_ext/a699e17f/NetworkSessionP.o \
	${OBJECTDIR}/_ext/a699e17f/RandomMessage.o \
	${OBJECTDIR}/_ext/a699e17f/RandomSocketName.o \
	${OBJECTDIR}/_ext/a699e17f/TcpClientP.o \
	${OBJECTDIR}/_ext/a699e17f/TcpListenerP.o \
	${OBJECTDIR}/_ext/a699e17f/TcpListenerThreadP.o \
	${OBJECTDIR}/_ext/a699e17f/TcpServerP.o \
	${OBJECTDIR}/_ext/a699e17f/UdpClientP.o \
	${OBJECTDIR}/_ext/a699e17f/UdpMulticastClientP.o \
	${OBJECTDIR}/_ext/a699e17f/UdpMulticastServerP.o \
	${OBJECTDIR}/_ext/a699e17f/UdpServerP.o \
	${OBJECTDIR}/_ext/a699e17f/UnixClientP.o \
	${OBJECTDIR}/_ext/a699e17f/UnixListenerP.o \
	${OBJECTDIR}/_ext/a699e17f/UnixServerP.o \
	${OBJECTDIR}/_ext/df517948/ThreaderP.o \
	${OBJECTDIR}/_ext/3347eea5/TimeOutP.o \
	${OBJECTDIR}/_ext/eec6de75/gtest-all.o \
	${OBJECTDIR}/_ext/29dd86f/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../../NetBeans/OSEF_POSIX/dist/Debug/GNU-Linux/libosef_posix.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_posix_gtest

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_posix_gtest: ../../../NetBeans/OSEF_POSIX/dist/Debug/GNU-Linux/libosef_posix.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_posix_gtest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_posix_gtest ${OBJECTFILES} ${LDLIBSOPTIONS} -lpthread -lrt

${OBJECTDIR}/_ext/e2a3a156/AppendFileP.o: ../../../File/Test/AppendFileP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/e2a3a156
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/e2a3a156/AppendFileP.o ../../../File/Test/AppendFileP.cpp

${OBJECTDIR}/_ext/e2a3a156/OverwriteFileP.o: ../../../File/Test/OverwriteFileP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/e2a3a156
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/e2a3a156/OverwriteFileP.o ../../../File/Test/OverwriteFileP.cpp

${OBJECTDIR}/_ext/e2a3a156/ReadFileP.o: ../../../File/Test/ReadFileP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/e2a3a156
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/e2a3a156/ReadFileP.o ../../../File/Test/ReadFileP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQBlockingStringReceiverP.o: ../../../MsgQ/Test/MsgQBlockingStringReceiverP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQBlockingStringReceiverP.o ../../../MsgQ/Test/MsgQBlockingStringReceiverP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQDirectoryP.o: ../../../MsgQ/Test/MsgQDirectoryP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQDirectoryP.o ../../../MsgQ/Test/MsgQDirectoryP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingStringReceiverP.o: ../../../MsgQ/Test/MsgQNonBlockingStringReceiverP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingStringReceiverP.o ../../../MsgQ/Test/MsgQNonBlockingStringReceiverP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingStringSenderP.o: ../../../MsgQ/Test/MsgQNonBlockingStringSenderP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingStringSenderP.o ../../../MsgQ/Test/MsgQNonBlockingStringSenderP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingTypeReceiverP.o: ../../../MsgQ/Test/MsgQNonBlockingTypeReceiverP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingTypeReceiverP.o ../../../MsgQ/Test/MsgQNonBlockingTypeReceiverP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingTypeSenderP.o: ../../../MsgQ/Test/MsgQNonBlockingTypeSenderP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQNonBlockingTypeSenderP.o ../../../MsgQ/Test/MsgQNonBlockingTypeSenderP.cpp

${OBJECTDIR}/_ext/d4ae5662/MsgQTestTools.o: ../../../MsgQ/Test/MsgQTestTools.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d4ae5662
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d4ae5662/MsgQTestTools.o ../../../MsgQ/Test/MsgQTestTools.cpp

${OBJECTDIR}/_ext/cc91ad71/MutexP.o: ../../../Mutex/Test/MutexP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/cc91ad71
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cc91ad71/MutexP.o ../../../Mutex/Test/MutexP.cpp

${OBJECTDIR}/_ext/4285ee2f/RandomizerP.o: ../../../Random/Test/RandomizerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/4285ee2f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4285ee2f/RandomizerP.o ../../../Random/Test/RandomizerP.cpp

${OBJECTDIR}/_ext/a699e17f/NetworkSessionP.o: ../../../Socket/Test/NetworkSessionP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/NetworkSessionP.o ../../../Socket/Test/NetworkSessionP.cpp

${OBJECTDIR}/_ext/a699e17f/RandomMessage.o: ../../../Socket/Test/RandomMessage.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/RandomMessage.o ../../../Socket/Test/RandomMessage.cpp

${OBJECTDIR}/_ext/a699e17f/RandomSocketName.o: ../../../Socket/Test/RandomSocketName.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/RandomSocketName.o ../../../Socket/Test/RandomSocketName.cpp

${OBJECTDIR}/_ext/a699e17f/TcpClientP.o: ../../../Socket/Test/TcpClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/TcpClientP.o ../../../Socket/Test/TcpClientP.cpp

${OBJECTDIR}/_ext/a699e17f/TcpListenerP.o: ../../../Socket/Test/TcpListenerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/TcpListenerP.o ../../../Socket/Test/TcpListenerP.cpp

${OBJECTDIR}/_ext/a699e17f/TcpListenerThreadP.o: ../../../Socket/Test/TcpListenerThreadP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/TcpListenerThreadP.o ../../../Socket/Test/TcpListenerThreadP.cpp

${OBJECTDIR}/_ext/a699e17f/TcpServerP.o: ../../../Socket/Test/TcpServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/TcpServerP.o ../../../Socket/Test/TcpServerP.cpp

${OBJECTDIR}/_ext/a699e17f/UdpClientP.o: ../../../Socket/Test/UdpClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UdpClientP.o ../../../Socket/Test/UdpClientP.cpp

${OBJECTDIR}/_ext/a699e17f/UdpMulticastClientP.o: ../../../Socket/Test/UdpMulticastClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UdpMulticastClientP.o ../../../Socket/Test/UdpMulticastClientP.cpp

${OBJECTDIR}/_ext/a699e17f/UdpMulticastServerP.o: ../../../Socket/Test/UdpMulticastServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UdpMulticastServerP.o ../../../Socket/Test/UdpMulticastServerP.cpp

${OBJECTDIR}/_ext/a699e17f/UdpServerP.o: ../../../Socket/Test/UdpServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UdpServerP.o ../../../Socket/Test/UdpServerP.cpp

${OBJECTDIR}/_ext/a699e17f/UnixClientP.o: ../../../Socket/Test/UnixClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UnixClientP.o ../../../Socket/Test/UnixClientP.cpp

${OBJECTDIR}/_ext/a699e17f/UnixListenerP.o: ../../../Socket/Test/UnixListenerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UnixListenerP.o ../../../Socket/Test/UnixListenerP.cpp

${OBJECTDIR}/_ext/a699e17f/UnixServerP.o: ../../../Socket/Test/UnixServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/a699e17f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/a699e17f/UnixServerP.o ../../../Socket/Test/UnixServerP.cpp

${OBJECTDIR}/_ext/df517948/ThreaderP.o: ../../../Thread/Test/ThreaderP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/df517948
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/df517948/ThreaderP.o ../../../Thread/Test/ThreaderP.cpp

${OBJECTDIR}/_ext/3347eea5/TimeOutP.o: ../../../Time/Test/TimeOutP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/3347eea5
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3347eea5/TimeOutP.o ../../../Time/Test/TimeOutP.cpp

${OBJECTDIR}/_ext/eec6de75/gtest-all.o: ../../../googletest/googletest/src/gtest-all.cc
	${MKDIR} -p ${OBJECTDIR}/_ext/eec6de75
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/eec6de75/gtest-all.o ../../../googletest/googletest/src/gtest-all.cc

${OBJECTDIR}/_ext/29dd86f/main.o: ../../main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../googletest/googletest -I../../../googletest/googletest/include -I../../../osef-log/Debug -I../../../File -I../../../File/Test -I../../../MsgQ -I../../../MsgQ/Test -I../../../Mutex -I../../../Mutex/Test -I../../../Random -I../../../Random/Test -I../../../Socket -I../../../Socket/Test -I../../../Thread -I../../../Thread/Test -I../../../Time -I../../../Time/Test -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/main.o ../../main.cpp

# Subprojects
.build-subprojects:
	cd ../../../NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../../../NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
