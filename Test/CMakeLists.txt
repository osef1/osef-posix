cmake_minimum_required(VERSION 3.13)

include(../osef.cmake)
include(gtest.cmake)

project(osef-gtest)

add_subdirectory(".." "${CMAKE_CURRENT_BINARY_DIR}/OSEF_POSIX")
link_directories(${CMAKE_CURRENT_BINARY_DIR}/OSEF_POSIX)

add_executable(${PROJECT_NAME}
    main.cpp
    ../File/Test/ReadFileP.cpp
    ../File/Test/AppendFileP.cpp
    ../File/Test/OverwriteFileP.cpp
    ../MsgQ/Test/MsgQTestTools.cpp
    ../MsgQ/Test/MsgQBlockingStringReceiverP.cpp
    ../MsgQ/Test/MsgQDirectoryP.cpp
    ../MsgQ/Test/MsgQNonBlockingStringReceiverP.cpp
    ../MsgQ/Test/MsgQNonBlockingStringSenderP.cpp
    ../MsgQ/Test/MsgQNonBlockingTypeReceiverP.cpp
    ../MsgQ/Test/MsgQNonBlockingTypeSenderP.cpp
    ../Mutex/Test/MutexP.cpp
    ../Random/Test/RandomizerP.cpp
    ../Socket/Test/RandomMessage.cpp
    ../Socket/Test/RandomSocketName.cpp
    ../Socket/Test/NetworkSessionP.cpp
    ../Socket/Test/TcpClientP.cpp
    ../Socket/Test/TcpServerP.cpp
    ../Socket/Test/TcpListenerP.cpp
    ../Socket/Test/TcpListenerThreadP.cpp
    ../Socket/Test/UnixClientP.cpp
    ../Socket/Test/UnixServerP.cpp
    ../Socket/Test/UnixListenerP.cpp
    ../Socket/Test/UdpClientP.cpp
#    ../Socket/Test/UdpServerP.cpp
    ../Socket/Test/UdpMulticastClientP.cpp
    ../Socket/Test/UdpMulticastServerP.cpp
    ../Thread/Test/ThreaderP.cpp
    ../Time/Test/TimeOutP.cpp
)

target_link_libraries(${PROJECT_NAME}
    OSEF_POSIX
    gtest
)

include_directories(
    ../File/Test
    ../Mutex/Test
    ../MsgQ/Test
    ../Random/Test
    ../Socket/Test
    ../Thread/Test
    ../Time/Test
)
