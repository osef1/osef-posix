#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/29dd86f/MsgQBlockingStringReceiver.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQDirectory.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingReceiver.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingSender.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringReceiver.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringSender.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeReceiver.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeSender.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wall -Wextra -Werror
CXXFLAGS=-Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_msgq.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_msgq.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_msgq.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_msgq.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_msgq.a

${OBJECTDIR}/_ext/29dd86f/MsgQBlockingStringReceiver.o: ../../MsgQBlockingStringReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQBlockingStringReceiver.o ../../MsgQBlockingStringReceiver.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQDirectory.o: ../../MsgQDirectory.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQDirectory.o ../../MsgQDirectory.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingReceiver.o: ../../MsgQNonBlockingReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingReceiver.o ../../MsgQNonBlockingReceiver.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingSender.o: ../../MsgQNonBlockingSender.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingSender.o ../../MsgQNonBlockingSender.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringReceiver.o: ../../MsgQNonBlockingStringReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringReceiver.o ../../MsgQNonBlockingStringReceiver.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringSender.o: ../../MsgQNonBlockingStringSender.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringSender.o ../../MsgQNonBlockingStringSender.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeReceiver.o: ../../MsgQNonBlockingTypeReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeReceiver.o ../../MsgQNonBlockingTypeReceiver.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeSender.o: ../../MsgQNonBlockingTypeSender.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../osef-log/Debug -I../../../Time -I../../../Random -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeSender.o ../../MsgQNonBlockingTypeSender.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
