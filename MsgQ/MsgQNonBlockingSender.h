#ifndef OSEFMSGQNONBLOCKINGSENDER_H
#define OSEFMSGQNONBLOCKINGSENDER_H

#include <mqueue.h>  // mqd_t
#include <string>

namespace OSEF
{
    class MsgQNonBlockingSender
    {
    public:
        explicit MsgQNonBlockingSender(const std::string& mqname);
        virtual ~MsgQNonBlockingSender();

        bool reset(const std::string& mqname);

        MsgQNonBlockingSender(const MsgQNonBlockingSender&) = delete;  // copy constructor
        MsgQNonBlockingSender& operator=(const MsgQNonBlockingSender&) = delete;  // copy assignment
        MsgQNonBlockingSender(MsgQNonBlockingSender&&) = delete;  // move constructor
        MsgQNonBlockingSender& operator=(MsgQNonBlockingSender&&) = delete;  // move assignment

    protected:
        bool sendBuffer(const void* buffer, const size_t& size);

    private:
        bool openMQ();
        void closeMQ();

        mqd_t msgQ;  ///< message queue POSIX descriptor
        std::string name;  ///< message queue name
        uint32_t flags;  ///< message queue flags
        size_t maxMsgSize;  ///< maximum message size allowed in bytes
    };
}  // namespace OSEF

#endif /* OSEFMSGQNONBLOCKINGSENDER_H */
