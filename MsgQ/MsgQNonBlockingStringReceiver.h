#ifndef OSEFMSGQNONBLOCKINGSTRINGRECEIVER_H
#define OSEFMSGQNONBLOCKINGSTRINGRECEIVER_H

#include "MsgQNonBlockingReceiver.h"

#include <string>

namespace OSEF
{
    class MsgQNonBlockingStringReceiver : public MsgQNonBlockingReceiver
    {
    public:
        explicit MsgQNonBlockingStringReceiver(const size_t& msgsize, const size_t& mqsize = 1);
        ~MsgQNonBlockingStringReceiver() override = default;

        bool receiveMsg(std::string& msg);  ///< receive string message

        MsgQNonBlockingStringReceiver(const MsgQNonBlockingStringReceiver&) = delete;  // copy constructor
        MsgQNonBlockingStringReceiver& operator=(const MsgQNonBlockingStringReceiver&) = delete;  // copy assignment
        MsgQNonBlockingStringReceiver(MsgQNonBlockingStringReceiver&&) = delete;  // move constructor
        MsgQNonBlockingStringReceiver& operator=(MsgQNonBlockingStringReceiver&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif  // OSEFMSGQNONBLOCKINGSTRINGRECEIVER_H
