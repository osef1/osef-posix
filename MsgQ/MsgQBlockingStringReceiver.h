#ifndef OSEFMSGQBLOCKINGSTRINGRECEIVER_H
#define OSEFMSGQBLOCKINGSTRINGRECEIVER_H

#include "MsgQNonBlockingStringReceiver.h"

#include <string>

namespace OSEF
{
    class MsgQBlockingStringReceiver : public MsgQNonBlockingStringReceiver
    {
    public:
        explicit MsgQBlockingStringReceiver(const size_t& msgsize, const size_t& mqsize = 1);
        ~MsgQBlockingStringReceiver() override = default;

        using MsgQNonBlockingStringReceiver::receiveMsg;  // makes receiveMsg(std::string& s) visible from MsgQBlockingReceiver

        bool receiveMsg(std::string& msg, const timespec& to, bool& rto);  ///< receive string message with timeout

        MsgQBlockingStringReceiver(const MsgQBlockingStringReceiver&) = delete;  // copy constructor
        MsgQBlockingStringReceiver& operator=(const MsgQBlockingStringReceiver&) = delete;  // copy assignment
        MsgQBlockingStringReceiver(MsgQBlockingStringReceiver&&) = delete;  // move constructor
        MsgQBlockingStringReceiver& operator=(MsgQBlockingStringReceiver&&) = delete;  // move assignment

     protected:
        bool receiveBuffer(char* buffer, size_t& size, const timespec& to, bool& rto);  ///< receive a message with timeout

    private:
        bool getAbsoluteTimeout(timespec& ts, const timespec& to) const;
    };
}  // namespace OSEF

#endif  // OSEFMSGQBLOCKINGSTRINGRECEIVER_H
