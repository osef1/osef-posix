#ifndef OSEFMSGQNONBLOCKINGSTRINGSENDER_H
#define OSEFMSGQNONBLOCKINGSTRINGSENDER_H

#include "MsgQNonBlockingSender.h"
#include <string>

namespace OSEF
{
    class MsgQNonBlockingStringSender : public MsgQNonBlockingSender
    {
    public:
        explicit MsgQNonBlockingStringSender(const std::string& mqname);
        ~MsgQNonBlockingStringSender() override = default;

        virtual bool sendMsg(const std::string& msg);  ///< template overloads to convert string to type T

        MsgQNonBlockingStringSender(const MsgQNonBlockingStringSender&) = delete;  // copy constructor
        MsgQNonBlockingStringSender& operator=(const MsgQNonBlockingStringSender&) = delete;  // copy assignment
        MsgQNonBlockingStringSender(MsgQNonBlockingStringSender&&) = delete;  // move constructor
        MsgQNonBlockingStringSender& operator=(MsgQNonBlockingStringSender&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif  // OSEFMSGQNONBLOCKINGSTRINGSENDER_H
