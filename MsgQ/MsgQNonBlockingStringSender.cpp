#include "MsgQNonBlockingStringSender.h"

OSEF::MsgQNonBlockingStringSender::MsgQNonBlockingStringSender(const std::string& mqname)
    : MsgQNonBlockingSender(mqname) {}

bool OSEF::MsgQNonBlockingStringSender::sendMsg(const std::string& msg)
{
    return sendBuffer(msg.c_str(), msg.size());
}
