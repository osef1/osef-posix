#include "MsgQNonBlockingSender.h"
#include "MsgQFlags.h"  // OSEF_MQ_WRONLY OSEF_MQ_NONBLOCK
#include "Debug.h"

OSEF::MsgQNonBlockingSender::MsgQNonBlockingSender(const std::string& mqname)
    :msgQ(-1),
    name(mqname),
    flags(OSEF_MQ_WRONLY|OSEF_MQ_NONBLOCK),
    maxMsgSize(0) {}

OSEF::MsgQNonBlockingSender::~MsgQNonBlockingSender()
{
    closeMQ();
}

void OSEF::MsgQNonBlockingSender::closeMQ()
{
    if (msgQ > 0)
    {
        // close message queue as it won't be here used anymore
        if (mq_close(msgQ) != 0)
        {
            DERR("error closing message queue");
        }

        msgQ = -1;
    }
}

/// reset message queue connection according to new name and flags

/// \param mqname message queue name
/// \param mqblock blocking message queue flag
bool OSEF::MsgQNonBlockingSender::reset(const std::string& mqname)
{
    if (msgQ > 0)  // message queue already connected
    {
        closeMQ();
    }

    name = mqname;

    return openMQ();
}

/// sends message buffer
bool OSEF::MsgQNonBlockingSender::sendBuffer(const void* buffer, const size_t& size)
{
    bool ret = false;

    if (openMQ())
    {
        if (size <= maxMsgSize)
        {
            if (mq_send(msgQ, static_cast<const char*>(buffer), size, 0) == 0)
            {
                ret = true;
            }
            else
            {
                DWARN("error sending message to " << name.c_str() << " " << strerror(errno));
            }
        }
        else
        {
            DWARN("message too long " << size << ">" << maxMsgSize);
        }
    }

    return ret;
}

bool OSEF::MsgQNonBlockingSender::openMQ()
{
    bool ret = false;

    if (msgQ <= 0)  // not connected yet
    {
        msgQ = mq_open(name.c_str(), static_cast<int32_t>(flags) );
        if (msgQ > 0)
        {
            // reads message queue attributes to filter messages
            mq_attr attr{};
            if (mq_getattr(msgQ, &attr) == 0)  // check attributes were correctly read
            {
                maxMsgSize = attr.mq_msgsize;
                ret = true;
            }
            else
            {
                closeMQ();
                DWARN("error getting message queue " << name << " attributes");
            }
        }
        else
        {
            DWARN("error opening message queue " << name);
        }
    }
    else  // already connected
    {
        ret = true;
    }

    return ret;
}
