#include "MsgQNonBlockingReceiver.h"
#include "MsgQFlags.h"  // OSEF_MQ_RDONLY, OSEF_MQ_CREAT, OSEF_MQ_NONBLOCK
#include "Debug.h"

OSEF::MsgQDirectory OSEF::MsgQNonBlockingReceiver::directory;

OSEF::MsgQNonBlockingReceiver::MsgQNonBlockingReceiver(const uint16_t& msgsize, const uint16_t& mqsize)
    :msgQ(-1),
    flags(OSEF_MQ_RDONLY|OSEF_MQ_CREAT|OSEF_MQ_NONBLOCK),
    maxMsgSize(msgsize),
    maxQueueSize(mqsize) {}

OSEF::MsgQNonBlockingReceiver::~MsgQNonBlockingReceiver()
{
    if (msgQ > 0)
    {
        // close message queue as it won't be used anymore
        if (mq_close(msgQ) != 0)
        {
            DERR("error closing message queue");
        }

        msgQ = -1;

        // before unlinking it to remove it from system
        if (mq_unlink(name.c_str()) != 0)
        {
            DWARN("error unlinking message queue");
        }
    }

    // and releasing reserved name from directory
    if ((not name.empty()) && (not directory.release(name)))
    {
        DWARN("error releasing unlinked message queue name " << name);
    }
}

/// receives message buffer
/// will block or not depending on flags
/// \param buffer buffer pointer
/// \param size buffer size then received size
/// \return true if received sized not null
bool OSEF::MsgQNonBlockingReceiver::receiveBuffer(char* buffer, uint16_t& size)
{
    bool ret = false;

    if (openMQ())
    {
        if (size <= maxMsgSize)  // check message size
        {
            // will block or not depending on flags
            ssize_t rcvsize = mq_receive(msgQ, buffer, size, nullptr);
            if (rcvsize > 0L)
            {
                size = rcvsize;
                ret = true;
            }
        }
        else
        {
            DWARN("buffer too big " << size << ">" << maxMsgSize);
        }
    }

    return ret;
}

bool OSEF::MsgQNonBlockingReceiver::getName(std::string& n)
{
    bool ret = false;

    if (openMQ())
    {
        n = name;
        ret = true;
    }

    return ret;
}

/// opens message queue according to name, flags and sizes
bool OSEF::MsgQNonBlockingReceiver::openMQ()
{
    bool ret = false;

    if (msgQ <= 0)
    {
        struct mq_attr attr{};
        attr.mq_flags =  0;
        attr.mq_maxmsg = maxQueueSize;
        attr.mq_msgsize = maxMsgSize;
        attr.mq_curmsgs = 0;

        name = directory.reserve();
        msgQ = mq_open(name.c_str(), static_cast<int32_t>(flags), 0600U, &attr);
        if (msgQ > 0)
        {
            ret = true;
        }
        else
        {
            if (not directory.release(name))
            {
                DWARN("error releasing aborted message queue name " << name);
            }
            DWARN("opening message queue failed returned " << strerror(errno) << std::endl << "Check name format (\"/xxx\") and limits (/proc/sys/fs/mqueue/msg_max)");
            name = "";
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}
