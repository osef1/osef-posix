#include "MsgQBlockingStringReceiver.h"
#include "TimeOut.h"
#include "Debug.h"

OSEF::MsgQBlockingStringReceiver::MsgQBlockingStringReceiver(const size_t& msgsize, const size_t& mqsize)
    :MsgQNonBlockingStringReceiver(msgsize, mqsize)
{
    resetNonBlockingFlag();
}

bool OSEF::MsgQBlockingStringReceiver::receiveMsg(std::string& msg, const timespec& to, bool& rto)
{
    bool ret = false;

    size_t size = getMaxMsgSize();
    char b[size];

    if (receiveBuffer(&b[0], size, to, rto))
    {
        if (not rto)
        {
            msg.assign(&b[0], size);
        }

        ret = true;
    }

    return ret;
}

/// receives message buffer with timeout
/// \param buffer buffer pointer
/// \param size maximum buffer size then received size
/// \param to reception timeout
/// \param rto flag raised when reception timeout occurs or non-blocking queue was configured
/// \return true if received sized strictly positive or if rto is true
bool OSEF::MsgQBlockingStringReceiver::receiveBuffer(char* buffer, size_t& size, const timespec& to, bool& rto)
{
    bool ret = false;
    rto = false;

    if (openMQ())
    {
        if (size <= getMaxMsgSize())  // check message size
        {
            timespec ts{};
            // get absolute timeout from current time and relative timeout
            if (getAbsoluteTimeout(ts, to))
            {
                // will block until reception or timeout is reached
                ssize_t rcvsize = mq_timedreceive(getDescriptor(), buffer, size, nullptr, &ts);
                if (rcvsize > 0L)
                {
                    size = rcvsize;
                    ret = true;
                }
                else
                {
                    size = 0;

                    if ((rcvsize < 0L) && ((errno == ETIMEDOUT) || (errno == EAGAIN)))  // timeout occured or non-blocking queue was configured
                    {
                        rto = true;
                        ret = true;
                    }
                }
            }
            else
            {
                DERR("error getting time");
            }
        }
        else
        {
            DERR("buffer too big max " << getMaxMsgSize());
        }
    }

    return ret;
}

bool OSEF::MsgQBlockingStringReceiver::getAbsoluteTimeout(timespec& ts, const timespec& to) const
{
    bool ret = false;

    if (OSEF::getTime(ts))
    {
        // build absolute timeout from current time and relative timeout
        ts = ts + to;

        ret = true;
    }

    return ret;
}
