#include "MsgQDirectory.h"
#include "Randomizer.h"
#include "Debug.h"

#include <climits>

bool OSEF::MsgQDirectory::reserve(const std::string& name)
{
    bool ret = false;

    if (checkUserName(name))
    {
        if (directory.size() < OSEF_MSGQ_DIRECTORY_MAX)
        {
            ret = true;

            auto i = directory.begin();
            while ((i != directory.end()) && ret)
            {
                if (*i == name)
                {
                    ret = false;
                }
                ++i;
            }

            if (ret)
            {
                directory.push_back(name);
            }
            else
            {
                DWARN("name is already reserved " << name);
            }
        }
        else
        {
            DWARN("name directory is full " << directory.size());
        }
    }

    return ret;
}

bool OSEF::MsgQDirectory::checkUserName(const std::string& name) const
{
    bool ret = false;

    if (name.size() > 1UL)
    {
        if (name.size() <= OSEF_MSGQ_NAME_MAX)
        {
            if (name[0] == '/')
            {
                if (name.find('/', 1) == std::string::npos)
                {
                    ret = true;
                }
                else
                {
                    DWARN("character at position " << name.find('/', 1) << " is a slash");
                }
            }
            else
            {
                DWARN("first character is not a slash " << name[0]);
            }
        }
        else
        {
            DWARN("name is too long " << name.length() << " > " << OSEF_MSGQ_NAME_MAX);
        }
    }
    else
    {
        DWARN("name is too short " << name.length() << " < 2");
    }

    return ret;
}

std::string OSEF::MsgQDirectory::reserve()
{
    std::string ret;

    std::string name = getRandomName();

    if (reserve(name))
    {
        ret = name;
    }

    return ret;
}

std::string OSEF::MsgQDirectory::getRandomName() const
{
    std::string ret = "/";  // name must start with /

    OSEF::Randomizer randomizer;
    do
    {
        //  get printable character from SPACE to tild
        //  https://www.asciihex.com/ascii-printable-characters
        uint8_t c = (randomizer.getUInt8() % ('~' - ' ')) + ' ';

        // name must not contain / apart from first character
        if (c != '/')
        {
            ret += static_cast<char>(c);
        }
    }while (ret.size() < OSEF_MSGQ_NAME_MAX);

    return ret;
}

bool OSEF::MsgQDirectory::release(const std::string& name)
{
    bool ret = false;

    auto i = directory.begin();
    while ((i != directory.end()) && not ret)
    {
        if (*i == name)
        {
            directory.erase(i);
            ret = true;
        }
        ++i;
    }

    if (not ret)
    {
        DWARN("Name " << name << " was not reserved");
    }

    return ret;
}
