#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/3c163524/gtest-all.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQBlockingStringReceiverP.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQDirectoryP.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringReceiverP.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringSenderP.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeReceiverP.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeSenderP.o \
	${OBJECTDIR}/_ext/29dd86f/MsgQTestTools.o \
	${OBJECTDIR}/_ext/29dd86f/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wall -Wextra -Werror
CXXFLAGS=-Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../../NetBeans/OSEF_MsgQ/dist/Release/GNU-Linux/libosef_msgq.a ../../../../Random/NetBeans/OSEF_Random/dist/Release/GNU-Linux/libosef_random.a ../../../../Time/netbeans/osef-time/dist/Release/GNU-Linux/libosef-time.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_msgq_gtest

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_msgq_gtest: ../../../NetBeans/OSEF_MsgQ/dist/Release/GNU-Linux/libosef_msgq.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_msgq_gtest: ../../../../Random/NetBeans/OSEF_Random/dist/Release/GNU-Linux/libosef_random.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_msgq_gtest: ../../../../Time/netbeans/osef-time/dist/Release/GNU-Linux/libosef-time.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_msgq_gtest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_msgq_gtest ${OBJECTFILES} ${LDLIBSOPTIONS} -lrt -lpthread

${OBJECTDIR}/_ext/3c163524/gtest-all.o: ../../../../googletest/googletest/src/gtest-all.cc
	${MKDIR} -p ${OBJECTDIR}/_ext/3c163524
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3c163524/gtest-all.o ../../../../googletest/googletest/src/gtest-all.cc

${OBJECTDIR}/_ext/29dd86f/MsgQBlockingStringReceiverP.o: ../../MsgQBlockingStringReceiverP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQBlockingStringReceiverP.o ../../MsgQBlockingStringReceiverP.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQDirectoryP.o: ../../MsgQDirectoryP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQDirectoryP.o ../../MsgQDirectoryP.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringReceiverP.o: ../../MsgQNonBlockingStringReceiverP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringReceiverP.o ../../MsgQNonBlockingStringReceiverP.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringSenderP.o: ../../MsgQNonBlockingStringSenderP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingStringSenderP.o ../../MsgQNonBlockingStringSenderP.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeReceiverP.o: ../../MsgQNonBlockingTypeReceiverP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeReceiverP.o ../../MsgQNonBlockingTypeReceiverP.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeSenderP.o: ../../MsgQNonBlockingTypeSenderP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQNonBlockingTypeSenderP.o ../../MsgQNonBlockingTypeSenderP.cpp

${OBJECTDIR}/_ext/29dd86f/MsgQTestTools.o: ../../MsgQTestTools.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/MsgQTestTools.o ../../MsgQTestTools.cpp

${OBJECTDIR}/_ext/29dd86f/main.o: ../../main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../Random -I../../../../Time -I../../../../osef-log/Debug -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/main.o ../../main.cpp

# Subprojects
.build-subprojects:
	cd ../../../NetBeans/OSEF_MsgQ && ${MAKE}  -f Makefile CONF=Release
	cd ../../../../Random/NetBeans/OSEF_Random && ${MAKE}  -f Makefile CONF=Release
	cd ../../../../Time/netbeans/osef-time && ${MAKE}  -f Makefile CONF=Release

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../../../NetBeans/OSEF_MsgQ && ${MAKE}  -f Makefile CONF=Release clean
	cd ../../../../Random/NetBeans/OSEF_Random && ${MAKE}  -f Makefile CONF=Release clean
	cd ../../../../Time/netbeans/osef-time && ${MAKE}  -f Makefile CONF=Release clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
