#include "MsgQTestTools.h"
#include "MsgQNonBlockingStringSender.h"

void getRandomMessage(std::string& msg, const size_t& size)
{
    msg = "";

    OSEF::Randomizer randomizer;
    do
    {
        uint8_t c = randomizer.getUInt8();

        // msg must not contain end of string character
        if (c != 0)
        {
            msg += c;
        }
    }while (msg.size() < size);
}

bool sendRandomMessage(OSEF::MsgQNonBlockingStringReceiver& receiver, const size_t& msgSize, std::string& sentMsg)
{
    bool ret = false;

    // connect sender to receiver
    std::string name = "";
    if (receiver.getName(name))
    {
        OSEF::MsgQNonBlockingStringSender sender(name);

        // get random message
        sentMsg = "";
        getRandomMessage(sentMsg, msgSize);

        // send message
        ret = sender.sendMsg(sentMsg);
    }

    return ret;
}

bool sendRandomMessage(OSEF::MsgQBlockingStringReceiver& receiver, const size_t& size, std::string& sentMsg)
{
    bool ret = false;

    // connect sender to receiver
    std::string name = "";
    if (receiver.getName(name))
    {
        OSEF::MsgQNonBlockingStringSender sender(name);

        // get random message
        sentMsg = "";
        getRandomMessage(sentMsg, size);

        // send message
        ret = sender.sendMsg(sentMsg);
    }

    return ret;
}
