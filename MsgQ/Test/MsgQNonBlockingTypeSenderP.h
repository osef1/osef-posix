#ifndef OSEFMSGQNONBLOCKINGTYPEGSENDERP_H
#define OSEFMSGQNONBLOCKINGTYPEGSENDERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t msgQSize;
    }MsgQNonBlockingTypeSenderParam;

    class MsgQNonBlockingTypeSenderP : public ::testing::TestWithParam<MsgQNonBlockingTypeSenderParam>
    {
        protected:
            MsgQNonBlockingTypeSenderP() {}
            ~MsgQNonBlockingTypeSenderP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMSGQNONBLOCKINGTYPEGSENDERP_H
