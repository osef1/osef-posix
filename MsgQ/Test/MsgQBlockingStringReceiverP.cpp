#include "MsgQBlockingStringReceiverP.h"
#include "MsgQBlockingStringReceiver.h"
#include "MsgQNonBlockingStringSender.h"
#include "MsgQTestTools.h"
#include "TimeOut.h"

#include <string>
#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(ShortMessage_NoDepth,
                             MsgQBlockingStringReceiverP,
                             testing::Values(MsgQBlockingStringReceiverParam({8, 2})));

    TEST_P(MsgQBlockingStringReceiverP, receiveNothingTimeout)
    {
        OSEF::MsgQBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // no message should be received
        // timeout should be reached
        std::string receivedMsg = "";
        bool rto = false;

        EXPECT_TRUE(receiver.receiveMsg(receivedMsg, OSEF::T100ms, rto));
        EXPECT_TRUE(rto);
    }

    void receiveBlocking(OSEF::MsgQBlockingStringReceiver* receiver, std::string* msg, bool* running)
    {
        if ((receiver != nullptr) && (msg != nullptr) && (running != nullptr))
        {
            // loop on message reception until error occurs
            *running = true;
            do
            {
                // block until message is received
                *running = receiver->receiveMsg(*msg);
            }while (*running);
        }
    }

    TEST_P(MsgQBlockingStringReceiverP, receiveNothingBlock)
    {
        OSEF::MsgQBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // setup blocking receiver thread
        std::string receivedMsg = "";
        bool running = false;

        std::thread receiveBlockingThread(receiveBlocking, &receiver, &receivedMsg, &running);

        OSEF::sleepms(100);

        // blocking receiver thread should be still running
        EXPECT_TRUE(running);

        // no message should have been received
        EXPECT_EQ(receivedMsg, "");

        if (pthread_cancel(receiveBlockingThread.native_handle()) == 0)
        {
            try
            {
                receiveBlockingThread.join();
            }
            catch (const std::system_error& e) {}
        }
    }

    TEST_P(MsgQBlockingStringReceiverP, receiveSingleMessageTimeout)
    {
        OSEF::MsgQBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send then receive single messages of sizes from 1 to GetParam().msgSize
        for (size_t msgSize = 1; msgSize <= GetParam().msgSize; msgSize++)
        {
            // send random message
            std::string sentMsg = "";
            EXPECT_TRUE(sendRandomMessage(receiver, msgSize, sentMsg));

            // receive message
            std::string receivedMsg = "";
            bool rto = false;
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg, OSEF::T10ms, rto));
            EXPECT_FALSE(rto);
            EXPECT_EQ(receivedMsg, sentMsg);

            // no more message should be received
            receivedMsg = "";
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg, OSEF::T10ms, rto));
            EXPECT_TRUE(rto);
            EXPECT_EQ(receivedMsg, "");
        }
    }

    TEST_P(MsgQBlockingStringReceiverP, receiveSingleMessageBlock)
    {
        OSEF::MsgQBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send then receive single messages of sizes from 1 to GetParam().msgSize
        for (size_t msgSize = 1; msgSize <= GetParam().msgSize; msgSize++)
        {
            // send random message
            std::string sentMsg = "";
            EXPECT_TRUE(sendRandomMessage(receiver, msgSize, sentMsg));

            // receive message
            std::string receivedMsg = "";
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
            EXPECT_EQ(receivedMsg, sentMsg);
        }
    }

    TEST_P(MsgQBlockingStringReceiverP, receiveMultipleMessagesTimeout)
    {
        OSEF::MsgQBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send multiple messages until queue is full
        // back them up for further comparison
        std::string sentMsg = "";
        std::string backupMsg[GetParam().msgQSize];

        for (size_t m = 0; m < GetParam().msgQSize; m++)
        {
            EXPECT_TRUE(sendRandomMessage(receiver, GetParam().msgSize, sentMsg));
            backupMsg[m] = sentMsg;
        }

        // message queue should be full
        EXPECT_FALSE(sendRandomMessage(receiver, GetParam().msgSize, sentMsg));

        // receive multiple messages until queue is empty
        // compare them to emission back up
        std::string receivedMsg = "";
        bool rto = false;

        for (size_t m = 0; m < GetParam().msgQSize; m++)
        {
            // receive single message
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg, OSEF::T10ms, rto));
            EXPECT_FALSE(rto);

            // compare message to backup
            EXPECT_EQ(receivedMsg, backupMsg[m]);
            receivedMsg = "";
        }

        // message queue should be empty
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg, OSEF::T10ms, rto));
        EXPECT_TRUE(rto);
        EXPECT_EQ(receivedMsg, "");
    }

    TEST_P(MsgQBlockingStringReceiverP, receiveMultipleMessagesBlock)
    {
        OSEF::MsgQBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send multiple messages until queue is full
        // back them up for further comparison
        std::string sentMsg = "";
        std::string backupMsg[GetParam().msgQSize];

        for (size_t m = 0; m < GetParam().msgQSize; m++)
        {
            EXPECT_TRUE(sendRandomMessage(receiver, GetParam().msgSize, sentMsg));
            backupMsg[m] = sentMsg;
        }

        // message queue should be full
        EXPECT_FALSE(sendRandomMessage(receiver, GetParam().msgSize, sentMsg));

        // receive multiple messages until queue is empty
        // compare them to emission back up
        std::string receivedMsg = "";

        for (size_t m = 0; m < GetParam().msgQSize; m++)
        {
            // receive single message
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg));

            // compare message to backup
            EXPECT_EQ(receivedMsg, backupMsg[m]);
            receivedMsg = "";
        }

        // message queue should be empty
        bool rto = false;
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg, OSEF::T10ms, rto));
        EXPECT_TRUE(rto);
        EXPECT_EQ(receivedMsg, "");
    }
}  // namespace OSEF
