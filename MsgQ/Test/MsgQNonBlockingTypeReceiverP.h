#ifndef OSEFMSGQNONBLOCKINGTYPERECEIVERP_H
#define OSEFMSGQNONBLOCKINGTYPERECEIVERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t msgQSize;
    }MsgQNonBlockingTypeReceiverParam;

    class MsgQNonBlockingTypeReceiverP : public ::testing::TestWithParam<MsgQNonBlockingTypeReceiverParam>
    {
        protected:
            MsgQNonBlockingTypeReceiverP() {}
            ~MsgQNonBlockingTypeReceiverP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMSGQNONBLOCKINGTYPERECEIVERP_H
