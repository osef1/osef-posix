#include "MsgQNonBlockingTypeReceiverP.h"
#include "MsgQNonBlockingTypeReceiver.h"
#include "MsgQNonBlockingTypeSender.h"
#include "MsgQTestTools.h"
#include "Randomizer.h"

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(NoDepth,
                             MsgQNonBlockingTypeReceiverP,
                             testing::Values(MsgQNonBlockingTypeReceiverParam({2})));

    template <typename T> void receiveNothing(const size_t& msgQSize)
    {
        OSEF::MsgQNonBlockingTypeReceiver<T> receiver(msgQSize);

        // no message should be received
        T receivedMsg = 0;

        EXPECT_FALSE(receiver.receiveMsg(receivedMsg));
    }

    TEST_P(MsgQNonBlockingTypeReceiverP, receiveNothing)
    {
        receiveNothing<uint64_t>(GetParam().msgQSize);
        receiveNothing<uint32_t>(GetParam().msgQSize);
        receiveNothing<uint16_t>(GetParam().msgQSize);
        receiveNothing<uint8_t>(GetParam().msgQSize);
        receiveNothing<int64_t>(GetParam().msgQSize);
        receiveNothing<int32_t>(GetParam().msgQSize);
        receiveNothing<int16_t>(GetParam().msgQSize);
        receiveNothing<int8_t>(GetParam().msgQSize);
        receiveNothing<double>(GetParam().msgQSize);
        receiveNothing<float>(GetParam().msgQSize);
        receiveNothing<bool>(GetParam().msgQSize);
    }

    template <typename T> void receiveSingleMessage(const size_t& msgQSize)
    {
        OSEF::MsgQNonBlockingTypeReceiver<T> receiver(msgQSize);

        // send random message
        T sentMsg = 0;
        EXPECT_TRUE(sendRandomMessage(receiver, sentMsg));

        // receive message
        T receivedMsg = 0;
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
        EXPECT_EQ(receivedMsg, sentMsg);

        // no more message should be received
        EXPECT_FALSE(receiver.receiveMsg(receivedMsg));
    }

    TEST_P(MsgQNonBlockingTypeReceiverP, receiveSingleMessage)
    {
        receiveSingleMessage<uint64_t>(GetParam().msgQSize);
        receiveSingleMessage<uint32_t>(GetParam().msgQSize);
        receiveSingleMessage<uint16_t>(GetParam().msgQSize);
        receiveSingleMessage<uint8_t>(GetParam().msgQSize);
        receiveSingleMessage<int64_t>(GetParam().msgQSize);
        receiveSingleMessage<int32_t>(GetParam().msgQSize);
        receiveSingleMessage<int16_t>(GetParam().msgQSize);
        receiveSingleMessage<int8_t>(GetParam().msgQSize);
        receiveSingleMessage<double>(GetParam().msgQSize);
        receiveSingleMessage<float>(GetParam().msgQSize);
        receiveSingleMessage<bool>(GetParam().msgQSize);
    }

    template <typename T> void receiveMultipleMessages(const size_t& msgQSize)
    {
        OSEF::MsgQNonBlockingTypeReceiver<T> receiver(msgQSize);

        // send multiple messages until queue is full
        // back them up for further comparison
        T sentMsg = 0;
        T backupMsg[msgQSize];

        for (size_t m = 0; m < msgQSize; m++)
        {
            EXPECT_TRUE(sendRandomMessage(receiver, sentMsg));
            backupMsg[m] = sentMsg;
        }

        // message queue should be full
        EXPECT_FALSE(sendRandomMessage(receiver, sentMsg));

        // receive multiple messages until queue is empty
        // compare them to emission back up
        T receivedMsg = 0;

        for (size_t m = 0; m < msgQSize; m++)
        {
            // receive single message
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg));

            // compare message to backup
            EXPECT_EQ(receivedMsg, backupMsg[m]);
            receivedMsg = 0;
        }

        // message queue should be empty
        EXPECT_FALSE(receiver.receiveMsg(receivedMsg));
    }

    TEST_P(MsgQNonBlockingTypeReceiverP, receiveMultipleMessages)
    {
        receiveMultipleMessages<uint64_t>(GetParam().msgQSize);
        receiveMultipleMessages<uint32_t>(GetParam().msgQSize);
        receiveMultipleMessages<uint16_t>(GetParam().msgQSize);
        receiveMultipleMessages<uint8_t>(GetParam().msgQSize);
        receiveMultipleMessages<int64_t>(GetParam().msgQSize);
        receiveMultipleMessages<int32_t>(GetParam().msgQSize);
        receiveMultipleMessages<int16_t>(GetParam().msgQSize);
        receiveMultipleMessages<int8_t>(GetParam().msgQSize);
        receiveMultipleMessages<double>(GetParam().msgQSize);
        receiveMultipleMessages<float>(GetParam().msgQSize);
        receiveMultipleMessages<bool>(GetParam().msgQSize);
    }
}  // namespace OSEF
