#include "MsgQNonBlockingStringSenderP.h"
#include "MsgQNonBlockingStringSender.h"
#include "MsgQNonBlockingStringReceiver.h"
#include "MsgQTestTools.h"
#include "Randomizer.h"

#include <string>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(ShortMessage_SmallDepth,
                             MsgQNonBlockingStringSenderP,
                             testing::Values(MsgQNonBlockingStringSenderParam({8, 4})));

    TEST_P(MsgQNonBlockingStringSenderP, sendMsgToNonExistentQueue)
    {
        OSEF::MsgQNonBlockingStringSender sender("/noqname");

        // send random message
        std::string sentMsg = "";
        getRandomMessage(sentMsg, GetParam().msgSize);
        EXPECT_FALSE(sender.sendMsg(sentMsg));
    }

    TEST_P(MsgQNonBlockingStringSenderP, sendTooLongMessage)
    {
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize);

        // connect sender to receiver
        std::string name = "";
        EXPECT_TRUE(receiver.getName(name));
        OSEF::MsgQNonBlockingStringSender sender(name);

        // send random message
        std::string sentMsg = "";
        getRandomMessage(sentMsg, GetParam().msgSize + 1);
        EXPECT_FALSE(sender.sendMsg(sentMsg));
    }

    TEST_P(MsgQNonBlockingStringSenderP, resetToNonExistentQueue)
    {
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize);

        // connect sender to receiver
        std::string name = "";
        EXPECT_TRUE(receiver.getName(name));
        OSEF::MsgQNonBlockingStringSender sender(name);

        // send random message
        std::string sentMsg = "";
        getRandomMessage(sentMsg, GetParam().msgSize);
        EXPECT_TRUE(sender.sendMsg(sentMsg));

        // receive message
        std::string receivedMsg = "";
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
        EXPECT_EQ(receivedMsg, sentMsg);

        // reset sender
        EXPECT_FALSE(sender.reset("/noqname"));

        // send random message
        sentMsg = "";
        getRandomMessage(sentMsg, GetParam().msgSize);
        EXPECT_FALSE(sender.sendMsg(sentMsg));
    }

    TEST_P(MsgQNonBlockingStringSenderP, resetToExistingQueue)
    {
        OSEF::MsgQNonBlockingStringSender sender("/noqname");

        // send random message
        std::string sentMsg = "";
        getRandomMessage(sentMsg, GetParam().msgSize);
        EXPECT_FALSE(sender.sendMsg(sentMsg));

        // reset sender
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize);
        std::string name = "";
        EXPECT_TRUE(receiver.getName(name));
        EXPECT_TRUE(sender.reset(name));

        // send random message
        sentMsg = "";
        getRandomMessage(sentMsg, GetParam().msgSize);
        EXPECT_TRUE(sender.sendMsg(sentMsg));

        // receive message
        std::string receivedMsg = "";
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
        EXPECT_EQ(receivedMsg, sentMsg);
    }

    TEST_P(MsgQNonBlockingStringSenderP, sendMessageFromTwoSenders)
    {
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send then receive single messages of sizes from 1 to GetParam().msgSize
        for (size_t msgSize = 1; msgSize <= GetParam().msgSize; msgSize++)
        {
            // send random message from first sender
            std::string sentMsgA = "";
            EXPECT_TRUE(sendRandomMessage(receiver, msgSize, sentMsgA));

            // send random message from second sender
            std::string sentMsgB = "";
            EXPECT_TRUE(sendRandomMessage(receiver, msgSize, sentMsgB));

            // receive message first message
            std::string receivedMsgA = "";
            EXPECT_TRUE(receiver.receiveMsg(receivedMsgA));
            EXPECT_EQ(receivedMsgA, sentMsgA);

            // receive message second message
            std::string receivedMsgB = "";
            EXPECT_TRUE(receiver.receiveMsg(receivedMsgB));
            EXPECT_EQ(receivedMsgB, sentMsgB);

            // no more message should be received
            EXPECT_FALSE(receiver.receiveMsg(receivedMsgA));
        }
    }
}  // namespace OSEF
