#ifndef OSEFMSGQDIRECTORYP_H
#define OSEFMSGQDIRECTORYP_H

#include "gtest/gtest.h"

namespace OSEF
{
    class MsgQDirectoryP : public ::testing::TestWithParam<uint32_t>
    {
        protected:
            MsgQDirectoryP() {}
            ~MsgQDirectoryP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMSGQDIRECTORYP_H
