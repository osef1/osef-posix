#ifndef OSEFMSGQNONBLOCKINGSTRINGSENDERP_H
#define OSEFMSGQNONBLOCKINGSTRINGSENDERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t msgSize;
        size_t msgQSize;
    }MsgQNonBlockingStringSenderParam;

    class MsgQNonBlockingStringSenderP : public ::testing::TestWithParam<MsgQNonBlockingStringSenderParam>
    {
        protected:
            MsgQNonBlockingStringSenderP() {}
            ~MsgQNonBlockingStringSenderP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMSGQNONBLOCKINGSTRINGSENDERP_H
