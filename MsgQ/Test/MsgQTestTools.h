#ifndef MSGQTESTTOOLS_H
#define MSGQTESTTOOLS_H

#include "MsgQNonBlockingStringReceiver.h"
#include "MsgQBlockingStringReceiver.h"
#include "MsgQNonBlockingTypeReceiver.h"
#include "MsgQNonBlockingTypeSender.h"
#include "Randomizer.h"

#include <string>

void getRandomMessage(std::string& msg, const size_t& size);

bool sendRandomMessage(OSEF::MsgQNonBlockingStringReceiver& receiver, const size_t& msgSize, std::string& sentMsg);
bool sendRandomMessage(OSEF::MsgQBlockingStringReceiver& receiver, const size_t& size, std::string& sentMsg);

template <typename T> void getRandomMessage(T& msg)
{
    uint64_t tmp = 0;

    OSEF::Randomizer randomizer;
    for (size_t i = 0; i < sizeof(T); i++)
    {
        uint8_t c = randomizer.getUInt8();
        tmp += static_cast<uint64_t>(c) << (i * 8);
    }

    msg = static_cast<T>(tmp);
}

template <typename T> bool sendRandomMessage(OSEF::MsgQNonBlockingTypeReceiver<T>& receiver, T& sentMsg)
{
    bool ret = false;

    // connect sender to receiver
    std::string name = "";
    if (receiver.getName(name))
    {
        OSEF::MsgQNonBlockingTypeSender<T> sender(name);

        // get random message
        getRandomMessage(sentMsg);

        // send message
        ret = sender.sendMsg(sentMsg);
    }

    return ret;
}

#endif /* MSGQTESTTOOLS_H */
