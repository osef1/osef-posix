#include "MsgQNonBlockingTypeSenderP.h"
#include "MsgQNonBlockingTypeSender.h"
#include "MsgQNonBlockingTypeReceiver.h"
#include "MsgQTestTools.h"

#include <string>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(NoDepth,
                             MsgQNonBlockingTypeSenderP,
                             testing::Values(MsgQNonBlockingTypeSenderParam({2})));

    template <typename T> void sendMsgToNonExistentQueue()
    {
        OSEF::MsgQNonBlockingTypeSender<T> sender("/noqname");

        // no message should be sent
        T sentMsg = 0;

        EXPECT_FALSE(sender.sendMsg(sentMsg));
    }

    TEST_P(MsgQNonBlockingTypeSenderP, sendMsgToNonExistentQueue)
    {
        sendMsgToNonExistentQueue<uint64_t>();
        sendMsgToNonExistentQueue<uint32_t>();
        sendMsgToNonExistentQueue<uint16_t>();
        sendMsgToNonExistentQueue<uint8_t>();
        sendMsgToNonExistentQueue<int64_t>();
        sendMsgToNonExistentQueue<int32_t>();
        sendMsgToNonExistentQueue<int16_t>();
        sendMsgToNonExistentQueue<int8_t>();
        sendMsgToNonExistentQueue<double>();
        sendMsgToNonExistentQueue<float>();
        sendMsgToNonExistentQueue<bool>();
    }

    template <typename T> void resetMsgToNonExistentQueue(const size_t& msgQSize)
    {
        OSEF::MsgQNonBlockingTypeReceiver<T> receiver(msgQSize);

        // connect sender to receiver
        std::string name = "";
        EXPECT_TRUE(receiver.getName(name));
        OSEF::MsgQNonBlockingTypeSender<T> sender(name);

        // send random message
        T sentMsg = 0;
        getRandomMessage(sentMsg);
        EXPECT_TRUE(sender.sendMsg(sentMsg));

        // receive message
        T receivedMsg = 0;
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
        EXPECT_EQ(receivedMsg, sentMsg);

        // reset sender
        EXPECT_FALSE(sender.reset("/noqname"));

        // send random message
        sentMsg = 0;
        getRandomMessage(sentMsg);
        EXPECT_FALSE(sender.sendMsg(sentMsg));
    }

    TEST_P(MsgQNonBlockingTypeSenderP, resetToNonExistentQueue)
    {
        resetMsgToNonExistentQueue<uint64_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<uint32_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<uint16_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<uint8_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<int64_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<int32_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<int16_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<int8_t>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<double>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<float>(GetParam().msgQSize);
        resetMsgToNonExistentQueue<bool>(GetParam().msgQSize);
    }

    template <typename T> void resetToExistingQueue(const size_t& msgQSize)
    {
        OSEF::MsgQNonBlockingTypeSender<T> sender("/noqname");

        // send random message
        T sentMsg = 0;
        getRandomMessage(sentMsg);
        EXPECT_FALSE(sender.sendMsg(sentMsg));

        // reset sender
        OSEF::MsgQNonBlockingTypeReceiver<T> receiver(msgQSize);
        std::string name = "";
        EXPECT_TRUE(receiver.getName(name));
        EXPECT_TRUE(sender.reset(name));

        // send random message
        sentMsg = 0;
        getRandomMessage(sentMsg);
        EXPECT_TRUE(sender.sendMsg(sentMsg));

        // receive message
        T receivedMsg = 0;
        EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
        EXPECT_EQ(receivedMsg, sentMsg);
    }

    TEST_P(MsgQNonBlockingTypeSenderP, resetToExistingQueue)
    {
        resetToExistingQueue<uint64_t>(GetParam().msgQSize);
        resetToExistingQueue<uint32_t>(GetParam().msgQSize);
        resetToExistingQueue<uint16_t>(GetParam().msgQSize);
        resetToExistingQueue<uint8_t>(GetParam().msgQSize);
        resetToExistingQueue<int64_t>(GetParam().msgQSize);
        resetToExistingQueue<int32_t>(GetParam().msgQSize);
        resetToExistingQueue<int16_t>(GetParam().msgQSize);
        resetToExistingQueue<int8_t>(GetParam().msgQSize);
        resetToExistingQueue<double>(GetParam().msgQSize);
        resetToExistingQueue<float>(GetParam().msgQSize);
        resetToExistingQueue<bool>(GetParam().msgQSize);
    }

    template <typename T> void sendMessageFromTwoSenders(const size_t& msgQSize)
    {
        OSEF::MsgQNonBlockingTypeReceiver<T> receiver(msgQSize);

        // send random message from first sender
        T sentMsgA = 0;
        EXPECT_TRUE(sendRandomMessage(receiver, sentMsgA));

        // send random message from second sender
        T sentMsgB = 0;
        EXPECT_TRUE(sendRandomMessage(receiver, sentMsgB));

        // receive message first message
        T receivedMsgA = 0;
        EXPECT_TRUE(receiver.receiveMsg(receivedMsgA));
        EXPECT_EQ(receivedMsgA, sentMsgA);

        // receive message second message
        T receivedMsgB = 0;
        EXPECT_TRUE(receiver.receiveMsg(receivedMsgB));
        EXPECT_EQ(receivedMsgB, sentMsgB);

        // no more message should be received
        EXPECT_FALSE(receiver.receiveMsg(receivedMsgA));
    }

    TEST_P(MsgQNonBlockingTypeSenderP, sendMessageFromTwoSenders)
    {
        sendMessageFromTwoSenders<uint64_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<uint32_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<uint16_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<uint8_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<int64_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<int32_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<int16_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<int8_t>(GetParam().msgQSize);
        sendMessageFromTwoSenders<double>(GetParam().msgQSize);
        sendMessageFromTwoSenders<float>(GetParam().msgQSize);
        sendMessageFromTwoSenders<bool>(GetParam().msgQSize);
    }
}  // namespace OSEF
