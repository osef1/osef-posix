#ifndef OSEFMSGQBLOCKINGSTRINGRECEIVERP_H
#define OSEFMSGQBLOCKINGSTRINGRECEIVERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t msgSize;
        size_t msgQSize;
    }MsgQBlockingStringReceiverParam;

    class MsgQBlockingStringReceiverP : public ::testing::TestWithParam<MsgQBlockingStringReceiverParam>
    {
        protected:
            MsgQBlockingStringReceiverP() {}
            ~MsgQBlockingStringReceiverP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMSGQBLOCKINGSTRINGRECEIVERP_H
