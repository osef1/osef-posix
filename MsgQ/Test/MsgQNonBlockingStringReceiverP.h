#ifndef OSEFMSGQNONBLOCKINGSTRINGRECEIVERP_H
#define OSEFMSGQNONBLOCKINGSTRINGRECEIVERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        size_t msgSize;
        size_t msgQSize;
    }MsgQNonBlockingStringReceiverParam;

    class MsgQNonBlockingStringReceiverP : public ::testing::TestWithParam<MsgQNonBlockingStringReceiverParam>
    {
        protected:
            MsgQNonBlockingStringReceiverP() {}
            ~MsgQNonBlockingStringReceiverP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFMSGQNONBLOCKINGSTRINGRECEIVERP_H
