#include "MsgQDirectoryP.h"
#include "MsgQDirectory.h"
#include "Randomizer.h"

#include <string>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(noParam,
                             MsgQDirectoryP,
                             testing::Values(0));

    TEST_P(MsgQDirectoryP, reserveTooShortName)
    {
        OSEF::MsgQDirectory directory;

        EXPECT_FALSE(directory.reserve(""));
        EXPECT_FALSE(directory.reserve("/"));
    }

    void getRandomName(std::string& name, const size_t& size = OSEF_MSGQ_NAME_MAX, const bool& filter_slash = true)
    {
        OSEF::Randomizer randomizer;
        do
        {
            //  get printable character from SPACE to tild
            //  https://www.asciihex.com/ascii-printable-characters
            uint8_t c = (randomizer.getUInt8() % ('~' - ' ')) + ' ';

            if (filter_slash)
            {
                // name must not contain / apart from first character
                if (c != '/')
                {
                    name += c;
                }
            }
            else
            {
                name += c;
            }
        }while (name.size() < size);
    }

    TEST_P(MsgQDirectoryP, reserveTooLongName)
    {
        OSEF::MsgQDirectory directory;

          // name must start with /
        std::string name = "/";

        // name is one character too long to trigger error
        getRandomName(name, OSEF_MSGQ_NAME_MAX + 1, true);

        EXPECT_FALSE(directory.reserve(name));
    }

    TEST_P(MsgQDirectoryP, reserveWrongFirstCharacterName)
    {
        OSEF::MsgQDirectory directory;

        // name starts with random non-slash character to trigger error
        std::string name = "";

        getRandomName(name);

        EXPECT_FALSE(directory.reserve(name));
    }

    TEST_P(MsgQDirectoryP, reserveWrongName)
    {
        OSEF::MsgQDirectory directory;

         std::string name = "";

        do
        {
            name = "/";
            getRandomName(name, OSEF_MSGQ_NAME_MAX, false);
            // build random name until / character is found after initial one
        }while (name.find('/', 1) == std::string::npos);

        EXPECT_FALSE(directory.reserve(name));
    }

    TEST_P(MsgQDirectoryP, releaseUnreservedName)
    {
        OSEF::MsgQDirectory directory;

        std::string name = "/";

        getRandomName(name);

        EXPECT_FALSE(directory.release(name));
    }

    TEST_P(MsgQDirectoryP, reserveSpecificName)
    {
        OSEF::MsgQDirectory directory;

        std::string name = "/";

        getRandomName(name);

        EXPECT_TRUE(directory.reserve(name));

        EXPECT_TRUE(directory.release(name));
        EXPECT_FALSE(directory.release(name));
    }

    TEST_P(MsgQDirectoryP, reserveSpecificNameTwice)
    {
        OSEF::MsgQDirectory directory;

        std::string name = "/";

        getRandomName(name);

        EXPECT_TRUE(directory.reserve(name));
        EXPECT_FALSE(directory.reserve(name));

        EXPECT_TRUE(directory.release(name));
        EXPECT_FALSE(directory.release(name));
    }

    TEST_P(MsgQDirectoryP, reserveRandomName)
    {
        OSEF::MsgQDirectory directory;

        std::string name = directory.reserve();
        EXPECT_EQ(name.size(), OSEF_MSGQ_NAME_MAX);
        EXPECT_EQ(name.at(0), '/');
        EXPECT_EQ(name.find('/', 1), std::string::npos);

        EXPECT_TRUE(directory.release(name));
        EXPECT_FALSE(directory.release(name));
    }

    TEST_P(MsgQDirectoryP, reserveRandomNameTwice)
    {
        OSEF::MsgQDirectory directory;

        std::string name = directory.reserve();
        EXPECT_EQ(name.size(), OSEF_MSGQ_NAME_MAX);
        EXPECT_EQ(name.at(0), '/');
        EXPECT_EQ(name.find('/', 1), std::string::npos);

        EXPECT_FALSE(directory.reserve(name));

        EXPECT_TRUE(directory.release(name));
        EXPECT_FALSE(directory.release(name));
    }

    TEST_P(MsgQDirectoryP, reserveFullDirectory)
    {
        OSEF::MsgQDirectory directory;

        for (uint32_t i = 0; i < OSEF_MSGQ_DIRECTORY_MAX; i++)
        {
            std::string name = directory.reserve();
            EXPECT_EQ(name.size(), OSEF_MSGQ_NAME_MAX);
            EXPECT_EQ(name.at(0), '/');
            EXPECT_EQ(name.find('/', 1), std::string::npos);
        }

        std::string noName = directory.reserve();
        EXPECT_EQ(noName, "");
    }
}  // namespace OSEF
