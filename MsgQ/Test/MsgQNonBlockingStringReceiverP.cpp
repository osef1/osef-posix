#include "MsgQNonBlockingStringReceiverP.h"
#include "MsgQNonBlockingStringReceiver.h"
#include "MsgQNonBlockingStringSender.h"
#include "MsgQTestTools.h"
#include "Debug.h"

#include <string>
#include <list>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(ShortMessage_NoDepth,
                             MsgQNonBlockingStringReceiverP,
                             testing::Values(MsgQNonBlockingStringReceiverParam({8, 2})));

    TEST_P(MsgQNonBlockingStringReceiverP, receiveNothing)
    {
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // no message should be received
        std::string receivedMsg = "";

        EXPECT_FALSE(receiver.receiveMsg(receivedMsg));
    }

    TEST_P(MsgQNonBlockingStringReceiverP, receiveSingleMessage)
    {
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send then receive single messages of sizes from 1 to GetParam().msgSize
        for (size_t msgSize = 1; msgSize <= GetParam().msgSize; msgSize++)
        {
            // send random message
            std::string sentMsg = "";
            EXPECT_TRUE(sendRandomMessage(receiver, msgSize, sentMsg));

            // receive message
            std::string receivedMsg = "";
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg));
            EXPECT_EQ(receivedMsg, sentMsg);

            // no more message should be received
            EXPECT_FALSE(receiver.receiveMsg(receivedMsg));
        }
    }

    TEST_P(MsgQNonBlockingStringReceiverP, receiveMultipleMessages)
    {
        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);

        // send multiple messages until queue is full
        // back them up for further comparison
        std::string sentMsg = "";
        std::string backupMsg[GetParam().msgQSize];

        for (size_t m = 0; m < GetParam().msgQSize; m++)
        {
            EXPECT_TRUE(sendRandomMessage(receiver, GetParam().msgSize, sentMsg));
            backupMsg[m] = sentMsg;
        }

        // message queue should be full
        EXPECT_FALSE(sendRandomMessage(receiver, GetParam().msgSize, sentMsg));

        // receive multiple messages until queue is empty
        // compare them to emission back up
        std::string receivedMsg = "";

        for (size_t m = 0; m < GetParam().msgQSize; m++)
        {
            // receive single message
            EXPECT_TRUE(receiver.receiveMsg(receivedMsg));

            // compare message to backup
            EXPECT_EQ(receivedMsg, backupMsg[m]);
            receivedMsg = "";
        }

        // message queue should be empty
        EXPECT_FALSE(receiver.receiveMsg(receivedMsg));
    }

    TEST_P(MsgQNonBlockingStringReceiverP, openTooMuchQueues)
    {
        std::list<OSEF::MsgQNonBlockingStringReceiver> receiverContainer;

        size_t count = 0;
        for (size_t i = 0; i < OSEF_MSGQ_DIRECTORY_MAX; i++)
        {
            receiverContainer.emplace_back(GetParam().msgSize, GetParam().msgQSize);
            std::string name = "";
            EXPECT_TRUE(receiverContainer.back().getName(name));
            if (receiverContainer.back().getName(name))
            {
                count++;
            }
        }

        OSEF::MsgQNonBlockingStringReceiver receiver(GetParam().msgSize, GetParam().msgQSize);
        std::string name = "";
        EXPECT_FALSE(receiver.getName(name));

        if (count == OSEF_MSGQ_DIRECTORY_MAX)
        {
            DOUT(count << " maximum messages queues were successfully created");
        }
        else
        {
            DERR("only " << count << " message queues were created compared to maximum " << OSEF_MSGQ_DIRECTORY_MAX);
        }
    }
}  // namespace OSEF
