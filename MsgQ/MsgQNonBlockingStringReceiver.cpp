#include "MsgQNonBlockingStringReceiver.h"

OSEF::MsgQNonBlockingStringReceiver::MsgQNonBlockingStringReceiver(const size_t& msgsize, const size_t& mqsize)
    : MsgQNonBlockingReceiver(msgsize, mqsize) {}

bool OSEF::MsgQNonBlockingStringReceiver::receiveMsg(std::string& msg)
{
    bool ret = false;

    uint16_t size = getMaxMsgSize();
    char b[size];

    if (receiveBuffer(&b[0], size) && (size > 0))
    {
        msg.assign(&b[0], size);
        ret = true;
    }

    return ret;
}
