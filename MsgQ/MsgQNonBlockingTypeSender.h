#ifndef OSEFMSGQNONBLOCKINGTYPESENDER_H
#define OSEFMSGQNONBLOCKINGTYPESENDER_H

#include "MsgQNonBlockingSender.h"

#include <string>

namespace OSEF
{
    template <typename T>
    class MsgQNonBlockingTypeSender : public MsgQNonBlockingSender
    {
    public:
        explicit MsgQNonBlockingTypeSender(const std::string& mqname);
        ~MsgQNonBlockingTypeSender() override;

        virtual bool sendMsg(const T& msg);

        MsgQNonBlockingTypeSender(const MsgQNonBlockingTypeSender&) = delete;  // copy constructor
        MsgQNonBlockingTypeSender& operator=(const MsgQNonBlockingTypeSender&) = delete;  // copy assignment
        MsgQNonBlockingTypeSender(MsgQNonBlockingTypeSender&&) = delete;  // move constructor
        MsgQNonBlockingTypeSender& operator=(MsgQNonBlockingTypeSender&&) = delete;  // move assignment
    };
}  // namespace OSEF

template <typename T>
OSEF::MsgQNonBlockingTypeSender<T>::MsgQNonBlockingTypeSender(const std::string& mqname)
    :MsgQNonBlockingSender(mqname) {}

template <typename T>
OSEF::MsgQNonBlockingTypeSender<T>::~MsgQNonBlockingTypeSender() = default;

template <typename T>
bool OSEF::MsgQNonBlockingTypeSender<T>::sendMsg(const T& msg)
{
    const bool ret = sendBuffer(&msg, sizeof(T));
    return ret;
}

#endif  // OSEFMSGQNONBLOCKINGTYPESENDER_H
