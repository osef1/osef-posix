#ifndef OSEFMSGQDIRECTORY_H
#define OSEFMSGQDIRECTORY_H

#include <string>
#include <list>

namespace OSEF
{
#ifndef OSEF_MSGQ_NAME_MAX
    const size_t OSEF_MSGQ_NAME_MAX = 8U;
#endif

#ifndef OSEF_MSGQ_DIRECTORY_MAX
    const size_t OSEF_MSGQ_DIRECTORY_MAX = 256U;
#endif

    using MQD_Container = std::list<std::string>;

    class MsgQDirectory
    {
    public:
        MsgQDirectory() = default;
        virtual ~MsgQDirectory() = default;

        bool reserve(const std::string& name);
        std::string reserve();

        bool release(const std::string& name);

        MsgQDirectory(const MsgQDirectory&) = delete;  // copy constructor
        MsgQDirectory& operator=(const MsgQDirectory&) = delete;  // copy assignment
        MsgQDirectory(MsgQDirectory&&) = delete;  // move constructor
        MsgQDirectory& operator=(MsgQDirectory&&) = delete;  // move assignment

    private:
        bool checkUserName(const std::string& name) const;
        std::string getRandomName() const;

        MQD_Container directory;
    };
}  // namespace OSEF

#endif /* OSEFMSGQDIRECTORY_H */
