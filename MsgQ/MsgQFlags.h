#ifndef OSEFMSGQFLAGS_H
#define OSEFMSGQFLAGS_H

#include <cstdint>  // uint32_t

namespace OSEF
{
    const uint32_t OSEF_MQ_RDONLY = 0x0000U;  // O_RDONLY 00
    const uint32_t OSEF_MQ_WRONLY = 0x0001U;  // O_WRONLY 01
    const uint32_t OSEF_MQ_CREAT = 0x0040U;  // O_CREAT 0100
    const uint32_t OSEF_MQ_NONBLOCK = 0x0800U;  // O_NONBLOCK 04000
}  // namespace OSEF

#endif /* OSEFMSGQFLAGS_H */
