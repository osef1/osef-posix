#ifndef OSEFMSGQNONBLOCKINGTYPERECEIVER_H
#define OSEFMSGQNONBLOCKINGTYPERECEIVER_H

#include "MsgQNonBlockingReceiver.h"

namespace OSEF
{
    template <typename T>
    class MsgQNonBlockingTypeReceiver : public MsgQNonBlockingReceiver
    {
    public:
        explicit MsgQNonBlockingTypeReceiver(const size_t& mqsize = 1);
        ~MsgQNonBlockingTypeReceiver() override;

        bool receiveMsg(T& msg);  ///< receive T message

        MsgQNonBlockingTypeReceiver(const MsgQNonBlockingTypeReceiver&) = delete;  // copy constructor
        MsgQNonBlockingTypeReceiver& operator=(const MsgQNonBlockingTypeReceiver&) = delete;  // copy assignment
        MsgQNonBlockingTypeReceiver(MsgQNonBlockingTypeReceiver&&) = delete;  // move constructor
        MsgQNonBlockingTypeReceiver& operator=(MsgQNonBlockingTypeReceiver&&) = delete;  // move assignment
    };
}  // namespace OSEF

template <typename T>
OSEF::MsgQNonBlockingTypeReceiver<T>::MsgQNonBlockingTypeReceiver(const size_t& mqsize)
    :MsgQNonBlockingReceiver(sizeof(T), mqsize) {}

template <typename T>
OSEF::MsgQNonBlockingTypeReceiver<T>::~MsgQNonBlockingTypeReceiver() = default;

template <typename T>
bool OSEF::MsgQNonBlockingTypeReceiver<T>::receiveMsg(T& msg)
{
    bool ret = false;

    uint16_t size = sizeof(T);

    if (receiveBuffer(reinterpret_cast<char*>(&msg), size))
    {
        if (size == sizeof(T))
        {
            ret = true;
        }
    }

    return ret;
}

#endif  // OSEFMSGQNONBLOCKINGTYPERECEIVER_H
