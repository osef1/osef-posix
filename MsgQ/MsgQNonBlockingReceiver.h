#ifndef OSEFMSGQNONBLOCKINGRECEIVER_H
#define OSEFMSGQNONBLOCKINGRECEIVER_H

#include "MsgQDirectory.h"
#include "MsgQFlags.h"  // OSEF_MQ_NONBLOCK

#include <mqueue.h>  // mqd_t
#include <string>

namespace OSEF
{
    class MsgQNonBlockingReceiver
    {
    public:
        MsgQNonBlockingReceiver(const uint16_t& msgsize, const uint16_t& mqsize);
        virtual ~MsgQNonBlockingReceiver();

        bool getName(std::string& n);

        MsgQNonBlockingReceiver(const MsgQNonBlockingReceiver&) = delete;  // copy constructor
        MsgQNonBlockingReceiver& operator=(const MsgQNonBlockingReceiver&) = delete;  // copy assignment
        MsgQNonBlockingReceiver(MsgQNonBlockingReceiver&&) = delete;  // move constructor
        MsgQNonBlockingReceiver& operator=(MsgQNonBlockingReceiver&&) = delete;  // move assignment

    protected:
        bool receiveBuffer(char* buffer, uint16_t& size);  ///< receive buffer message

        bool openMQ();

        mqd_t getDescriptor() const {return msgQ;}
        void resetNonBlockingFlag() {flags &= ~OSEF_MQ_NONBLOCK;}
        uint16_t getMaxMsgSize() const {return maxMsgSize;}

    private:
        mqd_t msgQ;  ///< message queue POSIX descriptor
        uint32_t flags;  ///< message queue flags
        uint16_t maxMsgSize;  ///< maximum message size allowed in bytes
        static MsgQDirectory directory;

        std::string name{};  ///< message queue name
        uint16_t maxQueueSize;  ///< maximum queue size allowed in messages
    };
}  // namespace OSEF

#endif /* OSEFMSGQNONBLOCKINGRECEIVER_H */
