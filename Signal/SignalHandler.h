#ifndef OSEFSIGNALHANDLER_H
#define OSEFSIGNALHANDLER_H

#include <csignal>  // siginfo_t
#include <cstdint>  // int32_t
#include <list>

namespace OSEF
{
    using SA_Container = std::list<int32_t>;

    class SignalHandler
    {
    public:
        SignalHandler() = default;
        virtual ~SignalHandler() = default;

        bool setSignalAction(const int32_t& sig);
        bool setSignalActionList(const SA_Container& siglist);
        bool setAllSignalAction();

        bool signalReceived() {return signalFlag;}

        SignalHandler(const SignalHandler&) = delete;  // copy constructor
        SignalHandler& operator=(const SignalHandler&) = delete;  // copy assignment
        SignalHandler(SignalHandler&&) = delete;  // move constructor
        SignalHandler& operator=(SignalHandler&&) = delete;  // move assignment

    private:
        static void signalActionRoutine(int32_t sig, siginfo_t* siginfo, void* context);

        static volatile bool signalFlag;
    };
}  // namespace OSEF

#endif /* OSEFSIGNALHANDLER_H */
