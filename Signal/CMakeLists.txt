cmake_minimum_required(VERSION 3.13)

include(../osef.cmake)

project(osef-signal)

# Convert TEST before adding subdirectories
if (TEST)
    # Reset TEST to avoid building subdirectories tests
    SET(TEST "off")
    SET(SIGNAL-TEST "on")
endif ()

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME}
    PRIVATE
        SignalHandler.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        .
        ../osef-log/Debug
)
