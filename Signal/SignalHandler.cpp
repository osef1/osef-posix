#include "SignalHandler.h"
#include "Debug.h"

#include <cstring>  // strsignal memset

namespace OSEF
{
    volatile bool OSEF::SignalHandler::signalFlag = false;
}  // namespace OSEF

// #define   SIGINT  2   // Interrupt (ANSI). // Shell CTRL+C
// #define   SIGTERM 15  // Termination (ANSI). // Netbeans stop
// #define   SIGPIPE 13  // Broken pipe (POSIX).

void OSEF::SignalHandler::signalActionRoutine(int32_t sig, siginfo_t* siginfo, void* context)
{
    if ( (sig > 0) && (siginfo != nullptr) && (context != nullptr) )
    {
        DOUT("Signal action " << strsignal(sig) << " code " << siginfo->si_code << " context " << context);
    }

    signalFlag = true;
}

bool OSEF::SignalHandler::setSignalAction(const int32_t& sig)
{
    bool ret = false;

    if (sig < _NSIG)
    {
        struct sigaction sa{};

        memset(&sa, 0, sizeof(struct sigaction));

        sa.sa_flags = SA_SIGINFO;
        sa.sa_sigaction = &signalActionRoutine;
        if (sigaction(sig, &sa, nullptr) == 0)
        {
            ret = true;
        }
        else
        {
            DERR("error setting signal action " << sig);
        }
    }
    else
    {
        DERR("signal number " << sig << " greater than " << _NSIG);
    }

    return ret;
}

bool OSEF::SignalHandler::setSignalActionList(const SA_Container& siglist)
{
    bool ret = false;

    if (not siglist.empty())
    {
        ret = true;

        for (const int32_t& sig : siglist)
        {
            ret &= setSignalAction(sig);
            if (not ret)
            {
                DERR("error setting signals list action while attempting signal " << sig);
            }
        }
    }

    return ret;
}

bool OSEF::SignalHandler::setAllSignalAction()
{
    bool ret = true;

    int32_t i = 0;
    do
    {
        ret &= setSignalAction(i);
        if (not ret)
        {
            DERR("error setting all signals action while attempting signal " << i);
        }
        i++;
    }while (i < _NSIG);

    return ret;
}
