#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/6e6acf3c/AppendFile.o \
	${OBJECTDIR}/_ext/6e6acf3c/File.o \
	${OBJECTDIR}/_ext/6e6acf3c/OverwriteFile.o \
	${OBJECTDIR}/_ext/6e6acf3c/ReadFile.o \
	${OBJECTDIR}/_ext/6e6acf3c/WriteFile.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQBlockingStringReceiver.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQDirectory.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingReceiver.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingSender.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingStringReceiver.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingStringSender.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingTypeReceiver.o \
	${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingTypeSender.o \
	${OBJECTDIR}/_ext/5f574fbf/Mutex.o \
	${OBJECTDIR}/_ext/92fe4e83/Randomizer.o \
	${OBJECTDIR}/_ext/9520d868/SignalHandler.o \
	${OBJECTDIR}/_ext/95738933/NetworkEndpoint.o \
	${OBJECTDIR}/_ext/95738933/NetworkSession.o \
	${OBJECTDIR}/_ext/95738933/SocketToolbox.o \
	${OBJECTDIR}/_ext/95738933/StreamEndpoint.o \
	${OBJECTDIR}/_ext/95738933/TcpApplication.o \
	${OBJECTDIR}/_ext/95738933/TcpClient.o \
	${OBJECTDIR}/_ext/95738933/TcpListener.o \
	${OBJECTDIR}/_ext/95738933/TcpListenerThread.o \
	${OBJECTDIR}/_ext/95738933/TcpServer.o \
	${OBJECTDIR}/_ext/95738933/UdpClient.o \
	${OBJECTDIR}/_ext/95738933/UdpEndpoint.o \
	${OBJECTDIR}/_ext/95738933/UdpMulticastClient.o \
	${OBJECTDIR}/_ext/95738933/UdpMulticastServer.o \
	${OBJECTDIR}/_ext/95738933/UdpServer.o \
	${OBJECTDIR}/_ext/95738933/UnixClient.o \
	${OBJECTDIR}/_ext/95738933/UnixListener.o \
	${OBJECTDIR}/_ext/95738933/UnixServer.o \
	${OBJECTDIR}/_ext/96cc77ca/Threader.o \
	${OBJECTDIR}/_ext/6e712c8d/TimeOut.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_posix.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_posix.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_posix.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_posix.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_posix.a

${OBJECTDIR}/_ext/6e6acf3c/AppendFile.o: ../../File/AppendFile.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6acf3c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6acf3c/AppendFile.o ../../File/AppendFile.cpp

${OBJECTDIR}/_ext/6e6acf3c/File.o: ../../File/File.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6acf3c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6acf3c/File.o ../../File/File.cpp

${OBJECTDIR}/_ext/6e6acf3c/OverwriteFile.o: ../../File/OverwriteFile.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6acf3c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6acf3c/OverwriteFile.o ../../File/OverwriteFile.cpp

${OBJECTDIR}/_ext/6e6acf3c/ReadFile.o: ../../File/ReadFile.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6acf3c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6acf3c/ReadFile.o ../../File/ReadFile.cpp

${OBJECTDIR}/_ext/6e6acf3c/WriteFile.o: ../../File/WriteFile.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6acf3c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6acf3c/WriteFile.o ../../File/WriteFile.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQBlockingStringReceiver.o: ../../MsgQ/MsgQBlockingStringReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQBlockingStringReceiver.o ../../MsgQ/MsgQBlockingStringReceiver.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQDirectory.o: ../../MsgQ/MsgQDirectory.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQDirectory.o ../../MsgQ/MsgQDirectory.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingReceiver.o: ../../MsgQ/MsgQNonBlockingReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingReceiver.o ../../MsgQ/MsgQNonBlockingReceiver.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingSender.o: ../../MsgQ/MsgQNonBlockingSender.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingSender.o ../../MsgQ/MsgQNonBlockingSender.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingStringReceiver.o: ../../MsgQ/MsgQNonBlockingStringReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingStringReceiver.o ../../MsgQ/MsgQNonBlockingStringReceiver.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingStringSender.o: ../../MsgQ/MsgQNonBlockingStringSender.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingStringSender.o ../../MsgQ/MsgQNonBlockingStringSender.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingTypeReceiver.o: ../../MsgQ/MsgQNonBlockingTypeReceiver.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingTypeReceiver.o ../../MsgQ/MsgQNonBlockingTypeReceiver.cpp

${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingTypeSender.o: ../../MsgQ/MsgQNonBlockingTypeSender.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e6e22b0
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e6e22b0/MsgQNonBlockingTypeSender.o ../../MsgQ/MsgQNonBlockingTypeSender.cpp

${OBJECTDIR}/_ext/5f574fbf/Mutex.o: ../../Mutex/Mutex.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/5f574fbf
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5f574fbf/Mutex.o ../../Mutex/Mutex.cpp

${OBJECTDIR}/_ext/92fe4e83/Randomizer.o: ../../Random/Randomizer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/92fe4e83
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/92fe4e83/Randomizer.o ../../Random/Randomizer.cpp

${OBJECTDIR}/_ext/9520d868/SignalHandler.o: ../../Signal/SignalHandler.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/9520d868
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/9520d868/SignalHandler.o ../../Signal/SignalHandler.cpp

${OBJECTDIR}/_ext/95738933/NetworkEndpoint.o: ../../Socket/NetworkEndpoint.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/NetworkEndpoint.o ../../Socket/NetworkEndpoint.cpp

${OBJECTDIR}/_ext/95738933/NetworkSession.o: ../../Socket/NetworkSession.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/NetworkSession.o ../../Socket/NetworkSession.cpp

${OBJECTDIR}/_ext/95738933/SocketToolbox.o: ../../Socket/SocketToolbox.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/SocketToolbox.o ../../Socket/SocketToolbox.cpp

${OBJECTDIR}/_ext/95738933/StreamEndpoint.o: ../../Socket/StreamEndpoint.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/StreamEndpoint.o ../../Socket/StreamEndpoint.cpp

${OBJECTDIR}/_ext/95738933/TcpApplication.o: ../../Socket/TcpApplication.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/TcpApplication.o ../../Socket/TcpApplication.cpp

${OBJECTDIR}/_ext/95738933/TcpClient.o: ../../Socket/TcpClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/TcpClient.o ../../Socket/TcpClient.cpp

${OBJECTDIR}/_ext/95738933/TcpListener.o: ../../Socket/TcpListener.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/TcpListener.o ../../Socket/TcpListener.cpp

${OBJECTDIR}/_ext/95738933/TcpListenerThread.o: ../../Socket/TcpListenerThread.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/TcpListenerThread.o ../../Socket/TcpListenerThread.cpp

${OBJECTDIR}/_ext/95738933/TcpServer.o: ../../Socket/TcpServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/TcpServer.o ../../Socket/TcpServer.cpp

${OBJECTDIR}/_ext/95738933/UdpClient.o: ../../Socket/UdpClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UdpClient.o ../../Socket/UdpClient.cpp

${OBJECTDIR}/_ext/95738933/UdpEndpoint.o: ../../Socket/UdpEndpoint.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UdpEndpoint.o ../../Socket/UdpEndpoint.cpp

${OBJECTDIR}/_ext/95738933/UdpMulticastClient.o: ../../Socket/UdpMulticastClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UdpMulticastClient.o ../../Socket/UdpMulticastClient.cpp

${OBJECTDIR}/_ext/95738933/UdpMulticastServer.o: ../../Socket/UdpMulticastServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UdpMulticastServer.o ../../Socket/UdpMulticastServer.cpp

${OBJECTDIR}/_ext/95738933/UdpServer.o: ../../Socket/UdpServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UdpServer.o ../../Socket/UdpServer.cpp

${OBJECTDIR}/_ext/95738933/UnixClient.o: ../../Socket/UnixClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UnixClient.o ../../Socket/UnixClient.cpp

${OBJECTDIR}/_ext/95738933/UnixListener.o: ../../Socket/UnixListener.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UnixListener.o ../../Socket/UnixListener.cpp

${OBJECTDIR}/_ext/95738933/UnixServer.o: ../../Socket/UnixServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/95738933
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/95738933/UnixServer.o ../../Socket/UnixServer.cpp

${OBJECTDIR}/_ext/96cc77ca/Threader.o: ../../Thread/Threader.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/96cc77ca
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/96cc77ca/Threader.o ../../Thread/Threader.cpp

${OBJECTDIR}/_ext/6e712c8d/TimeOut.o: ../../Time/TimeOut.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e712c8d
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-log/Debug -I../../Random -I../../Swap -I../../MsgQ -I../../Thread -I../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e712c8d/TimeOut.o ../../Time/TimeOut.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
