cmake_minimum_required(VERSION 3.13)

include(../osef.cmake)

project(osef-time)

# Convert TEST before adding subdirectories
if (TEST)
    # Reset TEST to avoid building subdirectories tests
    SET(TEST "off")
    SET(TIME-TEST "on")
endif ()

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME}
    PRIVATE
        TimeOut.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        .
        ../osef-log/Debug
)

# Add test
if (TIME-TEST OR GLOBAL-TEST)
    add_subdirectory(Test)
    enable_testing ()
    add_test(NAME osef-time-gtest-suite  
            COMMAND osef-time-gtest
    )
endif ()
