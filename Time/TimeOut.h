#ifndef OSEFTIMEOUT_H
#define OSEFTIMEOUT_H

#include <ctime>  // timespec
#include <cstdint>  // uint16_t

namespace OSEF
{
    const int32_t ns_in_ms = 1000000;
    const int32_t ns_in_s = 1000000000;

    const timespec T10s =   {10, 0};  // 10 seconds
    const timespec T5s =    {5, 0};  // 5 seconds
    const timespec T2s =    {2, 0};  // 2 seconds
    const timespec T1s =    {1, 0};  // 1 seconds
    const timespec T0s =    {0, 0};  // 0 seconds
    const timespec T500ms = {0, 500000000};  // 500 milliseconds
    const timespec T250ms = {0, 250000000};  // 250 milliseconds
    const timespec T200ms = {0, 200000000};  // 200 milliseconds
    const timespec T100ms = {0, 100000000};  // 100 milliseconds
    const timespec T50ms =  {0, 50000000};  // 50 milliseconds
    const timespec T25ms =  {0, 25000000};  // 25 milliseconds
    const timespec T20ms =  {0, 20000000};  // 20 milliseconds
    const timespec T10ms =  {0, 10000000};  // 10 milliseconds
    const timespec T5ms =   {0, 5000000};  // 5 millisecond
    const timespec T2ms =   {0, 2000000};  // 2 millisecond
    const timespec T1ms =   {0, 1000000};  // 1 millisecond

    bool operator <(const timespec& lhs, const timespec& rhs);
    bool operator >=(const timespec& lhs, const timespec& rhs);
    timespec operator -(const timespec& lhs, const timespec& rhs);
    timespec operator +(const timespec& lhs, const timespec& rhs);

    bool getTime(timespec& to);
    bool sleep(const timespec& to);
    bool sleepms(const int16_t& ms);
    bool sleeps(const uint16_t& s);

    class TimeOut
    {
    public:
        explicit TimeOut(const timespec& to);
        virtual ~TimeOut() = default;

        timespec getDelay() const {return delay;}
        timespec getElapsedTime() const {return elapsedTime;}

        bool start();
        bool stop();

        bool sleepOff();
        bool isElapsed();

        bool isElapsedNow();
        timespec getRemainingTimeNow();

        TimeOut(const TimeOut&) = delete;  // copy constructor
        TimeOut& operator=(const TimeOut&) = delete;  // copy assignment
        TimeOut(TimeOut&&) = delete;  // move constructor
        TimeOut& operator=(TimeOut&&) = delete;  // move assignment

    private:
        timespec startTime;
        timespec stopTime;
        timespec elapsedTime;
        timespec remainingTime;
        timespec delay;
    };
}  // namespace OSEF

#endif /* OSEFTIMEOUT_H */
