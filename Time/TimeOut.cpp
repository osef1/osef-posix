#include "TimeOut.h"
#include "Debug.h"

OSEF::TimeOut::TimeOut(const timespec& to)
    :startTime({0, 0}),
    stopTime({0, 0}),
    elapsedTime({0, 0}),
    remainingTime({0, 0}),
    delay(to) {}

bool OSEF::TimeOut::sleepOff()
{
    bool ret = false;

    if (isElapsedNow())
    {
        ret = true;
    }
    else
    {
        ret = sleep(remainingTime);
    }

    return ret;
}

bool OSEF::TimeOut::isElapsedNow()
{
    bool ret = false;

    if (stop())
    {
        ret = isElapsed();
    }
    else
    {
        ret = true;
    }

    return ret;
}

timespec OSEF::TimeOut::getRemainingTimeNow()
{
    timespec ret = {0, 0};

    if (stop())
    {
        ret = remainingTime;
    }

    return ret;
}

bool OSEF::TimeOut::isElapsed()
{
    bool ret = false;

    if (elapsedTime >= delay)
    {
        ret = true;
    }

    return ret;
}

bool OSEF::TimeOut::stop()
{
    bool ret = false;

    if (getTime(stopTime))
    {
        elapsedTime = stopTime - startTime;
        if (elapsedTime >= delay)
        {
            remainingTime = {0, 0};
        }
        else
        {
            remainingTime = delay - elapsedTime;
        }

        ret = true;
    }

    return ret;
}

bool OSEF::TimeOut::start()
{
    return getTime(startTime);
}

bool OSEF::getTime(timespec& to)
{
    bool ret = false;

    if (clock_gettime(0 /*CLOCK_REALTIME*/, &to) == 0)
    {
        ret = true;
    }
    else
    {
        DERR("error getting time");
    }

    return ret;
}

bool OSEF::sleep(const timespec& to)
{
    bool ret = false;

    if (clock_nanosleep(0 /*CLOCK_REALTIME*/, 0 /*relative*/, &to, nullptr) == 0)
    {
        ret = true;
    }
    else
    {
        DERR("error sleeping: " << strerror(errno));
    }

    return ret;
}

bool OSEF::sleepms(const int16_t& ms)
{
    bool ret = false;
    const timespec ts = {0, ms * ns_in_ms};
    ret = sleep(ts);
    return ret;
}

bool OSEF::sleeps(const uint16_t& s)
{
    bool ret = false;
    const timespec ts = {s, 0};
    ret = sleep(ts);
    return ret;
}

bool OSEF::operator <(const timespec& lhs, const timespec& rhs)
{
    bool ret = false;

    if (lhs.tv_sec == rhs.tv_sec)
    {
        ret = lhs.tv_nsec < rhs.tv_nsec;
    }
    else
    {
        ret = lhs.tv_sec < rhs.tv_sec;
    }

    return ret;
}

bool OSEF::operator >=(const timespec& lhs, const timespec& rhs)
{
    bool ret = false;

    if (lhs.tv_sec == rhs.tv_sec)
    {
        ret = lhs.tv_nsec >= rhs.tv_nsec;
    }
    else
    {
        ret = lhs.tv_sec >= rhs.tv_sec;
    }

    return ret;
}

timespec OSEF::operator -(const timespec& lhs, const timespec& rhs)
{
    timespec ret = {0, 0};

    ret.tv_sec = lhs.tv_sec - rhs.tv_sec;
    ret.tv_nsec = lhs.tv_nsec - rhs.tv_nsec;

    if (ret.tv_sec > 0L)
    {
        if (ret.tv_nsec < 0L)
        {
            ret.tv_sec -= 1;
            ret.tv_nsec += ns_in_s;
        }
    }
    else if (ret.tv_sec == 0L)
    {
        if (ret.tv_nsec < 0L)
        {
            ret.tv_nsec = 0L;
        }
    }
    else
    {
        ret.tv_sec = 0L;
        ret.tv_nsec = 0L;
    }

    return ret;
}

timespec OSEF::operator +(const timespec& lhs, const timespec& rhs)
{
    timespec ret = {0, 0};

    ret.tv_sec = lhs.tv_sec + rhs.tv_sec;
    ret.tv_nsec = lhs.tv_nsec + rhs.tv_nsec;

    if (ret.tv_nsec >= static_cast<int64_t>(ns_in_s))
    {
        ret.tv_nsec -= ns_in_s;
        ret.tv_sec += 1;  // next second
    }

    return ret;
}
