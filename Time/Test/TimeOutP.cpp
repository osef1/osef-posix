#include "TimeOutP.h"
#include "TimeOut.h"
#include "Randomizer.h"
#include "Debug.h"

namespace OSEF
{
    TimeOutTestParam TimeOutP::testParam = {{0, 0}, {0, 0}};

    INSTANTIATE_TEST_SUITE_P(zeroTime,
                             TimeOutP,
                             testing::Values(timespec({0, 0})));

    INSTANTIATE_TEST_SUITE_P(userTime,
                             TimeOutP,
                             testing::Values(TimeOutP::testParam.userTestTime));

    INSTANTIATE_TEST_SUITE_P(autoTime,
                             TimeOutP,
                             testing::Values(timespec(TimeOutP::testParam.autoTestTime)));

    void TimeOutP::adjustAutoTime()
    {
        OSEF::Randomizer randomizer;

        if (OSEF::TimeOutP::testParam.userTestTime.tv_sec == 0)
        {
            if (OSEF::TimeOutP::testParam.userTestTime.tv_nsec == 0)
            {
                // user 0 time is replaced by random nanoseconds time
                OSEF::TimeOutP::testParam.userTestTime.tv_nsec = randomizer.getUInt32() % 1000000000;
            }

            // auto time is over 1 second when user time is below
            OSEF::TimeOutP::testParam.autoTestTime.tv_sec = 1;
        }

        // auto time nanoseconds are always random
        OSEF::TimeOutP::testParam.autoTestTime.tv_nsec = randomizer.getUInt32() % 1000000000;

        DOUT("TimeOut user test time is " << OSEF::TimeOutP::testParam.userTestTime.tv_sec << " s and " << OSEF::TimeOutP::testParam.userTestTime.tv_nsec << " ns");
        DOUT("TimeOut auto test time is " << OSEF::TimeOutP::testParam.autoTestTime.tv_sec << " s and " << OSEF::TimeOutP::testParam.autoTestTime.tv_nsec << " ns");
    }

    bool getDelayedTimes(timespec& t0, timespec& t1, const timespec& d)
    {
        bool ret = false;

        if (OSEF::getTime(t0))
        {
            if (clock_nanosleep(0 /*CLOCK_REALTIME*/, 0 /*relative*/, &d, nullptr) == 0)
            {
                if (OSEF::getTime(t1))
                {
                    ret = true;
                }
            }
        }

        if (not ret)
        {
            ADD_FAILURE();
        }

        return ret;
    }

    TEST_P(TimeOutP, lessThanOperator)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        if (getDelayedTimes(t0, t1, GetParam()))
        {
            EXPECT_TRUE(OSEF::operator <(t0, t1));
            EXPECT_FALSE(OSEF::operator <(t1, t0));

            EXPECT_FALSE(OSEF::operator <(t0, t0));
            EXPECT_FALSE(OSEF::operator <(t1, t1));
        }
    }

    TEST_P(TimeOutP, moreThanEqualOperator)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        if (getDelayedTimes(t0, t1, GetParam()))
        {
            EXPECT_FALSE(OSEF::operator >=(t0, t1));
            EXPECT_TRUE(OSEF::operator >=(t1, t0));

            EXPECT_TRUE(OSEF::operator >=(t0, t0));
            EXPECT_TRUE(OSEF::operator >=(t1, t1));
        }
    }

    TEST_P(TimeOutP, substractionOperator)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        if (getDelayedTimes(t0, t1, GetParam()))
        {
            timespec delta = OSEF::operator -(t1, t0);

            // rebuild t1 from t0 and substraction result
            timespec t0Delta = {t0.tv_sec + delta.tv_sec, t0.tv_nsec + delta.tv_nsec};
            if (t0Delta.tv_nsec > 1000000000)
            {
                t0Delta.tv_sec++;
                t0Delta.tv_nsec -= 1000000000;
            }

            // if substraction is correct t1 should be equal to t0 + delta
            EXPECT_EQ(t0Delta.tv_sec, t1.tv_sec);
            EXPECT_EQ(t0Delta.tv_nsec, t1.tv_nsec);

            delta = OSEF::operator -(t0, t1);

            // delta should be null as t1 is greater than t0
            EXPECT_EQ(delta.tv_sec, 0);
            EXPECT_EQ(delta.tv_nsec, 0);
        }
    }

    TEST_P(TimeOutP, additionOperator)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        if (getDelayedTimes(t0, t1, GetParam()))
        {
            timespec add = OSEF::operator +({0, 0}, t0);

            EXPECT_EQ(add.tv_sec, t0.tv_sec);
            EXPECT_EQ(add.tv_nsec, t0.tv_nsec);

            add = OSEF::operator +({0, 0}, t1);

            EXPECT_EQ(add.tv_sec, t1.tv_sec);
            EXPECT_EQ(add.tv_nsec, t1.tv_nsec);

            add = OSEF::operator +(t0, t1);

            if ((t0.tv_nsec + t1.tv_nsec) >= 1000000000)
            {
                EXPECT_EQ(add.tv_sec, t0.tv_sec + t1.tv_sec + 1);
                EXPECT_EQ(add.tv_nsec, t0.tv_nsec + t1.tv_nsec - 1000000000);
            }
            else
            {
                EXPECT_EQ(add.tv_sec, t0.tv_sec + t1.tv_sec);
                EXPECT_EQ(add.tv_nsec, t0.tv_nsec + t1.tv_nsec);
            }
        }
    }

    TEST_P(TimeOutP, readOnce)
    {
        timespec t0 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_NE(t0.tv_sec, 0);
        EXPECT_NE(t0.tv_nsec, 0);
    }

    TEST_P(TimeOutP, readTwice)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_NE(t0.tv_sec, 0);
        EXPECT_NE(t0.tv_nsec, 0);

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_NE(t1.tv_sec, 0);
        EXPECT_NE(t1.tv_nsec, 0);

        if (t0.tv_sec == t1.tv_sec)
        {
            EXPECT_GT(t1.tv_nsec, t0.tv_nsec);
        }
        else
        {
            EXPECT_GT(t1.tv_sec, t0.tv_sec);
        }
    }

    timespec getExtendedDelay(const timespec& delay)
    {
        return OSEF::operator +(delay, OSEF::T2ms);
    }

    bool isInTimeWindow(const timespec& t0, const timespec& t1, const timespec& delay)
    {
        bool ret = false;

        timespec duration = OSEF::operator -(t1, t0);
        if (OSEF::operator >=(duration, delay))
        {
            ret = OSEF::operator <(duration, getExtendedDelay(delay));
        }

//        DOUT("t0 "<<DT(t0));
//        DOUT("t1 "<<DT(t1));
//        DOUT("duration "<<DT(duration));
//
//        DOUT("delay "<<DT(delay));
//        DOUT("extended delay "<<DT(getExtendedDelay(delay)));
//        DOUT("time window ["<<DT(delay)<<" - "<<DT(getExtendedDelay(delay))<<"]");

        if (ret)
        {
            DOUT("margin " << DT(OSEF::operator -(getExtendedDelay(delay), duration)) << " s");
        }
        else
        {
            DERR("duration is out of time window by " << DT(OSEF::operator -(duration, getExtendedDelay(delay))) << " s");
        }

        return ret;
    }

    TEST_P(TimeOutP, sleep)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(OSEF::sleep(GetParam()));

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, GetParam()));
    }

    TEST_P(TimeOutP, sleepMilliseconds)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        uint16_t ms = GetParam().tv_nsec / 1000000;

        EXPECT_TRUE(OSEF::sleepms(ms));

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, {0, ms * 1000000}));
    }

    TEST_P(TimeOutP, sleepSeconds)
    {
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(OSEF::sleeps(GetParam().tv_sec));

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, {GetParam().tv_sec, 0}));
    }

    TEST_P(TimeOutP, doNothing)
    {
        OSEF::TimeOut timer(GetParam());
    }

    TEST_P(TimeOutP, getDelay)
    {
        OSEF::TimeOut t(GetParam());

        EXPECT_EQ(t.getDelay().tv_sec, GetParam().tv_sec);
        EXPECT_EQ(t.getDelay().tv_nsec, GetParam().tv_nsec);
    }

    TEST_P(TimeOutP, getZeroElapsedTime)
    {
        OSEF::TimeOut t(GetParam());

        EXPECT_EQ(t.getElapsedTime().tv_sec, 0);
        EXPECT_EQ(t.getElapsedTime().tv_nsec, 0);
    }

    TEST_P(TimeOutP, getShortestElapsedTime)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(t.stop());

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, t.getElapsedTime()));
    }

    TEST_P(TimeOutP, getDelayElapsedTime)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(OSEF::sleep(GetParam()));

        EXPECT_TRUE(t.stop());

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, t.getElapsedTime()));
    }

    TEST_P(TimeOutP, sleepOffImmediately)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(t.sleepOff());

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, GetParam()));
    }

    TEST_P(TimeOutP, sleepOffAfterDelay)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(OSEF::sleep(GetParam()));

        EXPECT_TRUE(t.sleepOff());

        EXPECT_TRUE(OSEF::getTime(t1));

        EXPECT_TRUE(isInTimeWindow(t0, t1, GetParam()));
    }

    TEST_P(TimeOutP, isElapsedImmediately)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(t.stop());

        bool ie = t.isElapsed();

        EXPECT_TRUE(OSEF::getTime(t1));

        timespec d = OSEF::operator -(t1, t0);

        if (OSEF::operator >=(d, t.getDelay()))
        {
            EXPECT_TRUE(ie);
        }
        else
        {
            EXPECT_FALSE(ie);
        }
    }

    TEST_P(TimeOutP, isElapsedAfterDelay)
    {
        OSEF::TimeOut t(GetParam());

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(OSEF::sleep(getExtendedDelay(GetParam())));

        EXPECT_TRUE(t.stop());

        EXPECT_TRUE(t.isElapsed());
    }

    TEST_P(TimeOutP, isElapsedNowImmediately)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        bool ie = t.isElapsedNow();

        EXPECT_TRUE(OSEF::getTime(t1));

        timespec d = OSEF::operator -(t1, t0);

        if (OSEF::operator >=(d, t.getDelay()))
        {
            EXPECT_TRUE(ie);
        }
        else
        {
            EXPECT_FALSE(ie);
        }
    }

    TEST_P(TimeOutP, isElapsedNowAfterDelay)
    {
        OSEF::TimeOut t(GetParam());

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(OSEF::sleep(getExtendedDelay(GetParam())));

        EXPECT_TRUE(t.isElapsedNow());
    }

    TEST_P(TimeOutP, getRemainingTimeNowImmediately)
    {
        OSEF::TimeOut t(GetParam());
        timespec t0 = {0, 0};
        timespec t1 = {0, 0};

        EXPECT_TRUE(OSEF::getTime(t0));

        EXPECT_TRUE(t.start());

        timespec r = t.getRemainingTimeNow();

        EXPECT_TRUE(OSEF::getTime(t1));

        timespec d = OSEF::operator -(t1, t0);

        if (OSEF::operator >=(d, t.getDelay()))
        {
            EXPECT_EQ(r.tv_sec, 0);
            EXPECT_EQ(r.tv_nsec, 0);
        }
        else
        {
            EXPECT_TRUE(OSEF::operator <(r, GetParam()));
        }
    }

    TEST_P(TimeOutP, getRemainingTimeNowAfterDelay)
    {
        OSEF::TimeOut t(GetParam());

        EXPECT_TRUE(t.start());

        EXPECT_TRUE(OSEF::sleep(getExtendedDelay(GetParam())));

        timespec r = t.getRemainingTimeNow();

        EXPECT_EQ(r.tv_sec, 0);
        EXPECT_EQ(r.tv_nsec, 0);
    }

}  // namespace OSEF
