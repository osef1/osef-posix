#ifndef OSEFTIMEOUTP_H
#define OSEFTIMEOUTP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        timespec userTestTime;
        timespec autoTestTime;
    }TimeOutTestParam;

    class TimeOutP : public ::testing::TestWithParam<timespec>
    {
        protected:
            TimeOutP() {}
            ~TimeOutP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}

            static void adjustAutoTime();

            static TimeOutTestParam testParam;
    };
}  // namespace OSEF

#endif /* OSEFTIMEOUTP_H */
