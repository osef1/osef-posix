#include "TimeOutP.h"

#include <getopt.h>  // getopt_long

namespace OSEF
{
    bool getUserTime(const int32_t& argc, char** argv)
    {
        bool ret = true;
        int option_index = 0;
        static struct option long_options[] ={ {"gtest_output", required_argument, 0, 0}, {} };

        if (argc > 1)
        {
            int32_t opt = -1;
            do
            {
                opt = getopt_long(argc, argv, "s:n:", long_options, &option_index);
                if (opt != -1)
                {
                    switch (opt)
                    {
                        case 0:
                            break;
                        case 's':
                            OSEF::TimeOutP::testParam.userTestTime.tv_sec = std::stol(optarg);
                            break;
                        case 'n':
                            OSEF::TimeOutP::testParam.userTestTime.tv_nsec = std::stol(optarg);
                            break;
                        default:
                            std::cout << "Usage:   " << argv[0UL] << " [-option] [argument]" << std::endl;
                            std::cout << "option:  " << std::endl;
                            std::cout << "         " << "-s  seconds" << std::endl;
                            std::cout << "         " << "-n  nanoseconds" << std::endl;
                            ret = false;
                            break;
                    }
                }
            }while (opt != -1);
        }

        OSEF::TimeOutP::adjustAutoTime();

        return ret;
    }
}  // namespace OSEF

int main(int argc, char **argv)
{
    int ret = -1;

    if (OSEF::getUserTime(argc, argv))
    {
        ::testing::InitGoogleTest(&argc, argv);
        ret = RUN_ALL_TESTS();
    }

    return ret;
}
