#ifndef OSEFAPPENDFILE_H
#define OSEFAPPENDFILE_H

#include <string>

#include "WriteFile.h"

namespace OSEF
{
    class AppendFile : public WriteFile
    {
    public:
        explicit AppendFile(const std::string& filename);
        ~AppendFile() override = default;

        static bool appendString(const std::string& filename, const std::string& s);  ///< append string to file

        AppendFile(const AppendFile&) = delete;  // copy constructor
        AppendFile& operator=(const AppendFile&) = delete;  // copy assignment
        AppendFile(AppendFile&&) = delete;  // move constructor
        AppendFile& operator=(AppendFile&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* OSEFAPPENDFILE_H */
