#include "WriteFile.h"

OSEF::WriteFile::WriteFile(const std::string& filename, const std::ios_base::openmode& filemode)
    :File(filename, std::fstream::out|filemode) {}

bool OSEF::WriteFile::writeString(const std::string& s)
{
    bool ret = false;

    if (openFile())
    {
        getFstream().write(s.c_str(), static_cast<std::streamsize>(s.length()));
        if (getFstream().good())
        {
            ret = true;
        }
        else
        {
            F_DERR("error writing");
        }
    }

    return ret;
}
