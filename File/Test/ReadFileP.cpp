#include "ReadFileP.h"
#include "ReadFile.h"
#include "FileReference.h"

namespace OSEF
{
    ReadFileTestParam ReadFileP::testParam =
    {
        "thisfilenameshouldnotexist",
        "OSEF_File_UT_read"
    };

    INSTANTIATE_TEST_SUITE_P(UserFiles, ReadFileP, testing::Values(ReadFileP::testParam));

    TEST_P(ReadFileP, readLine)
    {
        // create read file
        OSEF::ReadFile readFile(GetParam().readFilename);
        EXPECT_FALSE(readFile.isOpened());


        // initialize reference
        std::string longText = longMultilineTextRef;

        // compare file to reference line by line
        do
        {
            // read file's next line
            std::string readLine = "";
            EXPECT_TRUE(readFile.readLine(readLine));
            EXPECT_TRUE(readFile.isOpened());

            // read reference's next line
            std::string refLine = "";
            size_t pos = longText.find_first_of('\n', 0);
            if (pos != std::string::npos)
            {
                refLine = longText.substr(0, pos);
                longText.erase(0, refLine.size()+1);
            }
            else
            {
                // clear reference to exit loop
                longText = "";
            }

            // additional test to signal loop failure
            EXPECT_NE(pos, std::string::npos);

            // compare file to reference
            EXPECT_EQ(refLine, readLine);

            readLine = "";
        }while (longText.size() > 0);

        std::string noLine = "";
        EXPECT_FALSE(readFile.readLine(noLine));
        EXPECT_TRUE(readFile.isOpened());
    }

    TEST_P(ReadFileP, readLine_noFile)
    {
        OSEF::ReadFile readFile(GetParam().noFilename);
        std::string rl = "";
        EXPECT_FALSE(readFile.isOpened());
        EXPECT_FALSE(readFile.readLine(rl));
        EXPECT_FALSE(readFile.isOpened());
    }

    TEST_P(ReadFileP, readLine_noAuthorization)
    {
        OSEF::ReadFile readFile("/root/"+GetParam().readFilename);
        std::string rl = "";
        EXPECT_FALSE(readFile.isOpened());
        EXPECT_FALSE(readFile.readLine(rl));
        EXPECT_FALSE(readFile.isOpened());
    }

    TEST_P(ReadFileP, readFile)
    {
        OSEF::ReadFile readFile(GetParam().readFilename);
        std::string rf = "";
        EXPECT_FALSE(readFile.isOpened());
        EXPECT_TRUE(readFile.readFile(rf));
        EXPECT_EQ(rf, longMultilineTextRef);
        EXPECT_TRUE(readFile.isOpened());
    }

    TEST_P(ReadFileP, readFile_noFile)
    {
        OSEF::ReadFile readFile(GetParam().noFilename);
        std::string rf = "";
        EXPECT_FALSE(readFile.isOpened());
        EXPECT_FALSE(readFile.readFile(rf));
        EXPECT_FALSE(readFile.isOpened());
    }

    TEST_P(ReadFileP, readFile_noAuthorization)
    {
        OSEF::ReadFile readFile("/root/"+GetParam().readFilename);
        std::string rf = "";
        EXPECT_FALSE(readFile.isOpened());
        EXPECT_FALSE(readFile.readFile(rf));
        EXPECT_FALSE(readFile.isOpened());
    }

    TEST_P(ReadFileP, static_readLine)
    {
        std::string rl = "";
        EXPECT_TRUE(OSEF::ReadFile::readLine(GetParam().readFilename, rl));
        EXPECT_EQ(rl, longMultilineTextFirstLine);
    }

    TEST_P(ReadFileP, static_readLine_noFile)
    {
        std::string rl = "";
        EXPECT_FALSE(OSEF::ReadFile::readLine(GetParam().noFilename, rl));
    }

    TEST_P(ReadFileP, static_readLine_noAuthorization)
    {
        std::string rl = "";
        EXPECT_FALSE(OSEF::ReadFile::readLine("/root/"+GetParam().readFilename, rl));
    }

    TEST_P(ReadFileP, static_readFile)
    {
        std::string rf = "";
        EXPECT_TRUE(OSEF::ReadFile::readFile(GetParam().readFilename, rf));
        EXPECT_EQ(rf, longMultilineTextRef);
    }

    TEST_P(ReadFileP, static_readFile_noFile)
    {
        std::string rf = "";
        EXPECT_FALSE(OSEF::ReadFile::readFile(GetParam().noFilename, rf));
    }

    TEST_P(ReadFileP, static_readFile_noAuthorization)
    {
        std::string rf = "";
        EXPECT_FALSE(OSEF::ReadFile::readFile("/root/"+GetParam().readFilename, rf));
    }
}  // namespace OSEF
