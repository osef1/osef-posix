#include "ReadFileP.h"
#include "AppendFileP.h"
#include "OverwriteFileP.h"

#include <getopt.h>

namespace OSEF
{
    bool getFileNames(const int32_t& argc, char** argv)
    {
        bool ret = true;
        int option_index = 0;
        static struct option long_options[] =
        {
            {"gtest_output", required_argument, 0, 0},
            {0, 0, 0, 0 }
        };

        if (argc > 1)
        {
            int32_t opt = -1;
            do
            {
                opt = getopt_long(argc, argv, "f:r:o:a:", long_options, &option_index);
                if (opt != -1)
                {
                    switch (opt)
                    {
                        case 0:
                            break;
                        case 'f':
                            OSEF::ReadFileP::testParam.noFilename = optarg;
                            break;
                        case 'r':
                            OSEF::ReadFileP::testParam.readFilename = optarg;
                            break;
                        case 'o':
                            OSEF::OverwriteFileP::testParam.overwriteFilename = optarg;
                            break;
                        case 'a':
                            OSEF::AppendFileP::testParam.appendFilename = optarg;
                            break;
                        default:
                            std::cout << "Usage:   " << argv[0UL] << " [-option] [argument]" << std::endl;
                            std::cout << "option:  " << std::endl;
                            std::cout << "         " << "-f  Not a file test file name" << std::endl;
                            std::cout << "         " << "-r  Read test file name" << std::endl;
                            std::cout << "         " << "-o  Overwrite test file name" << std::endl;
                            std::cout << "         " << "-a  Append test file name" << std::endl;
                            ret = false;
                            break;
                    }
                }
            }while (opt != -1);
        }

        return ret;
    }
}  // namespace OSEF

int main(int argc, char **argv)
{
    int ret = -1;

    if (OSEF::getFileNames(argc, argv))
    {
        ::testing::InitGoogleTest(&argc, argv);
        ret = RUN_ALL_TESTS();
    }

    return ret;
}
