#include "AppendFileP.h"
#include "AppendFile.h"
#include "OverwriteFile.h"
#include "ReadFile.h"
#include "FileReference.h"

namespace OSEF
{
    AppendFileTestParam AppendFileP::testParam =
    {
        "OSEF_File_UT_append"
    };

    INSTANTIATE_TEST_SUITE_P(UserFiles, AppendFileP, testing::Values(AppendFileP::testParam));

    TEST_P(AppendFileP, writeString)
    {
        // overwrite first line
        EXPECT_TRUE(OSEF::OverwriteFile::overwriteString(GetParam().appendFilename, longMultilineTextFirstLine + '\n'));

        // append last line
        {
            OSEF::AppendFile appendFile(GetParam().appendFilename);
            EXPECT_FALSE(appendFile.isOpened());
            EXPECT_TRUE(appendFile.writeString(longMultilineTextLastLine + '\n'));
            EXPECT_TRUE(appendFile.isOpened());
        }

        // read first line
        OSEF::ReadFile readFile(GetParam().appendFilename);
        EXPECT_FALSE(readFile.isOpened());

        std::string rl = "";
        EXPECT_TRUE(readFile.readLine(rl));
        EXPECT_TRUE(readFile.isOpened());
        EXPECT_EQ(rl, longMultilineTextFirstLine);

        // read last line
        rl = "";
        EXPECT_TRUE(readFile.readLine(rl));
        EXPECT_EQ(rl, longMultilineTextLastLine);
    }

    TEST_P(AppendFileP, writeString_noAuthorization)
    {
        OSEF::AppendFile appendFile("/root/"+GetParam().appendFilename);
        EXPECT_FALSE(appendFile.isOpened());
        EXPECT_FALSE(appendFile.writeString(longMultilineTextLastLine + '\n'));
        EXPECT_FALSE(appendFile.isOpened());
    }

    TEST_P(AppendFileP, static_appendString)
    {
        // clear file
        EXPECT_TRUE(OSEF::OverwriteFile::overwriteString(GetParam().appendFilename, ""));

        // append file
        EXPECT_TRUE(OSEF::AppendFile::appendString(GetParam().appendFilename, longMultilineTextFirstLine));

        // read file
        std::string rl = "";
        EXPECT_TRUE(OSEF::ReadFile::readLine(GetParam().appendFilename, rl));
        EXPECT_EQ(rl, longMultilineTextFirstLine);
    }

    TEST_P(AppendFileP, static_appendString_noAuthorization)
    {
        EXPECT_FALSE(OSEF::AppendFile::appendString("/root/"+GetParam().appendFilename, longMultilineTextFirstLine));
    }
}  // namespace OSEF
