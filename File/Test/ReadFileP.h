#ifndef OSEFREADFILEP_H
#define OSEFREADFILEP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string noFilename;
        std::string readFilename;
    }ReadFileTestParam;

    class ReadFileP : public ::testing::TestWithParam<ReadFileTestParam>
    {
        protected:
            ReadFileP() {}
            ~ReadFileP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}

            static ReadFileTestParam testParam;
    };
}  // namespace OSEF

#endif  // OSEFREADFILEP_H
