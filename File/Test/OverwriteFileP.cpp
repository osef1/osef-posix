#include "OverwriteFileP.h"
#include "OverwriteFile.h"
#include "ReadFile.h"
#include "AppendFile.h"
#include "FileReference.h"

namespace OSEF
{
    OverwriteFileTestParam OverwriteFileP::testParam =
    {
        "OSEF_File_UT_overwrite"
    };

    INSTANTIATE_TEST_SUITE_P(UserFiles, OverwriteFileP, testing::Values(OverwriteFileP::testParam));

    TEST_P(OverwriteFileP, writeString)
    {
        // make sure file is not empty
        {
            // write some text
            EXPECT_TRUE(OSEF::AppendFile::appendString(GetParam().overwriteFilename, longMultilineTextRef));
        }

        // overwrite file
        {
            // write first line
            OSEF::OverwriteFile overwriteFile(GetParam().overwriteFilename);
            EXPECT_FALSE(overwriteFile.isOpened());

            EXPECT_TRUE(overwriteFile.writeString(longMultilineTextFirstLine + '\n'));
            EXPECT_TRUE(overwriteFile.isOpened());

            // write last line
            EXPECT_TRUE(overwriteFile.writeString(longMultilineTextLastLine + '\n'));
        }

        // read first line
        OSEF::ReadFile readFile(GetParam().overwriteFilename);
        EXPECT_FALSE(readFile.isOpened());

        std::string rl = "";
        EXPECT_TRUE(readFile.readLine(rl));
        EXPECT_TRUE(readFile.isOpened());
        EXPECT_EQ(rl, longMultilineTextFirstLine);

        // read last line
        rl = "";
        EXPECT_TRUE(readFile.readLine(rl));
        EXPECT_TRUE(readFile.isOpened());
        EXPECT_EQ(rl, longMultilineTextLastLine);

        // check end of line has been reached
        rl = "";
        EXPECT_FALSE(readFile.readLine(rl));
        EXPECT_TRUE(readFile.isOpened());
    }

    TEST_P(OverwriteFileP, writeString_noAuthorization)
    {
        OSEF::OverwriteFile overwriteFile("/root/"+GetParam().overwriteFilename);
        EXPECT_FALSE(overwriteFile.isOpened());
        EXPECT_FALSE(overwriteFile.writeString(longMultilineTextRef));
        EXPECT_FALSE(overwriteFile.isOpened());
    }

    TEST_P(OverwriteFileP, static_overwriteString)
    {
        // make sure file is not empty
        {
            // write some text
            EXPECT_TRUE(OSEF::AppendFile::appendString(GetParam().overwriteFilename, longMultilineTextLastLine));
        }

        // overwrite file
        EXPECT_TRUE(OSEF::OverwriteFile::overwriteString(GetParam().overwriteFilename, longMultilineTextRef));

        // read file
        std::string rf = "";
        EXPECT_TRUE(OSEF::ReadFile::readFile(GetParam().overwriteFilename, rf));
        EXPECT_EQ(rf, longMultilineTextRef);
    }

    TEST_P(OverwriteFileP, static_overwriteString_noAuthorization)
    {
        EXPECT_FALSE(OSEF::OverwriteFile::overwriteString("/root/"+GetParam().overwriteFilename, longMultilineTextRef));
    }
}  // namespace OSEF
