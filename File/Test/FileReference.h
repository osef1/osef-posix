#ifndef OSEFFILEREFERENCE_H
#define OSEFFILEREFERENCE_H

#include <string>

namespace OSEF
{
    const std::string longMultilineTextFirstLine =
    "See my chariot, run to your ships";

    const std::string longMultilineTextLastLine =
    "More blood will be spilled, more will be killed";

    const std::string longMultilineTextRef =
    longMultilineTextFirstLine+"\n"
    "I'll drive you back to the sea\n"
    "You came here for gold, the wall will not hold\n"
    "This day was promised to me\n"
    "The Gods are my shield, my fate has been sealed\n"
    "Lightning and javelins fly\n"
    "Soon many will fall, we are storming the wall\n"
    "Stones fall like snow from the sky\n"
    "\n"
    "We will pay with our glory in the fire of battle\n"
    "Zeus, today is mine\n"
    "Killing all in my way like sheep and like cattle\n"
    "Smashing skulls of all who defy\n"
    "I spare not the hammer, I spare not the sword\n"
    "This day will ring with my name\n"
    "None have to chase me, let he who will face me\n"
    "Kill me or die by the sword\n"
    "\n"
    "Oh friend of mine, how to say goodbye?\n"
    "This was your time, but the armor you wore was mine\n"
    "I will not rest until Hector's blood is spilled\n"
    "His bones will all be broken, dragged across the field\n"
    "This, dear friend, is how we'll say goodbye\n"
    "Until we meet in the sky\n"
    "\n"
    "Here inside the walls of Troy, the Gods weigh my fate\n"
    "From this day do I abstain, to a memory of hate\n"
    "To pay for all the blood that spilled\n"
    "Many thousands I did kill\n"
    "No walls can contain the Gods' almighty will\n"
    "\n"
    "I hear the silent voices, I cannot hide\n"
    "The Gods leave no choices, so we all must die\n"
    "Oh Achilles, let thy arrows fly, into the wind\n"
    "Where eagles cross the sky\n"
    "Today my mortal blood will mix with sand\n"
    "It was foretold I will die by thy hand\n"
    "Into Hades my soul descend\n"
    "\n"
    "Cowards in the grip of fear, no valor to uphold\n"
    "Cut into the earth, with honor long been sold\n"
    "For all shall come to know me\n"
    "As they fall unto their knees\n"
    "Zeus the Thunderer, control my destiny\n"
    "\n"
    "When the cards of life were dealt\n"
    "My hand a ruthless fate\n"
    "To avenge and bringeth fury\n"
    "Hector, feel my hate\n"
    "A bloodbath I was born to bring, by birth I'm an assassin\n"
    "To cut the cord of life and death\n"
    "Ties to earth unfasten\n"
    "\n"
    "Blood and fire, death and hate\n"
    "Your body I will desecrate\n"
    "Dogs and vultures eat your flesh\n"
    "The Hall of Hades waits\n"
    "Kill...\n"
    "\n"
    "Hector's blood lies on the battlefield\n"
    "His body's mangled wounds\n"
    "The Gods who once protected him are now his Gods of doom\n"
    "Like a tower standing tall, steadfast in direction\n"
    "I fall upon you bringing death, the Gods give no protection\n"
    "\n"
    "Cowards in the grip of fear, no valor to uphold\n"
    "Cut into the earth, with honor long been sold\n"
    "For all shall come to know me\n"
    "As they fall unto their knees\n"
    "Zeus the Thunderer, control my destiny\n"
    "\n"
    "Blood and fire death and hate\n"
    "Your body I will desecrate\n"
    "Dogs and vultures eat your flesh\n"
    "The Hall of Hades waits\n"
    "Die...\n"
    "\n"
    "The oath of the Gods, this day was fulfilled\n"
    "In the heat of the battle, Hector was killed\n"
    "See him Patroclus, down in the dust\n"
    "Rejoice in his death my symbol of trust\n"
    "\n"
    "A dozen highborn youths have been killed\n"
    "Cutting their throats, their blood was all spilled\n"
    "Their bodies set at the foot of your fire\n"
    "With oxen, sheep and two of your hounds\n"
    "\n"
    "Your funeral pyre high off the ground\n"
    "Hector's body dragged three times around\n"
    "I will carry the torch to your funeral pyre\n"
    "I will ask of the wind to send high your fire\n"
    "\n"
    "Hector's blood will not be washed from my body\n"
    "Until your body is burned\n"
    "A prophecy spoken, a promise fulfilled\n"
    +longMultilineTextLastLine+"\n";
}  // namespace OSEF

#endif  // OSEFFILEREFERENCE_H
