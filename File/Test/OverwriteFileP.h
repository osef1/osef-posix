#ifndef OSEFOVERWRITEFILEP_H
#define OSEFOVERWRITEFILEP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string overwriteFilename;
    }OverwriteFileTestParam;

    class OverwriteFileP : public ::testing::TestWithParam<OverwriteFileTestParam>
    {
        protected:
            OverwriteFileP(){}
            ~OverwriteFileP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}

            static OverwriteFileTestParam testParam;
    };
}  // namespace OSEF

#endif  // OSEFOVERWRITEFILEP_H
