#ifndef OSEFAPPENDFILEP_H
#define OSEFAPPENDFILEP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string appendFilename;
    }AppendFileTestParam;

    class AppendFileP : public ::testing::TestWithParam<AppendFileTestParam>
    {
        protected:
            AppendFileP(){}
            ~AppendFileP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}

            static AppendFileTestParam testParam;
    };
}  // namespace OSEF

#endif  // OSEFAPPENDFILEP_H
