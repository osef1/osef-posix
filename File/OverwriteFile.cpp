#include "OverwriteFile.h"

OSEF::OverwriteFile::OverwriteFile(const std::string& filename)
    :WriteFile(filename, std::fstream::trunc) {}

bool OSEF::OverwriteFile::overwriteString(const std::string& filename, const std::string& s)
{
    OverwriteFile f(filename);
    return f.writeString(s);
}
