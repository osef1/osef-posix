#include "AppendFile.h"

OSEF::AppendFile::AppendFile(const std::string& filename)
    :WriteFile(filename, std::fstream::app) {}

bool OSEF::AppendFile::appendString(const std::string& filename, const std::string& s)
{
    AppendFile f(filename);
    return f.writeString(s);
}
