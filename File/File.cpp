#include "File.h"

OSEF::File::File(const std::string& filename, const std::ios_base::openmode& filemode)
    :name(filename),
    mode(filemode),
    opened(false) {}

OSEF::File::~File()
{
    fs.close();
}

bool OSEF::File::openFile()
{
    if (not opened)
    {
        fs.open(name, mode);
        if (fs.good())
        {
            opened = true;
        }
        else
        {
            F_DERR("error opening");
        }
    }

    return opened;
}
