#include "ReadFile.h"
#include "Debug.h"

OSEF::ReadFile::ReadFile(const std::string& filename)
    :File(filename, std::fstream::in) {}

bool OSEF::ReadFile::readLine(std::string& s)
{
    bool ret = false;

    if (openFile())
    {
        std::string line;
        std::getline(getFstream(), line);

        s.append(line);

        if (getFstream().good())
        {
            ret = true;
        }
        else
        {
            if (not getFstream().bad() && getFstream().eof())
            {
                // end of file
                if (not line.empty())
                {
                    ret = true;
                }
            }
            else
            {
                F_DERR("error reading");
            }
        }
    }

    return ret;
}

bool OSEF::ReadFile::readFile(std::string& s)
{
    bool ret = false;

    if (openFile())
    {
        s.append(std::istreambuf_iterator<char>(getFstream()), std::istreambuf_iterator<char>());

        ret = true;
    }

    return ret;
}

bool OSEF::ReadFile::readLine(const std::string& filename, std::string& s)
{
    ReadFile f(filename);
    return f.readLine(s);
}

bool OSEF::ReadFile::readFile(const std::string& filename, std::string& s)
{
    ReadFile f(filename);
    return f.readFile(s);
}
