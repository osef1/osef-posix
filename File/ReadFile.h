#ifndef OSEFREADFILE_H
#define OSEFREADFILE_H

#include <string>

#include "File.h"

namespace OSEF
{
    class ReadFile : public File
    {
    public:
        explicit ReadFile(const std::string& filename);
        ~ReadFile() override = default;

        bool readLine(std::string& s);  ///< concatenate file's next line to string
        bool readFile(std::string& s);  ///< concatenate whole file to string

        static bool readLine(const std::string& filename, std::string& s);  ///< concatenate file's first line to string
        static bool readFile(const std::string& filename, std::string& s);  ///< concatenate whole file to string

        ReadFile(const ReadFile&) = delete;  // copy constructor
        ReadFile& operator=(const ReadFile&) = delete;  // copy assignment
        ReadFile(ReadFile&&) = delete;  // move constructor
        ReadFile& operator=(ReadFile&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* OSEFREADFILE_H */
