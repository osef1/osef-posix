#ifndef OSEFWRITEFILE_H
#define OSEFWRITEFILE_H

#include <string>

#include "File.h"

namespace OSEF
{
    class WriteFile : public File
    {
    public:
        explicit WriteFile(const std::string& filename, const std::ios_base::openmode& filemode);
        ~WriteFile() override = default;

        bool writeString(const std::string& s);  ///< write string to file, append or overwrite depending on mode

        WriteFile(const WriteFile&) = delete;  // copy constructor
        WriteFile& operator=(const WriteFile&) = delete;  // copy assignment
        WriteFile(WriteFile&&) = delete;  // move constructor
        WriteFile& operator=(WriteFile&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* OSEFWRITEFILE_H */
