#ifndef OSEFFILE_H
#define OSEFFILE_H

#include "Debug.h"

#include <string>
#include <fstream>

#ifndef F_DERR
   #ifdef DEBUG
      #define F_DERR(s) DERR(s << " file " << getName() << " fail " << getFstream().fail() << " bad " << getFstream().bad() << " eof " << getFstream().eof())
   #else
      #define F_DERR(s)
   #endif
#endif

namespace OSEF
{
    class File
    {
    public:
        File(const std::string& filename, const std::ios_base::openmode& filemode);
        virtual ~File();

        bool isOpened() const {return opened;}
        std::string getName() const {return name;}

        File(const File&) = delete;  // copy constructor
        File& operator=(const File&) = delete;  // copy assignment
        File(File&&) = delete;  // move constructor
        File& operator=(File&&) = delete;  // move assignment

    protected:
        bool openFile();
        std::fstream& getFstream() {return fs;}

    private:
        std::fstream fs;
        std::string name;
        std::ios_base::openmode mode;
        bool opened;
    };
}  // namespace OSEF

#endif /* OSEFFILE_H */
