#ifndef OSEFOVERWRITEFILE_H
#define OSEFOVERWRITEFILE_H

#include <string>

#include "WriteFile.h"

namespace OSEF
{
    class OverwriteFile : public WriteFile
    {
    public:
        explicit OverwriteFile(const std::string& filename);
        ~OverwriteFile() override = default;

        static bool overwriteString(const std::string& filename, const std::string& s);  ///< overwrite whole file with string

        OverwriteFile(const OverwriteFile&) = delete;  // copy constructor
        OverwriteFile& operator=(const OverwriteFile&) = delete;  // copy assignment
        OverwriteFile(OverwriteFile&&) = delete;  // move constructor
        OverwriteFile& operator=(OverwriteFile&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* OSEFOVERWRITEFILE_H */
