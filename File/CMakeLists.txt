cmake_minimum_required(VERSION 3.13)

include(../osef.cmake)

project(osef-file)

# Convert TEST before adding subdirectories
if (TEST)
    # Reset TEST to avoid building subdirectories tests
    SET(TEST "off")
    SET(FILE-TEST "on")
endif ()

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME}
    PRIVATE
        File.cpp
        ReadFile.cpp
        WriteFile.cpp
        AppendFile.cpp
        OverwriteFile.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        .
        ../osef-log/Debug
)

# Add test
if (FILE-TEST OR GLOBAL-TEST)
    add_subdirectory(Test)
    enable_testing ()
    add_test(NAME osef-file-gtest-suite  
            COMMAND osef-file-gtest
    )
endif ()
