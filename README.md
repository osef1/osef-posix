# OSEF-POSIX
```plantuml
title OSEF-POSIX classes

package <<Mutex>> {
    class Mutex
}

package <<Time>> {
    class TimeOut
}

package <<File>> {
    class File
    class ReadFile
    class WriteFile
    class AppendFile
    class OverwriteFile
}

ReadFile -up-|> File
WriteFile -up-|> File
AppendFile -up-|> WriteFile
OverwriteFile -up-|> WriteFile


package <<Random>> {
    class Randomizer
}

package <<MsgQ>> {
    class MsgQDirectory
    class MsgQNonBlockingReceiver
    class MsgQNonBlockingStringReceiver
    class MsgQBlockingStringReceiver
    class "MsgQNonBlockingTypeReceiver<T>" as MsgQNonBlockingTypeReceiver_T
    class MsgQNonBlockingSender
    class MsgQNonBlockingStringSender
    class "MsgQNonBlockingTypeSender<T>" as MsgQNonBlockingTypeSender_T
}

MsgQDirectory .up. Randomizer
MsgQDirectory -down-o MsgQNonBlockingReceiver
MsgQNonBlockingStringReceiver -up-|> MsgQNonBlockingReceiver
MsgQBlockingStringReceiver -up-|> MsgQNonBlockingStringReceiver
MsgQNonBlockingTypeReceiver_T -up-|> MsgQNonBlockingReceiver
MsgQNonBlockingStringSender -up-|> MsgQNonBlockingSender
MsgQNonBlockingTypeSender_T -up-|> MsgQNonBlockingSender

package <<Thread>> {
    class "MsgQNonBlockingTypeReceiver<ThreadSubMsg>" as MsgQNonBlockingTypeReceiver_TSM
    class Threader
}

MsgQNonBlockingReceiver <|-down- MsgQNonBlockingTypeReceiver_TSM
MsgQNonBlockingTypeReceiver_TSM -down-o Threader

package <<Socket>> {
    class SocketToolbox

    class NetworkEndpoint
    
    class StreamEndpoint
    class TcpClient
    class TcpServer
    class TcpListener
    class TcpApplication
    class TcpListenerThread
    class UnixClient
    class UnixServer
    class UnixListener
    
    class UdpEndpoint
    class UdpClient
    class UdpServer
    class UdpMulticastClient
    class UdpMulticastServer
    
    class NetworkSession
}

NetworkEndpoint -up-o NetworkSession
StreamEndpoint -up-|> NetworkEndpoint
TcpClient -up-|> StreamEndpoint
TcpServer -up-|> StreamEndpoint
UnixClient -up-|> StreamEndpoint
UnixServer -up-|> StreamEndpoint

TcpListenerThread ... TcpListener
TcpListenerThread o-up- Threader
TcpListenerThread o-up- TcpApplication

UdpEndpoint -up-|> NetworkEndpoint
UdpClient -up-|> UdpEndpoint
UdpServer -up-|> UdpEndpoint
UdpMulticastClient -up-|> UdpClient
UdpMulticastServer -up-|> UdpServer
```

# Clone
```
git clone --recursive https://gitlab.com/fredloreaud/osef-posix.git
```
:information_source: All following notes will assume you cloned this repo then cd to its root
# Build whole library
## CMake
```
./build_release.sh
```
## NetBeans
### Local host
* Open project osef-posix/NetBeans/OSEF_POSIX
* Right-click project > Build
### Remote host
* Open project osef-posix/NetBeans/OSEF_POSIX
* Right-click project > Set build host > [YourRemoteHost]
* Right-click project > Build
#### Remote host addition
* Right-click project > Build > Set build host > Manage hosts ... > Add ...
  * Type remote host IP address as Hostname
* Next
  * Change "Login" to root
* Next
  * Enter password
* OK
* Change "Access project files via:" to SFTP
* Finish

# Build Single sub-folder
Available sub-folders are:
* File
* MsgQ
* Mutex
* Random
* Signal
* Socket
* Thread
* Time

## CMake
```
cd [SubFolder]
../build_release.sh
```

# Test whole library
## CMake
```
cd Test
../build_release.sh
./build/Release/OSEF_POSIX_GTest
```
### Coverage report
```
cd Test
../build_coverage.sh
./coverage_html.sh
firefox ./build/Coverage/OSEF_POSIX_GTest.html
```

## NetBeans
### Clone git_clone_gtest
```
./git_clone_gtest
```
### Local host
* Open project osef-posix/Test/NetBeans/OSEF_POSIX_GTest
* Right-click project > Run
### Remote host
* Open project osef-posix/Test/NetBeans/OSEF_POSIX_GTest
* Right-click project > Set build host > [YourRemoteHost]
* Right-click project > Run

# Test single sub-folder
## CMake
```
cd [SubFolder]/Test
../../release_build.sh
./build/Release/OSEF_[SubFolder]_GTest
```
### Coverage report
```
cd [SubFolder]/Test
../../coverage_build.sh
./coverage_html.sh
firefox ./build/Coverage/OSEF_[SubFolder]_GTest.html
```

