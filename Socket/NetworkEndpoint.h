#ifndef OSEFNETWORKENDPOINT_H
#define OSEFNETWORKENDPOINT_H

#include <string>

namespace OSEF
{
    class NetworkEndpoint
    {
    public:
        NetworkEndpoint() = default;
        virtual ~NetworkEndpoint() = default;

        bool sendString(const std::string& s);
        bool receiveString(std::string& s, const size_t& size);
        bool receiveString(std::string& s, const size_t& size, const timespec& to);

        NetworkEndpoint(const NetworkEndpoint&) = delete;  // copy constructor
        NetworkEndpoint& operator=(const NetworkEndpoint&) = delete;  // copy assignment
        NetworkEndpoint(NetworkEndpoint&&) = delete;  // move constructor
        NetworkEndpoint& operator=(NetworkEndpoint&&) = delete;  // move assignment

    private:
        virtual bool connectNetwork() = 0;

        virtual bool sendToNet(void const* buffer, const size_t& size) = 0;
        virtual bool receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL) = 0;
        virtual bool receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL, const timespec& to) = 0;
    };
}  // namespace OSEF

#endif /* OSEFNETWORKENDPOINT_H */
