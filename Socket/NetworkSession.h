#ifndef OSEFNETWORKSESSION_H
#define OSEFNETWORKSESSION_H

#include <string>

#include "NetworkEndpoint.h"

namespace OSEF
{
    class NetworkSession
    {
    public:
        explicit NetworkSession(OSEF::NetworkEndpoint* ep);
        virtual ~NetworkSession() = default;

        bool sendString(const std::string& s);  ///< send string after session setup
        bool receiveString(std::string& s, const size_t& size);  ///< receive string after session setup
        bool receiveString(std::string& s, const size_t& size, const timespec& to);

        virtual bool tearDown() {return true;}

        NetworkSession(const NetworkSession&) = delete;  // copy constructor
        NetworkSession& operator=(const NetworkSession&) = delete;  // copy assignment
        NetworkSession(NetworkSession&&) = delete;  // move constructor
        NetworkSession& operator=(NetworkSession&&) = delete;  // move assignment

    protected:
        virtual bool setUp() {return true;}

        virtual bool validate(const std::string& s);

    private:
        OSEF::NetworkEndpoint* endpoint;
    };
}  // namespace OSEF

#endif /* OSEFNETWORKSESSION_H */
