#include "UdpClient.h"
#include "SocketToolbox.h"
#include "Swap.h"
#include "Debug.h"

#include <arpa/inet.h>  // inet_aton

// https://github.com/kallisti5/libmicrossdp/blob/master/ssdp.c

OSEF::UdpClient::UdpClient(const std::string& hst, const std::string& prt)
    :UdpEndpoint(hst, prt) {}

bool OSEF::UdpClient::connectNetwork()
{
    bool ret = false;

    if (getSocket() <= 0)
    {
        // get client socket to host:port
        if (getIPv4UDPSocket()
            && (getIPv4SocketAddress(getHost(), getPort(), getHostAddress())))
        {
            DOUT("UDP unicast client linked to " << getHost() << ":" << getPort());
            ret = true;
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::UdpClient::getIPv4SocketAddress(const std::string& hst, const std::string& prt, sockaddr_in& sa)
{
    bool ret = false;

    memset(reinterpret_cast<char*>(&sa), 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = hton16(std::stoi(prt));
    if (inet_aton(hst.c_str(), &(sa.sin_addr)) != 0)
    {
        ret = true;

        DOUT("Get IPv4 socket address to " << hst << ":" << prt);
    }
    else
    {
        DERR("error converting IP string " << hst << " to network address");
    }

    return ret;
}
