#ifndef OSEFUDPMULTICASTSERVER_H
#define OSEFUDPMULTICASTSERVER_H

#include "UdpServer.h"

#include <string>

namespace OSEF
{
    class UdpMulticastServer : public UdpServer
    {
    public:
        UdpMulticastServer(const std::string& hst, const std::string& prt, const std::string& lclhst);
        ~UdpMulticastServer() override = default;

        UdpMulticastServer(const UdpMulticastServer&) = delete;  // copy constructor
        UdpMulticastServer& operator=(const UdpMulticastServer&) = delete;  // copy assignment
        UdpMulticastServer(UdpMulticastServer&&) = delete;  // move constructor
        UdpMulticastServer& operator=(UdpMulticastServer&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;
        bool getLocalIpv4(std::string& ip);
        bool subscribeMulticastGroup(const std::string& localIp);

        std::string localHost;
    };
}  // namespace OSEF

#endif /* OSEFUDPMULTICASTSERVER_H */
