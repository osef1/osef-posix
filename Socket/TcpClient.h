#ifndef OSEFTCPCLIENT_H
#define OSEFTCPCLIENT_H

#include "StreamEndpoint.h"

#include <string>

namespace OSEF
{
    class TcpClient : public StreamEndpoint
    {
    public:
        TcpClient(const std::string& hst, const std::string& prt);
        ~TcpClient() override;

        std::string getHost() const {return host;}
        std::string getPort() const {return port;}

        TcpClient(const TcpClient&) = delete;  // copy constructor
        TcpClient& operator=(const TcpClient&) = delete;  // copy assignment
        TcpClient(TcpClient&&) = delete;  // move constructor
        TcpClient& operator=(TcpClient&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;

        std::string host;
        std::string port;
    };
}  // namespace OSEF

#endif /* OSEFTCPCLIENT_H */
