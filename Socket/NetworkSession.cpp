#include "NetworkSession.h"
#include "Debug.h"

OSEF::NetworkSession::NetworkSession(OSEF::NetworkEndpoint* ep)
    :endpoint(ep) {}

bool OSEF::NetworkSession::validate(const std::string& s)
{
    return not s.empty();
}

bool OSEF::NetworkSession::sendString(const std::string& s)
{
    bool ret = false;

    if (endpoint != nullptr)
    {
        if (setUp())
        {
            ret = endpoint->sendString(s);
        }
    }
    else
    {
        DERR("endpoint null");
    }

    return ret;
}

bool OSEF::NetworkSession::receiveString(std::string& s, const size_t& size)
{
    bool ret = false;

    if (endpoint != nullptr)
    {
        if (setUp() && endpoint->receiveString(s, size))
        {
            ret = validate(s);
        }
    }
    else
    {
        DERR("endpoint null");
    }

    return ret;
}

bool OSEF::NetworkSession::receiveString(std::string& s, const size_t& size, const timespec& to)
{
    bool ret = false;

    if (endpoint != nullptr)
    {
        if (setUp() && endpoint->receiveString(s, size, to))
        {
            ret = validate(s);
        }
    }
    else
    {
        DERR("endpoint null");
    }

    return ret;
}
