#include "TcpServer.h"

OSEF::TcpServer::TcpServer(const int32_t& sock)
    :StreamEndpoint(sock) {}

bool OSEF::TcpServer::connectNetwork()
{
    bool ret = false;

    if (getSocket() > 0)
    {
        ret = true;
    }

    return ret;
}
