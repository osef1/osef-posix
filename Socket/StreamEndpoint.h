#ifndef OSEFSTREAMENDPOINT_H
#define OSEFSTREAMENDPOINT_H

#include "NetworkEndpoint.h"

namespace OSEF
{
    class StreamEndpoint : public NetworkEndpoint
    {
    public:
        explicit StreamEndpoint(const int32_t& sock);  ///< server constructor with reception socket
        ~StreamEndpoint() override;

        StreamEndpoint(const StreamEndpoint&) = delete;  // copy constructor
        StreamEndpoint& operator=(const StreamEndpoint&) = delete;  // copy assignment
        StreamEndpoint(StreamEndpoint&&) = delete;  // move constructor
        StreamEndpoint& operator=(StreamEndpoint&&) = delete;  // move assignment

    protected:
        int32_t& getSocket() {return socketFD;}

    private:
        bool sendToNet(void const* buffer, const size_t& size) override;
        bool receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL) override;
        bool receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL, const timespec& to) override;

        int32_t socketFD;
    };
}  // namespace OSEF

#endif /* OSEFSTREAMENDPOINT_H */
