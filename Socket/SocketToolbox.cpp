#include "SocketToolbox.h"
#include "SocketTypes.h"  // OSEF_SOCK_NAME_LEN
#include "Debug.h"

#include <unistd.h>  // close
#include <netdb.h>  // addrinfo

bool OSEF::SocketToolbox::getHostStreamSocket(const std::string& name, int32_t& sock, sockaddr_un& address)
{
    bool ret = false;

    if (name.size() < OSEF_SOCK_NAME_LEN)
    {
        memset(&address, 0, sizeof(sockaddr_un));
        address.sun_family = AF_UNIX;
        memcpy(&address.sun_path[0], name.c_str(), name.size());

        sock = socket(AF_UNIX, SOCK_STREAM, 0);
        if (sock != -1)
        {
            ret = true;
        }
        else
        {
            DERR("error creating UNIX TCP socket");
        }
    }
    else
    {
        DERR("error UNIX socket name too long " << name.size() << ">" << OSEF_SOCK_NAME_LEN);
    }

    return ret;
}

bool OSEF::SocketToolbox::getHostStreamSocket(const std::string& host, const std::string& port, int32_t& sock, sockaddr& address)
{
    bool ret = false;

    struct addrinfo hints{};
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    struct addrinfo* addr_list = nullptr;
    if (getaddrinfo(host.c_str(), port.c_str(), &hints, &addr_list) == 0)
    {
        do
        {
            if (addr_list->ai_family == AF_INET)  // Choosing IPv4 over IPv6
            {
                sock = socket(addr_list->ai_family, addr_list->ai_socktype, addr_list->ai_protocol);
                if (sock != -1)
                {
                    memcpy(&address, addr_list->ai_addr, sizeof(sockaddr));
                    ret = true;
                }
                else
                {
                    DERR("error creating IPv4 TCP socket");
                }
            }

            addr_list = addr_list->ai_next;
        }while ((addr_list != nullptr) && (not ret));

        freeaddrinfo(addr_list);
    }
    else
    {
        DERR("unknown network host " << host << ":" << port);
    }

    return ret;
}

bool OSEF::SocketToolbox::setOption(const int32_t& sock, const int32_t& level, const int32_t& optName)
{
    bool ret = false;

    int32_t on = 1;
    if (setsockopt(sock, level, optName, static_cast<void*>(&on), static_cast<socklen_t>(sizeof(on))) == 0)
    {
        ret = true;
    }
    else
    {
        DERR("error setting socket " << sock << " option " << optName << " " << strerror(errno));
    }

    return ret;
}

bool OSEF::SocketToolbox::resetOption(const int32_t& sock, const int32_t& level, const int32_t& optName)
{
    bool ret = false;

    int32_t off = 0;
    if (setsockopt(sock, level, optName, static_cast<void*>(&off), static_cast<socklen_t>(sizeof(off))) == 0)
    {
        ret = true;
    }
    else
    {
        DERR("error resetting socket " << sock << " option " << optName << " " << strerror(errno));
    }

    return ret;
}

bool OSEF::SocketToolbox::setSocketOption(const int32_t& sock, const int32_t& optName)
{
    return setOption(sock, OSEF_SOL_SOCKET, optName);
}

bool OSEF::SocketToolbox::setTcpOption(const int32_t& sock, const int32_t& optName)
{
    return setOption(sock, OSEF_IPPROTO_TCP, optName);
}

void OSEF::SocketToolbox::closeSocket(int32_t& sock, const std::string& name)
{
    if (sock != -1)
    {
        if (close(sock) == 0)
        {
            sock = -1;
        }
        else
        {
            DERR("error closing socket " << sock);
        }

        if ((name.size() < SUN_PATH_MAX_SIZE) && (unlink(name.c_str()) != 0))
        {
            DERR("error unlinking Unix socket " << name);
        }
    }
}

void OSEF::SocketToolbox::closeSocket(int32_t& sock)
{
    if (sock != -1)
    {
        if (close(sock) == 0)
        {
            sock = -1;
        }
        else
        {
            DERR("error closing socket " << sock);
        }
    }
}

void OSEF::SocketToolbox::shutdownSocket(int32_t& sock)
{
    // shutdown only valid socket
    // ENOTCONN means already disconnected, not an error
    if ((sock != -1)
        && (shutdown(sock, OSEF_SHUT_RDWR) != 0)
        && (errno != ENOTCONN))
    {
        DERR("error shutting down socket " << sock << " errno " << errno << " " << strerror(errno));
    }
}
