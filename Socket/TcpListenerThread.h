#ifndef OSEFTCPLISTENERTHREAD_H
#define OSEFTCPLISTENERTHREAD_H

#include "TcpListener.h"
#include "Threader.h"
#include "TcpApplication.h"

#include <string>

namespace OSEF
{
    class TcpListenerThread
    {
    public:
        TcpListenerThread(const std::string& hst, const std::string& prt, const size_t& threads, TcpApplication* app);
        virtual ~TcpListenerThread() = default;

        bool spawnListenRoutine();

        size_t getOccupation() {return threader.getOccupation();}

        TcpListenerThread(const TcpListenerThread&) = delete;  // copy constructor
        TcpListenerThread& operator=(const TcpListenerThread&) = delete;  // copy assignment
        TcpListenerThread(TcpListenerThread&&) = delete;  // move constructor
        TcpListenerThread& operator=(TcpListenerThread&&) = delete;  // move assignment

    private:
        static void* listenRoutine(TcpListener* lstnr, Threader* thrdr, TcpApplication* app);

        static void serveRoutine(int32_t clientSocket, TcpApplication* app);

        // listener must be created out of listenRoutine
        // so that it is destroyed if thread is cancelled
        // otherwise listenSocket is not closed properly
        TcpListener listener;
        Threader threader;
        TcpApplication* application;
    };
}  // namespace OSEF

#endif /* OSEFTCPLISTENERTHREAD_H */
