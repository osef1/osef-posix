#ifndef OSEFUNIXLISTENER_H
#define OSEFUNIXLISTENER_H

#include "SocketTypes.h"  // IPv4AddressString

#include <string>

namespace OSEF
{
    class UnixListener
    {
    public:
        explicit UnixListener(const std::string& n);
        virtual ~UnixListener();

        bool acceptClient(int32_t& clientSock);

        UnixListener(const UnixListener&) = delete;  // copy constructor
        UnixListener& operator=(const UnixListener&) = delete;  // copy assignment
        UnixListener(UnixListener&&) = delete;  // move constructor
        UnixListener& operator=(UnixListener&&) = delete;  // move assignment

    private:
        bool setUpListener();
        bool getBindedStreamSocket();

        const int32_t OSEF_LISTEN_MAX_CLIENT = 1;  // N connection requests will be queued before further requests are refused

        std::string name;
        int32_t listenSocket;
    };
}  // namespace OSEF

#endif /* OSEFUNIXLISTENER_H */
