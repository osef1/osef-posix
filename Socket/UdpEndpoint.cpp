#include "UdpEndpoint.h"
#include "SocketToolbox.h"
#include "Debug.h"

OSEF::UdpEndpoint::UdpEndpoint(const std::string& hst, const std::string& prt)
    :host(hst),
    port(prt),
    socketFD(-1),
    hostAddress()
{
    memset(reinterpret_cast<char*>(&hostAddress), 0, sizeof(hostAddress));
}

OSEF::UdpEndpoint::~UdpEndpoint()
{
    SocketToolbox::closeSocket(socketFD);
}

bool OSEF::UdpEndpoint::sendToNet(void const* buffer, const size_t& size)
{
    bool ret = false;

    const ssize_t sendL = sendto(socketFD, buffer, size, 0, (struct sockaddr *)&hostAddress, sizeof(hostAddress));

    // negative sendL becomes positive after size_t cast
    // hence >= 0 test is necessary
    if ((sendL >= static_cast<ssize_t>(0))
        && (static_cast<size_t>(sendL) == size))
    {
        ret = true;
    }

    if (not ret)
    {
        SocketToolbox::closeSocket(socketFD);
        DERR("error sending to socket" << socketFD);
    }

    return ret;
}

bool OSEF::UdpEndpoint::receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL)
{
    bool ret = false;

    recvL = recvfrom(socketFD, buffer, size, 0, (struct sockaddr *)getReceptionAddress(), getReceptionAddressLength());
    if (recvL > 0L)
    {
        ret = true;
    }
    else
    {
        #ifdef DEBUG
        if (recvL == 0)
        {
            DOUT("datagram socket " << socketFD << " gracefully closed");
        }
        else
        {
            DERR("error receiving from socket " << socketFD << " " << strerror(errno));
        }
        #endif

        SocketToolbox::closeSocket(socketFD);
    }

    return ret;
}

bool OSEF::UdpEndpoint::receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL, const timespec& to)
{
    bool ret = false;

    if (socketFD < FD_SETSIZE)
    {
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(socketFD, &readfds);

        const int32_t ps = pselect(socketFD + 1, &readfds, nullptr, nullptr, &to, nullptr);
        if (ps == 1)
        {
            ret = receiveFromNet(buffer, size, recvL);
        }
        else
        {
            if (ps != 0)
            {
                DERR("select read error returned " << ps);
            }
        }
    }
    else
    {
        DERR("socket overflow");
    }

    return ret;
}

bool OSEF::UdpEndpoint::getIPv4UDPSocket()
{
    bool ret = false;

    socketFD = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socketFD != -1)
    {
        DOUT("Get IPv4 UDP socket " << socketFD);
        ret = true;
    }
    else
    {
        DERR("error creating IPv4 UDP socket");
    }

    return ret;
}
