#include "StreamEndpoint.h"
#include "SocketToolbox.h"
#include "Debug.h"

OSEF::StreamEndpoint::StreamEndpoint(const int32_t& sock)
    :socketFD(sock) {}

OSEF::StreamEndpoint::~StreamEndpoint()
{
    SocketToolbox::shutdownSocket(socketFD);
    SocketToolbox::closeSocket(socketFD);
}

bool OSEF::StreamEndpoint::sendToNet(void const* buffer, const size_t& size)
{
    bool ret = false;

    const ssize_t sendL = send(socketFD, buffer, size, 0);

    // negative sendL becomes positive after size_t cast
    // hence >= 0 test is necessary
    if ((sendL >= static_cast<ssize_t>(0))
        && (static_cast<size_t>(sendL) == size))
    {
        ret = true;
    }

    if (not ret)
    {
        DERR("error sending to socket " << socketFD);
        SocketToolbox::closeSocket(socketFD);
    }

    return ret;
}

bool OSEF::StreamEndpoint::receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL)
{
    bool ret = false;

    recvL = recv(socketFD, buffer, size, 0);
    if (recvL > 0L)
    {
        ret = true;
    }
    else
    {
        if (recvL == 0)
        {
            DOUT("stream socket " << socketFD << " gracefully closed");
            SocketToolbox::closeSocket(socketFD);
        }
        else
        {
            if ((errno != EWOULDBLOCK) && (errno != EAGAIN))
            {
                DWARN("error receiving from socket " << socketFD << " errno " << errno << " " << strerror(errno));
                SocketToolbox::closeSocket(socketFD);
            }
        }
    }

    return ret;
}

bool OSEF::StreamEndpoint::receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL, const timespec& to)
{
    bool ret = false;

    if (socketFD < FD_SETSIZE)
    {
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(socketFD, &readfds);

        const int32_t ps = pselect(socketFD + 1, &readfds, nullptr, nullptr, &to, nullptr);
        if (ps == 1)
        {
            ret = receiveFromNet(buffer, size, recvL);
        }
        else
        {
            if (ps != 0)
            {
                DERR("select read error returned " << ps);
            }
        }
    }
    else
    {
        DERR("socket overflow");
    }

    return ret;
}
