#include "UnixListener.h"
#include "SocketToolbox.h"  // getSocketAddress
#include "Debug.h"

#include <arpa/inet.h>  // inet_ntoa

OSEF::UnixListener::UnixListener(const std::string& n)
    :name(n),
    listenSocket(-1) {}

OSEF::UnixListener::~UnixListener()
{
    SocketToolbox::shutdownSocket(listenSocket);
    SocketToolbox::closeSocket(listenSocket, name);
}

bool OSEF::UnixListener::acceptClient(int32_t& clientSock)
{
    bool ret = false;

    if (setUpListener())
    {
        clientSock = accept4(listenSocket, nullptr, nullptr, SOCK_CLOEXEC);
        if (clientSock > 0)
        {
            DOUT("Stream UNIX listener accepted connection on socket " << clientSock);
            ret = true;
        }
        else
        {
            DERR("error accepting UNIX socket " << strerror(errno));
        }
    }

    return ret;
}

bool OSEF::UnixListener::setUpListener()
{
    bool ret = false;

    if (listenSocket <= 0)
    {
        if (getBindedStreamSocket())
        {
            if (listen(listenSocket, OSEF_LISTEN_MAX_CLIENT) >= 0)
            {
                ret = true;
                DOUT("Stream UNIX listener binded to " << name);
            }
            else
            {
                DERR("error listening UNIX socket " << listenSocket);
                SocketToolbox::shutdownSocket(listenSocket);
                SocketToolbox::closeSocket(listenSocket, name);
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::UnixListener::getBindedStreamSocket()
{
    bool ret = false;

    sockaddr_un serverAddress{};
    if (SocketToolbox::getHostStreamSocket(name, listenSocket, serverAddress))
    {
        if (bind(listenSocket, reinterpret_cast<sockaddr*>(&serverAddress), sizeof(sockaddr_un)) >= 0)
        {
            ret = true;
        }
        else
        {
            DERR("error binding UNIX socket " << name);
        }

        if (not ret)
        {
            SocketToolbox::shutdownSocket(listenSocket);
            SocketToolbox::closeSocket(listenSocket, name);
        }
    }

    return ret;
}
