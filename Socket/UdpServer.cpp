#include "UdpServer.h"
#include "SocketToolbox.h"
#include "Swap.h"
#include "Debug.h"

#include <arpa/inet.h>  // inet_aton
#include <net/if.h>  // IF_NAMESIZE
#include <ifaddrs.h>  // getifaddrs

OSEF::UdpServer::UdpServer(const std::string& hst, const std::string& prt)
    :UdpEndpoint(hst, prt),
    receptionAddressLength(sizeof(getHostAddress())) {}

bool OSEF::UdpServer::connectNetwork()
{
    bool ret = false;

    if (getSocket() <= 0)
    {
        if (getIPv4UDPBindedSocket(getSocket(), getPort()))
        {
            if (setsockopt(getSocket(), SOL_SOCKET, SO_BINDTODEVICE, getHost().c_str(), static_cast<socklen_t>(IF_NAMESIZE)) == 0)
            {
                DOUT("Set socket " << getSocket() << " bind device option to " << getHost());

                DOUT("UDP unicast server linked to port " << getPort() << " on device " << getHost());
                ret = true;
            }
            else
            {
                DERR("error setting bind to device socket option");
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::UdpServer::getLocalHostIPv4Address(struct in_addr& address)
{
    bool ret = false;

    struct ifaddrs* addresses = nullptr;
    if (getifaddrs(&addresses) == 0)
    {
        struct ifaddrs* i = nullptr;
        for (i=addresses; i != nullptr; i=i->ifa_next)
        {
            // check Ethernet devices name
            // choose IPv4 over IPv6
            if ((i->ifa_addr != nullptr)
                && (i->ifa_addr->sa_family == AF_INET)
                && (getHost() == i->ifa_name))
            {
                address = (reinterpret_cast<sockaddr_in*>(i->ifa_addr))->sin_addr;
                ret = true;

                DOUT("Get IPv4 address " << inet_ntoa(address) << " for local host " << getHost());
            }
        }

        freeifaddrs(addresses);

        if (not ret)
        {
            DERR("cannot find IPv4 address for local host " << getHost());
        }
    }

    return ret;
}

bool OSEF::UdpServer::getIPv4UDPBindedSocket(int32_t& sock, const std::string& prt)
{
    bool ret = false;

    if (getIPv4UDPSocket())
    {
        in_addr address{};
        if (getLocalHostIPv4Address(address))
        {
            sockaddr_in localAddress{};
            memset(reinterpret_cast<char*>(&localAddress), 0, sizeof(localAddress));
            localAddress.sin_family = AF_INET;
            localAddress.sin_port = hton16(std::stoi(prt));
            localAddress.sin_addr.s_addr = address.s_addr;
            if (bind(sock, reinterpret_cast<sockaddr*>(&localAddress), sizeof(localAddress)) == 0)
            {
                DOUT("Bind socket " << sock << " to any address on port " << prt);
                ret = true;
            }
            else
            {
                DERR("error binding socket to local port " << prt << "(" << strerror(errno) << ")");
            }
        }
    }

    return ret;
}

//    bool OSEF::UdpServer::bindSocketToPort(int32_t& sock, const std::string& prt)
//    {
//        bool ret = false;
//
//        sockaddr_in localAddress{};
//        memset(reinterpret_cast<char*>(&localAddress), 0, sizeof(localAddress));
//        localAddress.sin_family = AF_INET;
//        localAddress.sin_port = hton16(std::stoi(prt));
//        localAddress.sin_addr.s_addr = htonl(INADDR_ANY);
//        if (bind(sock, reinterpret_cast<sockaddr*>(&localAddress), sizeof(localAddress)) == 0)
//        {
//            DOUT("Bind socket " << sock << " to any address on port " << prt);
//            ret = true;
//        }
//        else
//        {
//            DERR("error converting IP string to network address");
//        }
//
//        return ret;
//    }
