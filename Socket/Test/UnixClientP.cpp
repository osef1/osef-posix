#include "UnixClientP.h"
#include "UnixClient.h"
#include "UnixListener.h"
#include "UnixServer.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"
#include "RandomSocketName.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                             UnixClientP,
                             testing::Values(UnixClientParam({getRandomSocketName(8), getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                             UnixClientP,
                             testing::Values(UnixClientParam({getRandomSocketName(8), getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UnixClientP, doNothing)
    {
        OSEF::UnixClient client(GetParam().name);
    }

    TEST_P(UnixClientP, sendStringFail)
    {
        OSEF::UnixClient client(GetParam().name);

        EXPECT_FALSE(client.sendString(GetParam().msg));
    }

    TEST_P(UnixClientP, receiveStringFail)
    {
        OSEF::UnixClient client(GetParam().name);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, "");
    }

    TEST_P(UnixClientP, receiveStringTimeoutFail)
    {
        OSEF::UnixClient client(GetParam().name);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

    void* listenRoutine(const std::string& name)
    {
        OSEF::UnixListener listener(name);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
//            _DOUT("accepted "<<clientSocket);
        }

        return nullptr;
    }

    TEST_P(UnixClientP, receiveStringTimeoutEnd)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenRoutine, GetParam().name);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, "");

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenReceiveRoutine(const std::string& name, const size_t& msgSize)
    {
        OSEF::UnixListener listener(name);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::UnixServer server(clientSocket);

            std::string msg = "";
            if (server.receiveString(msg, msgSize))
            {
//                _DOUT("received "<<msg);
            }
        }

        return nullptr;
    }

    TEST_P(UnixClientP, sendString)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenReceiveRoutine, GetParam().name, GetParam().msg.size());
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        EXPECT_TRUE(client.sendString(GetParam().msg));

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenSendRoutine(const std::string& name, const std::string& msg)
    {
        OSEF::UnixListener listener(name);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::UnixServer server(clientSocket);

            if (server.sendString(msg))
            {
//                _DOUT("sent "<<msg);
            }
        }

        return nullptr;
    }

    TEST_P(UnixClientP, receiveString)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenSendRoutine, GetParam().name, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    TEST_P(UnixClientP, receiveStringTimeout)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenSendRoutine, GetParam().name, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenReceiveTwiceRoutine(const std::string& name, const size_t& msgSize)
    {
        OSEF::UnixListener listener(name);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::UnixServer server(clientSocket);

            std::string msg = "";
            if (server.receiveString(msg, msgSize))
            {
//                _DOUT("received once "<<msg);
                if (server.receiveString(msg, msgSize))
                {
//                    _DOUT("received twice "<<msg);
                }
            }
        }

        return nullptr;
    }

    TEST_P(UnixClientP, sendStringTwice)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenReceiveTwiceRoutine, GetParam().name, GetParam().msg.size());
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        EXPECT_TRUE(client.sendString(GetParam().msg));

        EXPECT_TRUE(client.sendString(GetParam().msg));

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenSendTwiceRoutine(const std::string& name, const std::string& msg)
    {
        OSEF::UnixListener listener(name);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::UnixServer server(clientSocket);

            if (server.sendString(msg))
            {
//                _DOUT("sent once "<<msg);

                if (server.sendString(msg))
                {
//                    _DOUT("sent twice "<<msg);
                }
            }
        }

        return nullptr;
    }

    TEST_P(UnixClientP, receiveStringTwice)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenSendTwiceRoutine, GetParam().name, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    TEST_P(UnixClientP, receiveStringTimeoutTwice)
    {
        OSEF::UnixClient client(GetParam().name);

        std::thread tlistener(listenSendTwiceRoutine, GetParam().name, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }
}  // namespace OSEF
