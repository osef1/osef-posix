#ifndef OSEFUDPMULTICASTSERVERP_H
#define OSEFUDPMULTICASTSERVERP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string localhost;
        std::string msg;
        timespec timeout;
    }UdpMulticastServerParam;

    class UdpMulticastServerP : public ::testing::TestWithParam<UdpMulticastServerParam>
    {
        protected:
            UdpMulticastServerP(){}
            ~UdpMulticastServerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUDPMULTICASTSERVERP_H */
