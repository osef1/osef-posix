#include "UdpMulticastClientP.h"
#include "UdpMulticastClient.h"

#include "RandomMessage.h"
#include "TimeOut.h"

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                            UdpMulticastClientP,
                            testing::Values(UdpMulticastClientParam({"127.0.0.1", "1234", "lo", getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                            UdpMulticastClientP,
                            testing::Values(UdpMulticastClientParam({"127.0.0.1", "1234", "lo", getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UdpMulticastClientP, doNothing)
    {
        OSEF::UdpMulticastClient client(GetParam().host, GetParam().port, GetParam().localhost);
    }

    TEST_P(UdpMulticastClientP, receiveStringTimeoutFail)
    {
        OSEF::UdpMulticastClient client(GetParam().host, GetParam().port, GetParam().localhost);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

    TEST_P(UdpMulticastClientP, sendString)
    {
        OSEF::UdpMulticastClient client(GetParam().host, GetParam().port, GetParam().localhost);

        EXPECT_TRUE(client.sendString(GetParam().msg));
    }

    TEST_P(UdpMulticastClientP, sendStringTwice)
    {
        OSEF::UdpMulticastClient client(GetParam().host, GetParam().port, GetParam().localhost);

        EXPECT_TRUE(client.sendString(GetParam().msg));

        EXPECT_TRUE(client.sendString(GetParam().msg));
    }
}  // namespace OSEF
