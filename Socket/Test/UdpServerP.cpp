#include "UdpServerP.h"
#include "UdpServer.h"
#include "UdpClient.h"
#include "SocketToolbox.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                            UdpServerP,
                            testing::Values(UdpServerParam({"127.0.0.1", "1234", "lo", getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                            UdpServerP,
                            testing::Values(UdpServerParam({"127.0.0.1", "1234", "lo", getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UdpServerP, doNothing)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);
    }

    TEST_P(UdpServerP, sendStringFail)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        EXPECT_FALSE(server.sendString(GetParam().msg));
    }

    TEST_P(UdpServerP, receiveStringFail)
    {
        OSEF::UdpServer server("notanhostname", GetParam().port);

        std::string msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, "");
    }

    TEST_P(UdpServerP, receiveStringTimeoutFail)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        std::string msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

    void* sendStringRoutine(const UdpServerParam& param)
    {
        OSEF::UdpClient client(param.host, param.port);

        OSEF::sleep(param.timeout);

        EXPECT_TRUE(client.sendString(param.msg));

        return nullptr;
    }

    TEST_P(UdpServerP, receiveString)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        std::thread tclient(sendStringRoutine, GetParam());
        tclient.detach();

        std::string msg = "";
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, GetParam().msg);
    }

    TEST_P(UdpServerP, receiveStringTimeout)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        std::thread tclient(sendStringRoutine, GetParam());
        tclient.detach();

        std::string msg = "";
        timespec doubleTimeout = GetParam().timeout + GetParam().timeout;
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size(), doubleTimeout));
        EXPECT_EQ(msg, GetParam().msg);
    }

    TEST_P(UdpServerP, echoString)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        std::thread tclient(sendStringRoutine, GetParam());
        tclient.detach();

        std::string msg = "";
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, GetParam().msg);

        EXPECT_TRUE(server.sendString(msg));
    }

    TEST_P(UdpServerP, echoStringTwice)
    {
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        std::thread tclient1(sendStringRoutine, GetParam());
        tclient1.detach();

        std::string msg = "";
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, GetParam().msg);

        EXPECT_TRUE(server.sendString(msg));

        std::thread tclient2(sendStringRoutine, GetParam());
        tclient2.detach();

        msg = "";
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, GetParam().msg);

        EXPECT_TRUE(server.sendString(msg));
    }
}  // namespace OSEF
