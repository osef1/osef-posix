#include "UdpClientP.h"
#include "UdpClient.h"
#include "UdpServer.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                            UdpClientP,
                            testing::Values(UdpClientParam({"127.0.0.1", "1234", "lo", getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                            UdpClientP,
                            testing::Values(UdpClientParam({"127.0.0.1", "1234", "lo", getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UdpClientP, doNothing)
    {
        OSEF::UdpClient client(GetParam().host, GetParam().port);
    }

    TEST_P(UdpClientP, receiveStringTimeoutFail)
    {
        OSEF::UdpClient client(GetParam().host, GetParam().port);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

    TEST_P(UdpClientP, sendString)
    {
        OSEF::UdpClient client(GetParam().host, GetParam().port);

        EXPECT_TRUE(client.sendString(GetParam().msg));
    }

    TEST_P(UdpClientP, sendStringTwice)
    {
        OSEF::UdpClient client(GetParam().host, GetParam().port);

        EXPECT_TRUE(client.sendString(GetParam().msg));

        EXPECT_TRUE(client.sendString(GetParam().msg));
    }

    void* echoStringRoutine(UdpServer* server, const UdpClientParam& param)
    {
        if (server != nullptr)
        {
            std::string msg = "";
            EXPECT_TRUE(server->receiveString(msg, param.msg.size()));
            EXPECT_EQ(msg, param.msg);

            EXPECT_TRUE(server->sendString(msg));
        }

        return nullptr;
    }

    TEST_P(UdpClientP, receiveString)
    {
        OSEF::UdpClient client(GetParam().host, GetParam().port);
        OSEF::UdpServer server(GetParam().localhost, GetParam().port);

        std::thread tserver(echoStringRoutine, &server, GetParam());
        tserver.detach();

        OSEF::sleep(GetParam().timeout);

        EXPECT_TRUE(client.sendString(GetParam().msg));

        std::string echo = "";
        EXPECT_TRUE(client.receiveString(echo, GetParam().msg.size()));
        EXPECT_EQ(echo, GetParam().msg);
    }
}  // namespace OSEF
