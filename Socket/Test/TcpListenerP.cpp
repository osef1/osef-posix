#include "TcpListenerP.h"
#include "TcpListener.h"
#include "TcpClient.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                             TcpListenerP,
                             testing::Values(TcpListenerParam({"localhost", "1234", getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                             TcpListenerP,
                             testing::Values(TcpListenerParam({"localhost", "1234", getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(TcpListenerP, doNothing)
    {
        OSEF::TcpListener listener(GetParam().host, GetParam().port);
    }

    TEST_P(TcpListenerP, failWrongHost)
    {
        OSEF::TcpListener listener("notanhostname", GetParam().port);

        int32_t clientSocket = -1;
        EXPECT_FALSE(listener.acceptClient(clientSocket));
    }

    void* connectRoutine(const std::string& host, const std::string& port, const timespec& timeout, const std::string& msg)
    {
        OSEF::TcpClient client(host, port);

        OSEF::sleep(timeout);

        if (client.sendString(msg))
        {
//            _DOUT("sent "<<msg);
        }

        OSEF::sleep(timeout);

        return nullptr;
    }

    TEST_P(TcpListenerP, acceptOnce)
    {
        OSEF::TcpListener listener(GetParam().host, GetParam().port);

        std::thread tclient(connectRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
        tclient.detach();

        int32_t clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);
    }

    TEST_P(TcpListenerP, listenTwiceFail)
    {
        OSEF::TcpListener listener(GetParam().host, GetParam().port);

        std::thread tclient(connectRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
        tclient.detach();

        int32_t clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);

        OSEF::TcpListener redundantListener(GetParam().host, GetParam().port);
        EXPECT_FALSE(redundantListener.acceptClient(clientSocket));
    }

    TEST_P(TcpListenerP, acceptTwice)
    {
        OSEF::TcpListener listener(GetParam().host, GetParam().port);

        std::thread tclient1(connectRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
        tclient1.detach();

        int32_t clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);

        std::thread tclient2(connectRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
        tclient2.detach();

        clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);
    }
}  // namespace OSEF
