#ifndef OSEFRANDOMSOCKETNAME_H
#define OSEFRANDOMSOCKETNAME_H

#include <string>

namespace OSEF
{
    std::string getRandomSocketName(const size_t& size);
}  // namespace OSEF

#endif  // OSEFRANDOMSOCKETNAME_H
