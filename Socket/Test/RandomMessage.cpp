#include "RandomMessage.h"
#include "Randomizer.h"

std::string OSEF::getRandomMessage(const size_t& size)
{
    std::string msg = "";

    OSEF::Randomizer randomizer;
    do
    {
        uint8_t c = randomizer.getUInt8();

        // msg must not contain end of string character
        if (c != 0)
        {
            msg += c;
        }
    }while (msg.size() < size);

    return msg;
}
