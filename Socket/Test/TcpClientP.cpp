#include "TcpClientP.h"
#include "TcpClient.h"
#include "TcpListener.h"
#include "TcpServer.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                             TcpClientP,
                             testing::Values(TcpClientParam({"localhost", "1234", getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                             TcpClientP,
                             testing::Values(TcpClientParam({"localhost", "1234", getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(TcpClientP, doNothing)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);
    }

    TEST_P(TcpClientP, sendStringNoServerFail)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        EXPECT_FALSE(client.sendString(GetParam().msg));
    }

    TEST_P(TcpClientP, receiveStringNoServerFail)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, "");
    }

    TEST_P(TcpClientP, receiveStringTimeoutFail)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

    void* listenRoutine(const std::string& host, const std::string& port)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
//            _DOUT("accepted "<<clientSocket);
        }

        return nullptr;
    }

    TEST_P(TcpClientP, receiveStringTimeoutEnd)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenRoutine, GetParam().host, GetParam().port);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, "");

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenReceiveRoutine(const std::string& host, const std::string& port, const size_t& msgSize)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::TcpServer server(clientSocket);

            std::string msg = "";
            if (server.receiveString(msg, msgSize))
            {
//                _DOUT("received "<<msg);
            }
        }

        return nullptr;
    }

    TEST_P(TcpClientP, sendString)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenReceiveRoutine, GetParam().host, GetParam().port, GetParam().msg.size());
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        EXPECT_TRUE(client.sendString(GetParam().msg));

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenSendRoutine(const std::string& host, const std::string& port, const std::string& msg)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::TcpServer server(clientSocket);

            if (server.sendString(msg))
            {
//                _DOUT("sent "<<msg);
            }
        }

        return nullptr;
    }

    TEST_P(TcpClientP, receiveString)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenSendRoutine, GetParam().host, GetParam().port, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    TEST_P(TcpClientP, receiveStringTimeout)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenSendRoutine, GetParam().host, GetParam().port, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenReceiveTwiceRoutine(const std::string& host, const std::string& port, const size_t& msgSize)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::TcpServer server(clientSocket);

            std::string msg = "";
            if (server.receiveString(msg, msgSize))
            {
//                _DOUT("received once "<<msg);
                if (server.receiveString(msg, msgSize))
                {
//                    _DOUT("received twice "<<msg);
                }
            }
        }

        return nullptr;
    }

    TEST_P(TcpClientP, sendStringTwice)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenReceiveTwiceRoutine, GetParam().host, GetParam().port, GetParam().msg.size());
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        EXPECT_TRUE(client.sendString(GetParam().msg));

        EXPECT_TRUE(client.sendString(GetParam().msg));

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenSendTwiceRoutine(const std::string& host, const std::string& port, const std::string& msg)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::TcpServer server(clientSocket);

            if (server.sendString(msg))
            {
//                _DOUT("sent once "<<msg);

                if (server.sendString(msg))
                {
//                    _DOUT("sent twice "<<msg);
                }
            }
        }

        return nullptr;
    }

    TEST_P(TcpClientP, receiveStringTwice)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenSendTwiceRoutine, GetParam().host, GetParam().port, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    TEST_P(TcpClientP, receiveStringTimeoutTwice)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);

        std::thread tlistener(listenSendTwiceRoutine, GetParam().host, GetParam().port, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        msg = "";
        EXPECT_TRUE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }
}  // namespace OSEF
