#ifndef OSEFTCPLISTENERP_H
#define OSEFTCPLISTENERP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string msg;
        timespec timeout;
    }TcpListenerParam;

    class TcpListenerP : public ::testing::TestWithParam<TcpListenerParam>
    {
        protected:
            TcpListenerP(){}
            ~TcpListenerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFTCPLISTENERP_H */
