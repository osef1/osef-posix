#include "UdpMulticastServerP.h"
#include "UdpMulticastServer.h"
#include "UdpMulticastClient.h"
#include "SocketToolbox.h"
#include "TimeOut.h"

#include "RandomMessage.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                            UdpMulticastServerP,
                            testing::Values(UdpMulticastServerParam({"239.255.255.250", "1234", "lo", getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                            UdpMulticastServerP,
                            testing::Values(UdpMulticastServerParam({"239.255.255.250", "1234", "lo", getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UdpMulticastServerP, doNothing)
    {
        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);
    }

    TEST_P(UdpMulticastServerP, sendStringFail)
    {
        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);

        EXPECT_FALSE(server.sendString(GetParam().msg));
    }

    TEST_P(UdpMulticastServerP, receiveStringFail)
    {
        OSEF::UdpMulticastServer server("notanhostname", GetParam().port, GetParam().localhost);

        std::string msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, "");
    }

    TEST_P(UdpMulticastServerP, receiveStringTimeoutFail)
    {
        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);

        std::string msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

//    void* sendStringRoutine(const UdpMulticastServerParam& param)
//    {
//        std::string ip = "";
//        if (OSEF::SocketToolbox::getLocalIpv4(param.host, ip))
//        {
//            OSEF::UdpMulticastClient client(ip, param.port, param.localhost);
//
//            OSEF::sleep(param.timeout);
//
//            if (client.sendString(param.msg))
//            {
////                DOUT("UDP client sent " << msg);
//            }
//        }
//
//        return nullptr;
//    }

//    TEST_P(UdpMulticastServerP, receiveString)
//    {
//        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);
//
//        std::thread tclient(sendStringRoutine, GetParam().host, GetParam().port, GetParam().localhost, GetParam().timeout, GetParam().msg);
//        tclient.detach();
//
//        std::string msg = "";
//        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
//        EXPECT_EQ(msg, GetParam().msg);
//    }

//    TEST_P(UdpMulticastServerP, receiveStringTimeout)
//    {
//        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);
//
//        std::thread tclient(sendStringRoutine, GetParam().host, GetParam().port, timespec({0, 0}), GetParam().msg);
//        tclient.detach();
//
//        std::string msg = "";
//        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
//        EXPECT_EQ(msg, GetParam().msg);
//    }
//
//    TEST_P(UdpMulticastServerP, echoString)
//    {
//        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);
//
//        std::thread tclient(sendStringRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
//        tclient.detach();
//
//        std::string msg = "";
//        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
//        EXPECT_EQ(msg, GetParam().msg);
//
//        EXPECT_TRUE(server.sendString(msg));
//    }
//
//    TEST_P(UdpMulticastServerP, echoStringTwice)
//    {
//        OSEF::UdpMulticastServer server(GetParam().host, GetParam().port, GetParam().localhost);
//
//        std::thread tclient1(sendStringRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
//        tclient1.detach();
//
//        std::string msg = "";
//        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
//        EXPECT_EQ(msg, GetParam().msg);
//
//        EXPECT_TRUE(server.sendString(msg));
//
//        std::thread tclient2(sendStringRoutine, GetParam().host, GetParam().port, GetParam().timeout, GetParam().msg);
//        tclient2.detach();
//
//        msg = "";
//        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
//        EXPECT_EQ(msg, GetParam().msg);
//
//        EXPECT_TRUE(server.sendString(msg));
//    }
}  // namespace OSEF
