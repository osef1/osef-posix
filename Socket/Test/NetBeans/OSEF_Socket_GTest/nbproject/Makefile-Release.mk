#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/3c163524/gtest-all.o \
	${OBJECTDIR}/_ext/29dd86f/NetworkSessionP.o \
	${OBJECTDIR}/_ext/29dd86f/RandomMessage.o \
	${OBJECTDIR}/_ext/29dd86f/RandomSocketName.o \
	${OBJECTDIR}/_ext/29dd86f/TcpClientP.o \
	${OBJECTDIR}/_ext/29dd86f/TcpListenerP.o \
	${OBJECTDIR}/_ext/29dd86f/TcpListenerThreadP.o \
	${OBJECTDIR}/_ext/29dd86f/TcpServerP.o \
	${OBJECTDIR}/_ext/29dd86f/UdpClientP.o \
	${OBJECTDIR}/_ext/29dd86f/UdpMulticastClientP.o \
	${OBJECTDIR}/_ext/29dd86f/UdpMulticastServerP.o \
	${OBJECTDIR}/_ext/29dd86f/UdpServerP.o \
	${OBJECTDIR}/_ext/29dd86f/UnixClientP.o \
	${OBJECTDIR}/_ext/29dd86f/UnixListenerP.o \
	${OBJECTDIR}/_ext/29dd86f/UnixServerP.o \
	${OBJECTDIR}/_ext/29dd86f/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wall -Wextra -Werror
CXXFLAGS=-Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../../../NetBeans/OSEF_POSIX/dist/Release/GNU-Linux/libosef_posix.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_socket_gtest

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_socket_gtest: ../../../../NetBeans/OSEF_POSIX/dist/Release/GNU-Linux/libosef_posix.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_socket_gtest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_socket_gtest ${OBJECTFILES} ${LDLIBSOPTIONS} -lpthread -lrt

${OBJECTDIR}/_ext/3c163524/gtest-all.o: ../../../../googletest/googletest/src/gtest-all.cc
	${MKDIR} -p ${OBJECTDIR}/_ext/3c163524
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3c163524/gtest-all.o ../../../../googletest/googletest/src/gtest-all.cc

${OBJECTDIR}/_ext/29dd86f/NetworkSessionP.o: ../../NetworkSessionP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/NetworkSessionP.o ../../NetworkSessionP.cpp

${OBJECTDIR}/_ext/29dd86f/RandomMessage.o: ../../RandomMessage.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/RandomMessage.o ../../RandomMessage.cpp

${OBJECTDIR}/_ext/29dd86f/RandomSocketName.o: ../../RandomSocketName.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/RandomSocketName.o ../../RandomSocketName.cpp

${OBJECTDIR}/_ext/29dd86f/TcpClientP.o: ../../TcpClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/TcpClientP.o ../../TcpClientP.cpp

${OBJECTDIR}/_ext/29dd86f/TcpListenerP.o: ../../TcpListenerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/TcpListenerP.o ../../TcpListenerP.cpp

${OBJECTDIR}/_ext/29dd86f/TcpListenerThreadP.o: ../../TcpListenerThreadP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/TcpListenerThreadP.o ../../TcpListenerThreadP.cpp

${OBJECTDIR}/_ext/29dd86f/TcpServerP.o: ../../TcpServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/TcpServerP.o ../../TcpServerP.cpp

${OBJECTDIR}/_ext/29dd86f/UdpClientP.o: ../../UdpClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UdpClientP.o ../../UdpClientP.cpp

${OBJECTDIR}/_ext/29dd86f/UdpMulticastClientP.o: ../../UdpMulticastClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UdpMulticastClientP.o ../../UdpMulticastClientP.cpp

${OBJECTDIR}/_ext/29dd86f/UdpMulticastServerP.o: ../../UdpMulticastServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UdpMulticastServerP.o ../../UdpMulticastServerP.cpp

${OBJECTDIR}/_ext/29dd86f/UdpServerP.o: ../../UdpServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UdpServerP.o ../../UdpServerP.cpp

${OBJECTDIR}/_ext/29dd86f/UnixClientP.o: ../../UnixClientP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UnixClientP.o ../../UnixClientP.cpp

${OBJECTDIR}/_ext/29dd86f/UnixListenerP.o: ../../UnixListenerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UnixListenerP.o ../../UnixListenerP.cpp

${OBJECTDIR}/_ext/29dd86f/UnixServerP.o: ../../UnixServerP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/UnixServerP.o ../../UnixServerP.cpp

${OBJECTDIR}/_ext/29dd86f/main.o: ../../main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../.. -I../../../../Random -I../../../../osef-log/Debug -I../../../../Thread -I../../../../MsgQ -I../../../../Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/main.o ../../main.cpp

# Subprojects
.build-subprojects:
	cd ../../../../NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Release

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../../../../NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Release clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
