#ifndef OSEFTCPLISTENERTHREADP_H
#define OSEFTCPLISTENERTHREADP_H

#include <string>

#include "gtest/gtest.h"
#include "TcpApplication.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        size_t threads;
        std::string msg;
        timespec timeout;
    }TcpListenerThreadParam;

    class EchoLoopApplication : public TcpApplication
    {
    public:
       explicit EchoLoopApplication(const size_t& ms)
        : msgSize(ms) {}
       virtual ~EchoLoopApplication() {}

       bool serve(TcpServer& server) override;

    private:
       EchoLoopApplication(const EchoLoopApplication& orig);
       EchoLoopApplication& operator=(const EchoLoopApplication& orig);

       size_t msgSize;
    };

    class TcpListenerThreadP : public ::testing::TestWithParam<TcpListenerThreadParam>
    {
        protected:
            TcpListenerThreadP(){}
            ~TcpListenerThreadP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFTCPLISTENERTHREADP_H */
