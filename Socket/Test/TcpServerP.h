#ifndef OSEFTCPSERVERP_H
#define OSEFTCPSERVERP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string msg;
        timespec timeout;
    }TcpServerParam;

    class TcpServerP : public ::testing::TestWithParam<TcpServerParam>
    {
        protected:
            TcpServerP(){}
            ~TcpServerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFTCPSERVERP_H */
