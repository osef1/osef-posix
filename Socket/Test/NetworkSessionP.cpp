#include "NetworkSessionP.h"
#include "NetworkSession.h"

#include "RandomMessage.h"

#include "TcpClient.h"
#include "TcpListener.h"
#include "TcpServer.h"

#include "TimeOut.h"
#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                             NetworkSessionP,
                             testing::Values(NetworkSessionParam({"localhost", "1234", getRandomMessage(8), OSEF::T100ms})));

    TEST_P(NetworkSessionP, doNothing)
    {
        NetworkSession session(nullptr);
    }

    TEST_P(NetworkSessionP, sendNoEndpoint)
    {
        NetworkSession session(nullptr);

        EXPECT_FALSE(session.sendString(GetParam().msg));

        EXPECT_TRUE(session.tearDown());
    }

    TEST_P(NetworkSessionP, reveiceNoEndpoint)
    {
        NetworkSession session(nullptr);

        std::string msg = "";
        EXPECT_FALSE(session.receiveString(msg, GetParam().msg.size()));

        EXPECT_TRUE(session.tearDown());
    }

    TEST_P(NetworkSessionP, reveiceTimeoutNoEndpoint)
    {
        NetworkSession session(nullptr);

        std::string msg = "";
        EXPECT_FALSE(session.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_TRUE(session.tearDown());
    }

    void* listenSessionReceiveRoutine(const std::string& host, const std::string& port, const size_t& msgSize)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::TcpServer server(clientSocket);

            std::string msg = "";
            if (server.receiveString(msg, msgSize))
            {
//                _DOUT("received "<<msg);
            }
        }

        return nullptr;
    }

    TEST_P(NetworkSessionP, sendString)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);
        NetworkSession session(&client);

        std::thread tlistener(listenSessionReceiveRoutine, GetParam().host, GetParam().port, GetParam().msg.size());
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        EXPECT_TRUE(session.sendString(GetParam().msg));

        EXPECT_TRUE(session.tearDown());

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    void* listenSessionSendRoutine(const std::string& host, const std::string& port, const std::string& msg)
    {
        OSEF::TcpListener listener(host, port);

        int32_t clientSocket = -1;
        if (listener.acceptClient(clientSocket))
        {
            OSEF::TcpServer server(clientSocket);

            if (server.sendString(msg))
            {
//                _DOUT("sent "<<msg);
            }
        }

        return nullptr;
    }

    TEST_P(NetworkSessionP, receiveString)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);
        NetworkSession session(&client);

        std::thread tlistener(listenSessionSendRoutine, GetParam().host, GetParam().port, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(session.receiveString(msg, GetParam().msg.size()));

        EXPECT_EQ(msg, GetParam().msg);

        EXPECT_TRUE(session.tearDown());

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    TEST_P(NetworkSessionP, receiveStringTimeout)
    {
        OSEF::TcpClient client(GetParam().host, GetParam().port);
        NetworkSession session(&client);

        std::thread tlistener(listenSessionSendRoutine, GetParam().host, GetParam().port, GetParam().msg);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        std::string msg = "";
        EXPECT_TRUE(session.receiveString(msg, GetParam().msg.size(), GetParam().timeout));

        EXPECT_EQ(msg, GetParam().msg);

        EXPECT_TRUE(session.tearDown());

        session.tearDown();

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }
}  // namespace OSEF
