#include "UnixListenerP.h"
#include "UnixListener.h"
#include "UnixClient.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"
#include "RandomSocketName.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                             UnixListenerP,
                             testing::Values(UnixListenerParam({getRandomSocketName(8), getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                             UnixListenerP,
                             testing::Values(UnixListenerParam({getRandomSocketName(8), getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UnixListenerP, doNothing)
    {
        OSEF::UnixListener listener(GetParam().name);
    }

    TEST_P(UnixListenerP, failWrongName)
    {
        OSEF::UnixListener listener("/");

        int32_t clientSocket = -1;
        EXPECT_FALSE(listener.acceptClient(clientSocket));
    }

    void* connectRoutine(const std::string& name, const timespec& timeout, const std::string& msg)
    {
        OSEF::UnixClient client(name);

        OSEF::sleep(timeout);

        if (client.sendString(msg))
        {
//            _DOUT("sent "<<msg);
        }

        OSEF::sleep(timeout);

        return nullptr;
    }

    TEST_P(UnixListenerP, acceptOnce)
    {
        OSEF::UnixListener listener(GetParam().name);

        std::thread tclient(connectRoutine, GetParam().name, GetParam().timeout, GetParam().msg);
        tclient.detach();

        int32_t clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);
    }

    TEST_P(UnixListenerP, acceptTwice)
    {
        OSEF::UnixListener listener(GetParam().name);

        std::thread tclient1(connectRoutine, GetParam().name, GetParam().timeout, GetParam().msg);
        tclient1.detach();

        int32_t clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);

        std::thread tclient2(connectRoutine, GetParam().name, GetParam().timeout, GetParam().msg);
        tclient2.detach();

        clientSocket = -1;
        EXPECT_TRUE(listener.acceptClient(clientSocket));
        EXPECT_NE(clientSocket, -1);
    }
}  // namespace OSEF
