#include "TcpListenerThreadP.h"
#include "TcpListenerThread.h"
#include "TcpClient.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"

#include <vector>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage_MinThreads,
                                 TcpListenerThreadP,
                                 testing::Values(TcpListenerThreadParam({"localhost", "1234", 2, getRandomMessage(8), OSEF::T10ms})));

    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage_MaxThreads,
                             TcpListenerThreadP,
                             testing::Values(TcpListenerThreadParam({"localhost", "1234", 10, getRandomMessage(8), OSEF::T10ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage_MinThreads,
                             TcpListenerThreadP,
                             testing::Values(TcpListenerThreadParam({"localhost", "1234", 2, getRandomMessage(2048), OSEF::T10ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage_MaxThreads,
                             TcpListenerThreadP,
                             testing::Values(TcpListenerThreadParam({"localhost", "1234", 10, getRandomMessage(2048), OSEF::T10ms})));

    bool EchoLoopApplication::serve(TcpServer& server)
    {
        bool ret = false;

        std::string msg = "";
        while (server.receiveString(msg, msgSize))
        {
            ret = server.sendString(msg);
        }

        return ret;
    }

    TEST_P(TcpListenerThreadP, doNothing)
    {
        OSEF::EchoLoopApplication app(GetParam().msg.size());
        OSEF::TcpListenerThread listener(GetParam().host, GetParam().port, 1, &app);
    }

    void startListening(OSEF::TcpListenerThread& listener, const timespec& timeout)
    {
        EXPECT_TRUE(listener.spawnListenRoutine());

        //  give time to listen
        OSEF::sleep(timeout);
    }

    void exchangeEcho(OSEF::TcpClient& client, const std::string& msg)
    {
        EXPECT_TRUE(client.sendString(msg));

        std::string response = "";
        EXPECT_TRUE(client.receiveString(response, msg.size()));
        EXPECT_EQ(response, msg);
    }

    void connectClient(OSEF::TcpListenerThread& listener, const std::string& host, const std::string& port, const std::string& msg)
    {
        {
            OSEF::TcpClient client(host, port);

            exchangeEcho(client, msg);
        }

        // wait server threads to finalize
        do
        {
            OSEF::sleep(OSEF::T10ms);
        }while (listener.getOccupation() > 1);
    }

    TEST_P(TcpListenerThreadP, serveOnce)
    {
        OSEF::EchoLoopApplication app(GetParam().msg.size());
        OSEF::TcpListenerThread listener(GetParam().host, GetParam().port, 2, &app);

        startListening(listener, GetParam().timeout);

        connectClient(listener, GetParam().host, GetParam().port, GetParam().msg);
    }

    TEST_P(TcpListenerThreadP, serveTwice)
    {
        OSEF::EchoLoopApplication app(GetParam().msg.size());
        OSEF::TcpListenerThread listener(GetParam().host, GetParam().port, 2, &app);

        startListening(listener, GetParam().timeout);

        //  serve first request
        connectClient(listener, GetParam().host, GetParam().port, GetParam().msg);

        //  serve second request
        connectClient(listener, GetParam().host, GetParam().port, GetParam().msg);
    }

    TEST_P(TcpListenerThreadP, serveTooManyClients)
    {
        OSEF::EchoLoopApplication app(GetParam().msg.size());
        OSEF::TcpListenerThread listener(GetParam().host, GetParam().port, GetParam().threads, &app);

        startListening(listener, GetParam().timeout);

        std::vector<OSEF::TcpClient*> clients;

        for (size_t i = 0; i < (GetParam().threads - 1); i++)
        {
            clients.push_back(new OSEF::TcpClient(GetParam().host, GetParam().port));

            exchangeEcho(*(clients[i]), GetParam().msg);
        }

        OSEF::TcpClient client(GetParam().host, GetParam().port);

        //  serve too many request
        EXPECT_TRUE(client.sendString(GetParam().msg));

        std::string response = "";
        EXPECT_FALSE(client.receiveString(response, GetParam().msg.size(), GetParam().timeout));

        for (size_t i = 0; i < (GetParam().threads - 1); i++)
        {
            if (clients[i] != nullptr)
            {
                delete clients[i];
            }
        }

        // wait server threads to finalize
        do
        {
            OSEF::sleep(OSEF::T10ms);
        }while (listener.getOccupation() > 1);
    }

    TEST_P(TcpListenerThreadP, failWrongHost)
    {
        OSEF::EchoLoopApplication app(GetParam().msg.size());
        OSEF::TcpListenerThread listener("nohost", GetParam().port, 1, &app);

        startListening(listener, GetParam().timeout);

        OSEF::TcpClient client("nohost", GetParam().port);

        EXPECT_FALSE(client.sendString(GetParam().msg));

        // wait server thread to finalize
        do
        {
            OSEF::sleep(OSEF::T10ms);
        }while (listener.getOccupation() > 1);
    }
}  // namespace OSEF
