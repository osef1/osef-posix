#ifndef OSEFNETWORKSESSIONP_H
#define OSEFNETWORKSESSIONP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string msg;
        timespec timeout;
    }NetworkSessionParam;

    class NetworkSessionP : public ::testing::TestWithParam<NetworkSessionParam>
    {
        protected:
            NetworkSessionP(){}
            ~NetworkSessionP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFNETWORKSESSIONP_H */
