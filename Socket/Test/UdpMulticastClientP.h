#ifndef OSEFUDPMULTICASTCLIENTP_H
#define OSEFUDPMULTICASTCLIENTP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string localhost;
        std::string msg;
        timespec timeout;
    }UdpMulticastClientParam;

    class UdpMulticastClientP : public ::testing::TestWithParam<UdpMulticastClientParam>
    {
        protected:
            UdpMulticastClientP(){}
            ~UdpMulticastClientP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUDPMULTICASTCLIENTP_H */
