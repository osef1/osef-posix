#ifndef OSEFUNIXLISTENERP_H
#define OSEFUNIXLISTENERP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string name;
        std::string msg;
        timespec timeout;
    }UnixListenerParam;

    class UnixListenerP : public ::testing::TestWithParam<UnixListenerParam>
    {
        protected:
            UnixListenerP(){}
            ~UnixListenerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUNIXLISTENERP_H */
