#ifndef OSEFRANDOMMESSAGE_H
#define OSEFRANDOMMESSAGE_H

#include <string>

namespace OSEF
{
    std::string getRandomMessage(const size_t& size);
}  // namespace OSEF

#endif  // OSEFRANDOMMESSAGE_H
