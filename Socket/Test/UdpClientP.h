#ifndef OSEFUDPCLIENTP_H
#define OSEFUDPCLIENTP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string localhost;
        std::string msg;
        timespec timeout;
    }UdpClientParam;

    class UdpClientP : public ::testing::TestWithParam<UdpClientParam>
    {
        protected:
            UdpClientP(){}
            ~UdpClientP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUDPCLIENTP_H */
