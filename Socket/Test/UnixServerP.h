#ifndef OSEFUNIXSERVERP_H
#define OSEFUNIXSERVERP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string name;
        std::string msg;
        timespec timeout;
    }UnixServerParam;

    class UnixServerP : public ::testing::TestWithParam<UnixServerParam>
    {
        protected:
            UnixServerP(){}
            ~UnixServerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUNIXSERVERP_H */
