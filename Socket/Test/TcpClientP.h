#ifndef OSEFTCPCLIENTP_H
#define OSEFTCPCLIENTP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string msg;
        timespec timeout;
    }TcpClientParam;

    class TcpClientP : public ::testing::TestWithParam<TcpClientParam>
    {
        protected:
            TcpClientP(){}
            ~TcpClientP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFTCPCLIENTP_H */
