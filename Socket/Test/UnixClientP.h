#ifndef OSEFUNIXCLIENTP_H
#define OSEFUNIXCLIENTP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string name;
        std::string msg;
        timespec timeout;
    }UnixClientParam;

    class UnixClientP : public ::testing::TestWithParam<UnixClientParam>
    {
        protected:
            UnixClientP(){}
            ~UnixClientP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUNIXCLIENTP_H */
