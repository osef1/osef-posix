#ifndef OSEFUDPSERVERP_H
#define OSEFUDPSERVERP_H

#include <string>

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        std::string host;
        std::string port;
        std::string localhost;
        std::string msg;
        timespec timeout;
    }UdpServerParam;

    class UdpServerP : public ::testing::TestWithParam<UdpServerParam>
    {
        protected:
            UdpServerP(){}
            ~UdpServerP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif /* OSEFUDPSERVERP_H */
