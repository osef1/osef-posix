#include "UnixServerP.h"
#include "UnixServer.h"
#include "UnixListener.h"
#include "UnixClient.h"
#include "TimeOut.h"
#include "Debug.h"

#include "RandomMessage.h"
#include "RandomSocketName.h"

#include <thread>

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Local_ShortMessage,
                             UnixServerP,
                             testing::Values(UnixServerParam({getRandomSocketName(8), getRandomMessage(8), OSEF::T100ms})));

    INSTANTIATE_TEST_SUITE_P(Local_LongMessage,
                             UnixServerP,
                             testing::Values(UnixServerParam({getRandomSocketName(8), getRandomMessage(2048), OSEF::T100ms})));

    TEST_P(UnixServerP, doNothing)
    {
        OSEF::UnixServer server(-1);
    }

    TEST_P(UnixServerP, sendStringFail)
    {
        OSEF::UnixServer server(-1);

        EXPECT_FALSE(server.sendString(GetParam().msg));
    }

    TEST_P(UnixServerP, receiveStringFail)
    {
        OSEF::UnixServer server(-1);

        std::string msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, "");
    }

    TEST_P(UnixServerP, receiveStringTimeoutFail)
    {
        OSEF::UnixServer server(-1);

        std::string msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");
    }

    void* listenRoutine(const std::string& name, int32_t* clientSocket)
    {
        OSEF::UnixListener listener(name);

        if (clientSocket != nullptr)
        {
            if (listener.acceptClient(*clientSocket))
            {
//                _DOUT("accepted "<<*clientSocket);
            }
        }

        return nullptr;
    }

    TEST_P(UnixServerP, receiveStringTimeoutEnd)
    {
        int32_t clientSocket = -1;
        std::thread tlistener(listenRoutine, GetParam().name, &clientSocket);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        OSEF::UnixClient client(GetParam().name);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");

        OSEF::sleep(OSEF::T10ms);  // give time to accept

        OSEF::UnixServer server(clientSocket);

        msg = "";
        EXPECT_FALSE(server.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

//    void* listenReceiveRoutine(const std::string& host, const std::string port, const size_t& msgSize)
//    {
//        OSEF::UnixListener listener(name);
//
//        int32_t clientSocket = -1;
//        if (listener.acceptClient(clientSocket))
//        {
//            OSEF::UnixServer server(clientSocket);
//
//            std::string msg = "";
//            if (server.receiveString(msg, msgSize))
//            {
//                _DOUT("received "<<msg);
//            }
//        }
//
//        return nullptr;
//    }

    TEST_P(UnixServerP, sendString)
    {
        int32_t clientSocket = -1;
        std::thread tlistener(listenRoutine, GetParam().name, &clientSocket);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        OSEF::UnixClient client(GetParam().name);

        std::string msg = "";
        EXPECT_FALSE(client.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, "");

        OSEF::sleep(OSEF::T10ms);  // give time to accept

        OSEF::UnixServer server(clientSocket);

        EXPECT_TRUE(server.sendString(msg));

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

//    void* listenSendRoutine(const std::string& host, const std::string port, const std::string& msg)
//    {
//        OSEF::UnixListener listener(name);
//
//        int32_t clientSocket = -1;
//        if (listener.acceptClient(clientSocket))
//        {
//            OSEF::UnixServer server(clientSocket);
//
//            if (server.sendString(msg))
//            {
//                _DOUT("sent "<<msg);
//            }
//        }
//
//        return nullptr;
//    }

    TEST_P(UnixServerP, receiveString)
    {
        int32_t clientSocket = -1;
        std::thread tlistener(listenRoutine, GetParam().name, &clientSocket);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        OSEF::UnixClient client(GetParam().name);

        EXPECT_TRUE(client.sendString(GetParam().msg));

        OSEF::sleep(OSEF::T10ms);  // give time to accept

        OSEF::UnixServer server(clientSocket);

        std::string msg = "";
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size()));
        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

    TEST_P(UnixServerP, receiveStringTimeout)
    {
        int32_t clientSocket = -1;
        std::thread tlistener(listenRoutine, GetParam().name, &clientSocket);
        tlistener.detach();

        OSEF::sleep(OSEF::T10ms);  // give time to listen

        OSEF::UnixClient client(GetParam().name);

        EXPECT_TRUE(client.sendString(GetParam().msg));

        OSEF::sleep(OSEF::T10ms);  // give time to accept

        OSEF::UnixServer server(clientSocket);

        std::string msg = "";
        EXPECT_TRUE(server.receiveString(msg, GetParam().msg.size(), GetParam().timeout));
        EXPECT_EQ(msg, GetParam().msg);

        OSEF::sleep(OSEF::T10ms);  // give time to shutdown
    }

//
//    // UNIX
//
//    TEST_P(SocketGTest, UnixClient_sendString_Fail) {
//        OSEF::UnixClient client(GetParam().name);
//        EXPECT_FALSE(client.sendString(GetParam().buffer));
//    }
//
//    TEST_P(SocketGTest, UnixClient_sendStringTimeout_Fail) {
//        OSEF::UnixClient client(GetParam().name);
//        EXPECT_FALSE(client.sendString(GetParam().buffer, GetParam().timeout));
//    }
//
//    TEST_P(SocketGTest, UnixClient_receiveString_Fail) {
//        OSEF::UnixClient client(GetParam().name);
//        std::string recvS = "";
//        EXPECT_FALSE(client.receiveString(recvS, GetParam().buffer.size()));
//    }
//
//    TEST_P(SocketGTest, UnixClient_receiveStringTimeout_Fail) {
//        OSEF::UnixClient client(GetParam().name);
//        std::string recvS = "";
//        EXPECT_FALSE(client.receiveString(recvS, GetParam().timeout, GetParam().buffer.size()));
//    }
//
//    void* echoStringUnixServerRoutine(void* arg)
//    {
//        socketTestParam* param = static_cast <socketTestParam*>(arg);
//        if (param!=nullptr)
//        {
//            OSEF::UnixListener listener(param->name);
//            int32_t socketFD = -1;
//
//            EXPECT_TRUE(listener.acceptClient(socketFD));
//            EXPECT_NE(socketFD, -1);
//
//            OSEF::UnixServer server(socketFD);
//            std::string buffer = "";
//
//            uint32_t gid = 0;
//            uint32_t uid = 0;
//            uint32_t pid = 0;
//            EXPECT_TRUE(server.getCredentials(gid, uid, pid));
//
//            EXPECT_TRUE(server.receiveString(buffer, param->buffer.size()));
//            EXPECT_EQ(buffer, param->buffer);
//
//            EXPECT_TRUE(server.sendString(buffer));
//        }
//
//        return arg;
//    }
//
//    TEST_P(SocketGTest, UnixClient_sendString) {
//        OSEF::UnixClient client(GetParam().name);
//
//        OSEF::Threader threader(1);
//        socketTestParam param = GetParam();
//        EXPECT_TRUE(threader.spawn(echoStringUnixServerRoutine, &param));
//
//        OSEF::sleepms(100);
//
//        EXPECT_TRUE(client.sendString(GetParam().buffer));
//
//        OSEF::sleepms(100);
//    }
//
//    TEST_P(SocketGTest, UnixClient_sendStringTimeout) {
//        OSEF::UnixClient client(GetParam().name);
//
//        OSEF::Threader threader(1);
//        socketTestParam param = GetParam();
//        EXPECT_TRUE(threader.spawn(echoStringUnixServerRoutine, &param));
//
//        OSEF::sleepms(100);
//
//        EXPECT_TRUE(client.sendString(GetParam().buffer, GetParam().timeout));
//
//        OSEF::sleepms(100);
//    }
//
//    TEST_P(SocketGTest, UnixClient_receiveString) {
//        OSEF::UnixClient client(GetParam().name);
//
//        OSEF::Threader threader(1);
//        socketTestParam param = GetParam();
//        EXPECT_TRUE(threader.spawn(echoStringUnixServerRoutine, &param));
//
//        OSEF::sleepms(100);
//
//        EXPECT_TRUE(client.sendString(GetParam().buffer));
//
//        std::string recvS = "";
//        EXPECT_TRUE(client.receiveString(recvS, GetParam().buffer.size()));
//
//        OSEF::sleepms(100);
//    }
//
//    TEST_P(SocketGTest, UnixClient_receiveStringTimeout) {
//        OSEF::UnixClient client(GetParam().name);
//
//        OSEF::Threader threader(1);
//        socketTestParam param = GetParam();
//        EXPECT_TRUE(threader.spawn(echoStringUnixServerRoutine, &param));
//
//        OSEF::sleepms(100);
//
//        EXPECT_TRUE(client.sendString(GetParam().buffer));
//
//        std::string recvS = "";
//        EXPECT_TRUE(client.receiveString(recvS, GetParam().timeout, GetParam().buffer.size()));
//
//        OSEF::sleepms(100);
//    }

}  // namespace OSEF
