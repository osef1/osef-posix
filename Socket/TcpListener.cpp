#include "TcpListener.h"
#include "SocketToolbox.h"  // getSocketAddress
#include "Debug.h"

#include <arpa/inet.h>  // inet_ntoa

OSEF::TcpListener::TcpListener(const std::string& hst, const std::string& prt)
    :host(hst),
    port(prt),
    listenSocket(-1) {}

OSEF::TcpListener::~TcpListener()
{
    SocketToolbox::shutdownSocket(listenSocket);
    SocketToolbox::closeSocket(listenSocket);
}

bool OSEF::TcpListener::acceptClient(int32_t& clientSocket)
{
    bool ret = false;

    if (setUpListener())
    {
        // accept client socket then
        // set option for immediate delivery of small TCP messages
        std::string clientAddress;
        if (acceptStreamSocket(clientSocket, clientAddress)
            && (SocketToolbox::setTcpOption(clientSocket, OSEF_TCP_NODELAY)))
        {
            DOUT("Stream listener accepted connection from " << clientAddress << " on socket " << clientSocket);
            ret = true;
        }
    }

    return ret;
}

bool OSEF::TcpListener::setUpListener()
{
    bool ret = false;

    if (listenSocket <= 0)
    {
        sockaddr serverAddress{};
        if (SocketToolbox::getHostStreamSocket(host, port, listenSocket, serverAddress))
        {
            if (SocketToolbox::setSocketOption(listenSocket, OSEF_SO_REUSEADDR))
            {
                if (bind(listenSocket, &serverAddress, OSEF_SOCKADDR_IN_LEN) >= 0)
                {
                    if (listen(listenSocket, OSEF_LISTEN_MAX_CLIENT) >= 0)
                    {
                        ret = true;
                        DOUT("Stream TCP listener binded to " << host << ":" << port);
                    }
                    else
                    {
                        DERR("error listening TCP socket to " << inet_ntoa((reinterpret_cast<sockaddr_in*>(&serverAddress))->sin_addr) << ":" << ntohs((reinterpret_cast<sockaddr_in*>(&serverAddress))->sin_port));
                    }
                }
                else
                {
                    DERR("error binding TCP listener socket to " << inet_ntoa((reinterpret_cast<sockaddr_in*>(&serverAddress))->sin_addr) << ":" << ntohs((reinterpret_cast<sockaddr_in*>(&serverAddress))->sin_port));
                }
            }

            if (not ret)
            {
                SocketToolbox::shutdownSocket(listenSocket);
                SocketToolbox::closeSocket(listenSocket);
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::TcpListener::acceptStreamSocket(int32_t& clientSocket, std::string& clientAddress) const
{
    bool ret = false;

    sockaddr clientSocketAddress{};
    socklen_t adrL = OSEF_SOCKADDR_IN_LEN;
    clientSocket = accept4(listenSocket, &clientSocketAddress, &adrL, SOCK_CLOEXEC);
    if (clientSocket != -1)
    {
        char addr[sizeof(struct in6_addr)];
        if (inet_ntop(clientSocketAddress.sa_family, &(reinterpret_cast<sockaddr_in*>(&clientSocketAddress))->sin_addr, &addr[0], sizeof(struct in6_addr)) != nullptr)
        {
            clientAddress.append(&addr[0]);
            ret = true;
        }
        else
        {
            DERR("error converting address " << strerror(errno));
        }
    }
    else
    {
        DERR("error accepting socket " << strerror(errno));
    }

    return ret;
}
