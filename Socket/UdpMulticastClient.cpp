#include "UdpMulticastClient.h"
#include "SocketToolbox.h"
#include "Debug.h"

#include <ifaddrs.h>  // getifaddrs
#include <arpa/inet.h>  // inet_ntoa

// https://github.com/kallisti5/libmicrossdp/blob/master/ssdp.c

OSEF::UdpMulticastClient::UdpMulticastClient(const std::string& hst, const std::string& prt, const std::string& lclhst)
    :UdpClient(hst, prt),
    localHost(lclhst) {}

bool OSEF::UdpMulticastClient::connectNetwork()
{
    bool ret = false;

    if (getSocket() <= 0)
    {
        if (getIPv4UDPSocket()
            && (OSEF::SocketToolbox::resetOption(getSocket(), IPPROTO_IP, IP_MULTICAST_LOOP)))
        {
            DOUT("Reset socket " << getSocket() << " multicast loop option");

            in_addr localInterface{};
            if (getLocalHostIPv4Address(localInterface)
                && (setsockopt(getSocket(), IPPROTO_IP, IP_MULTICAST_IF, static_cast<void*>(&localInterface), static_cast<socklen_t>(sizeof(localInterface))) == 0))
            {
                DOUT("Set socket " << getSocket() << " multicast send interface to " << inet_ntoa(localInterface));

                if (getIPv4SocketAddress(getHost(), getPort(), getHostAddress()))
                {
                    ret = true;

                    DOUT("UDP multicast client linked to " << getHost() << ":" << getPort() << " on interface " << localHost);
                }
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::UdpMulticastClient::getLocalHostIPv4Address(struct in_addr& address)
{
    bool ret = false;

    struct ifaddrs* addresses = nullptr;
    if (getifaddrs(&addresses) == 0)
    {
        struct ifaddrs* i = nullptr;
        for (i=addresses; i != nullptr; i=i->ifa_next)
        {
            // check Ethernet devices name
            // choose IPv4 over IPv6
            if ((i->ifa_addr != nullptr)
                && (i->ifa_addr->sa_family == AF_INET)
                && (localHost == i->ifa_name))
            {
                address = (reinterpret_cast<sockaddr_in*>(i->ifa_addr))->sin_addr;
                ret = true;

                DOUT("Get IPv4 address " << inet_ntoa(address) << " for local host " << localHost);
            }
        }

        freeifaddrs(addresses);

        if (not ret)
        {
            DERR("cannot find IPv4 address for local host " << localHost);
        }
    }

    return ret;
}
