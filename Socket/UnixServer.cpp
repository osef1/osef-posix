#include "UnixServer.h"

OSEF::UnixServer::UnixServer(const int32_t& sock)
    :StreamEndpoint(sock) {}

bool OSEF::UnixServer::connectNetwork()
{
    bool ret = false;

    if (getSocket() > 0)
    {
        ret = true;
    }

    return ret;
}
