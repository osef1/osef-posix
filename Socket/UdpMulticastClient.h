#ifndef OSEFUDPMULTICASTCLIENT_H
#define OSEFUDPMULTICASTCLIENT_H

#include "UdpClient.h"

#include <string>

namespace OSEF
{
    class UdpMulticastClient : public UdpClient
    {
    public:
        UdpMulticastClient(const std::string& hst, const std::string& prt, const std::string& lclhst);
        ~UdpMulticastClient() override = default;

        UdpMulticastClient(const UdpMulticastClient&) = delete;  // copy constructor
        UdpMulticastClient& operator=(const UdpMulticastClient&) = delete;  // copy assignment
        UdpMulticastClient(UdpMulticastClient&&) = delete;  // move constructor
        UdpMulticastClient& operator=(UdpMulticastClient&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;
        bool getLocalHostIPv4Address(struct in_addr& address);

        std::string localHost;
    };
}  // namespace OSEF

#endif /* OSEFUDPMULTICASTCLIENT_H */
