#ifndef OSEFUDPENDPOINT_H
#define OSEFUDPENDPOINT_H

#include "NetworkEndpoint.h"

#include <netinet/in.h>  // sockaddr_in
#include <string>

namespace OSEF
{
    class UdpEndpoint : public NetworkEndpoint
    {
    public:
        UdpEndpoint(const std::string& hst, const std::string& prt);
        ~UdpEndpoint() override;

        std::string getHost() const {return host;}
        std::string getPort() const {return port;}

        UdpEndpoint(const UdpEndpoint&) = delete;  // copy constructor
        UdpEndpoint& operator=(const UdpEndpoint&) = delete;  // copy assignment
        UdpEndpoint(UdpEndpoint&&) = delete;  // move constructor
        UdpEndpoint& operator=(UdpEndpoint&&) = delete;  // move assignment

    protected:
        bool getIPv4UDPSocket();

        int32_t& getSocket() {return socketFD;}
        sockaddr_in& getHostAddress() {return hostAddress;}

        virtual sockaddr_in* getReceptionAddress() = 0;
        virtual socklen_t* getReceptionAddressLength() = 0;

    private:
        bool sendToNet(void const* buffer, const size_t& size) override;
        bool receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL) override;
        bool receiveFromNet(void* buffer, const size_t& size, ssize_t& recvL, const timespec& to) override;

        std::string host;
        std::string port;
        int32_t socketFD;
        sockaddr_in hostAddress;
    };
}  // namespace OSEF

#endif /* OSEFUDPENDPOINT_H */
