#ifndef OSEFTCPAPPLICATION_H
#define OSEFTCPAPPLICATION_H

#include "TcpServer.h"

namespace OSEF
{
    class TcpApplication
    {
    public:
        TcpApplication() = default;
        virtual ~TcpApplication() = default;

        virtual bool serve(TcpServer& server) = 0;

        TcpApplication(const TcpApplication&) = delete;  // copy constructor
        TcpApplication& operator=(const TcpApplication&) = delete;  // copy assignment
        TcpApplication(TcpApplication&&) = delete;  // move constructor
        TcpApplication& operator=(TcpApplication&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* OSEFTCPAPPLICATION_H */
