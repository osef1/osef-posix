#include "UdpMulticastServer.h"
#include "SocketTypes.h"
#include "Debug.h"

#include <arpa/inet.h>  // inet_aton
#include <ifaddrs.h>  // getifaddrs
#include <netdb.h>  // NI_NUMERICHOST

OSEF::UdpMulticastServer::UdpMulticastServer(const std::string& hst, const std::string& prt, const std::string& lclhst)
    :UdpServer(hst, prt),
    localHost(lclhst) {}

bool OSEF::UdpMulticastServer::connectNetwork()
{
    bool ret = false;

    if (getSocket() <= 0)
    {
        if (getIPv4UDPBindedSocket(getSocket(), getPort()))
        {
            std::string localIp;
            if (getLocalIpv4(localIp))
            {
                ret = subscribeMulticastGroup(localIp);
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::UdpMulticastServer::getLocalIpv4(std::string& ip)
{
    bool ret = false;

    struct ifaddrs* addresses = nullptr;
    if (getifaddrs(&addresses) == 0)
    {
        struct ifaddrs* i = nullptr;
        for (i=addresses; i != nullptr; i=i->ifa_next)
        {
            // check Ethernet devices name
            // choose IPv4 over IPv6
            if ((i->ifa_addr != nullptr)
                && (i->ifa_addr->sa_family == AF_INET)
                && (localHost == i->ifa_name))
            {
                char ha[NI_MAXHOST];
                if (getnameinfo(i->ifa_addr, sizeof(struct sockaddr_in), &ha[0], OSEF_NI_MAX_HOST, nullptr, 0U, NI_NUMERICHOST) == 0)
                {
                    ip = &ha[0];
                    ret = true;
                    DOUT("Found IP " << ip << " for local host " << localHost);
                }
            }
        }

        if (not ret)
        {
            DERR("Couldn't find local host " << localHost << " IP");
        }

        freeifaddrs(addresses);
    }

    return ret;
}

bool OSEF::UdpMulticastServer::subscribeMulticastGroup(const std::string& localIp)
{
    bool ret = false;

    struct ip_mreq group{};
    if (inet_aton(localIp.c_str(), &(group.imr_interface)) != 0)
    {
        DOUT("Multicast group interface set to " << localIp);
        if (inet_aton(getHost().c_str(), &(group.imr_multiaddr)) != 0)
        {
            DOUT("Multicast group address set to " << getHost());
            if (setsockopt(getSocket(), IPPROTO_IP, IP_ADD_MEMBERSHIP, reinterpret_cast<char*>(&group), sizeof(group)) == 0)
            {
                DOUT("UDP multicast server linked to " << getHost() << ":" << getPort() << " on interface " << localHost);
                ret = true;
            }
            else
            {
                DERR("error setting multicast group membership socket option");
            }
        }
        else
        {
            DERR("error converting multicast IP string to network address");
        }
    }
    else
    {
        DERR("error converting local IP string to network address");
    }

    return ret;
}
