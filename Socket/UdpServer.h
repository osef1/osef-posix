#ifndef OSEFUDPSERVER_H
#define OSEFUDPSERVER_H

#include "UdpEndpoint.h"

#include <string>

namespace OSEF
{
    class UdpServer : public UdpEndpoint
    {
    public:
        explicit UdpServer(const std::string& hst, const std::string& prt);
        ~UdpServer() override = default;

        UdpServer(const UdpServer&) = delete;  // copy constructor
        UdpServer& operator=(const UdpServer&) = delete;  // copy assignment
        UdpServer(UdpServer&&) = delete;  // move constructor
        UdpServer& operator=(UdpServer&&) = delete;  // move assignment

    protected:
        bool getIPv4UDPBindedSocket(int32_t& sock, const std::string& prt);

    private:
//        bool bindSocketToPort(int32_t& sock, const std::string& prt);
        bool connectNetwork() override;

        bool getLocalHostIPv4Address(struct in_addr& address);

        sockaddr_in* getReceptionAddress() override {return &getHostAddress();}
        socklen_t* getReceptionAddressLength() override {return &receptionAddressLength;}

        socklen_t receptionAddressLength;
    };
}  // namespace OSEF

#endif /* OSEFUDPSERVER_H */
