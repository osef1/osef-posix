#ifndef OSEFTCPSERVER_H
#define OSEFTCPSERVER_H

#include "StreamEndpoint.h"

namespace OSEF
{
    class TcpServer : public StreamEndpoint
    {
    public:
        explicit TcpServer(const int32_t& sock);
        ~TcpServer() override = default;

        TcpServer(const TcpServer&) = delete;  // copy constructor
        TcpServer& operator=(const TcpServer&) = delete;  // copy assignment
        TcpServer(TcpServer&&) = delete;  // move constructor
        TcpServer& operator=(TcpServer&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;
    };
}  // namespace OSEF

#endif /* OSEFTCPSERVER_H */
