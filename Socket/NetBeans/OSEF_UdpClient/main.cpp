#include "SignalHandler.h"
#include "UdpClient.h"

#include <iostream>

int main()
{
    int ret = -1;

    OSEF::SignalHandler signal;
    if (signal.setSignalActionList({SIGINT, SIGTERM}))
    {
        OSEF::UdpClient client("127.0.0.1", "1234");

        std::string sendMsg = "0";
        timespec to = {3, 0};

        do
        {
            ret = -1;

            if (client.sendString(sendMsg))
            {
                std::cout << "client sent " << sendMsg << std::endl;

                std::string recvMsg = "";
                if (client.receiveString(recvMsg, 1024, to))
                {
                    std::cout << "client received " << recvMsg << std::endl;
                    if (sendMsg == recvMsg)
                    {
                        sendMsg[0] += 1;

                        ret = 0;
                    }
                }
                else
                {
                    std::cout << "no response received to " << sendMsg << " will be retried" << std::endl;
                    ret = 0;
                }
            }
        }while (not signal.signalReceived() && (ret == 0));
    }

    return ret;
}
