#include "SignalHandler.h"
#include "UdpMulticastServer.h"
#include "TimeOut.h"

#include <iostream>

int main()
{
    int ret = -1;

    OSEF::SignalHandler signal;
    if (signal.setSignalActionList({SIGINT, SIGTERM}))
    {
        OSEF::UdpMulticastServer server("239.255.255.250", "1234", "wlan0");

        std::string s = "";
        do
        {
            ret = -1;

            if (server.receiveString(s, 1024))
            {
                std::cout << "server received " << s << std::endl;
                if (server.sendString(s))
                {
                    std::cout << "server sent " << s << std::endl;
                    ret = 0;
                    OSEF::sleeps(1);
                }
            }
        }while (not signal.signalReceived() && (ret == 0));
    }

    return ret;
}
