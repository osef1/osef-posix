#include "TcpClient.h"

int main()
{
    int ret = -1;

    OSEF::TcpClient client("localhost", "1234");

    if (not client.sendString("azertyUI"))
    {
        ret = 0;
    }

    return ret;
}
