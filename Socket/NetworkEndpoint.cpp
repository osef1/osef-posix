#include "NetworkEndpoint.h"
#include "Debug.h"

bool OSEF::NetworkEndpoint::sendString(const std::string& s)
{
    bool ret = false;

    if (connectNetwork())
    {
        ret = sendToNet(s.c_str(), s.size());
    }

    return ret;
}

bool OSEF::NetworkEndpoint::receiveString(std::string& s, const size_t& size)
{
    bool ret = false;

    if (connectNetwork())
    {
        char buffer[size];
        ssize_t recvL = 0;
        if (receiveFromNet(&buffer[0], size, recvL))
        {
            s.assign(&buffer[0], static_cast<size_t>(recvL));
            ret = true;
        }
    }

    return ret;
}

bool OSEF::NetworkEndpoint::receiveString(std::string& s, const size_t& size, const timespec& to)
{
    bool ret = false;

    if (connectNetwork())
    {
        char buffer[size];
        ssize_t recvL = 0;
        if (receiveFromNet(&buffer[0], size, recvL, to))
        {
            s.assign(&buffer[0], static_cast<size_t>(recvL));
            ret = true;
        }
    }

    return ret;
}
