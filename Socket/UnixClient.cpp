#include "UnixClient.h"
#include "SocketToolbox.h"
#include "Debug.h"

OSEF::UnixClient::UnixClient(const std::string& nm)
    :StreamEndpoint(-1),
    name(nm) {}

OSEF::UnixClient::~UnixClient()
{
    DOUT("Stream Unix client shutting down connection to " << name);
}

bool OSEF::UnixClient::connectNetwork()
{
    bool ret = false;

    if (getSocket() <= 0)
    {
        sockaddr_un address{};
        if (SocketToolbox::getHostStreamSocket(name, getSocket(), address))
        {
            if (connect(getSocket(), reinterpret_cast<sockaddr*>(&address), sizeof(sockaddr_un)) == 0)
            {
                ret = true;
                DOUT("Unix client connected to " << name);
            }
            else
            {
                DOUT("Unix client could not connect to " << name);
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}
