#ifndef OSEFUDPCLIENT_H
#define OSEFUDPCLIENT_H

#include "UdpEndpoint.h"

#include <string>

namespace OSEF
{
    class UdpClient : public UdpEndpoint
    {
    public:
        UdpClient(const std::string& hst, const std::string& prt);
        ~UdpClient() override = default;

        UdpClient(const UdpClient&) = delete;  // copy constructor
        UdpClient& operator=(const UdpClient&) = delete;  // copy assignment
        UdpClient(UdpClient&&) = delete;  // move constructor
        UdpClient& operator=(UdpClient&&) = delete;  // move assignment

    protected:
        bool getIPv4SocketAddress(const std::string& hst, const std::string& prt, sockaddr_in& sa);

    private:
        bool connectNetwork() override;

        sockaddr_in* getReceptionAddress() override {return nullptr;}
        socklen_t* getReceptionAddressLength() override {return nullptr;}
    };
}  // namespace OSEF

#endif /* OSEFUDPCLIENT_H */
