#ifndef OSEFSOCKETTOOLBOX_H
#define OSEFSOCKETTOOLBOX_H

#include <sys/socket.h>  // sockaddr
#include <sys/un.h>  // sockaddr_un
#include <string>

namespace OSEF
{
    class SocketToolbox
    {
    public:
        SocketToolbox() = default;
        virtual ~SocketToolbox() = default;

        static bool getHostStreamSocket(const std::string& name, int32_t& sock, sockaddr_un& address);

        static bool getHostStreamSocket(const std::string& host, const std::string& port, int32_t& sock, sockaddr& address);

        static void closeSocket(int32_t& sock, const std::string& name);  ///< close socket and unlink name
        static void closeSocket(int32_t& sock);  ///< close socket and set it to -1 if successful
        static void shutdownSocket(int32_t& sock);  ///< shutdown socket

        static bool setOption(const int32_t& sock, const int32_t& level, const int32_t& optName);  ///< set socket option to one
        static bool resetOption(const int32_t& sock, const int32_t& level, const int32_t& optName);  ///< set socket option to zero

        static bool setSocketOption(const int32_t& sock, const int32_t& optName);  ///< set socket option to one
        static bool setTcpOption(const int32_t& sock, const int32_t& optName);  ///< set TCP option to one

        SocketToolbox(const SocketToolbox&) = delete;  // copy constructor
        SocketToolbox& operator=(const SocketToolbox&) = delete;  // copy assignment
        SocketToolbox(SocketToolbox&&) = delete;  // move constructor
        SocketToolbox& operator=(SocketToolbox&&) = delete;  // move assignment
    };
}  // namespace OSEF

#endif /* OSEFSOCKETTOOLBOX_H */
