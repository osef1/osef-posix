#include "TcpClient.h"
#include "SocketToolbox.h"
#include "SocketTypes.h"  // OSEF_TCP_NODELAY
#include "Debug.h"

OSEF::TcpClient::TcpClient(const std::string& hst, const std::string& prt)
    :StreamEndpoint(-1), host(hst), port(prt) {}

OSEF::TcpClient::~TcpClient()
{
    DOUT("Stream client shutting down connection to " << host);
}

bool OSEF::TcpClient::connectNetwork()
{
    bool ret = false;

    if (getSocket() <= 0)
    {
        sockaddr address{};

        // get client socket to host:port then
        // set option for immediate delivery of small TCP messages
        if (SocketToolbox::getHostStreamSocket(host, port, getSocket(), address)
            && (SocketToolbox::setTcpOption(getSocket(), OSEF_TCP_NODELAY)))
        {
            // connect socket to host
            if (connect(getSocket(), &address, OSEF_SOCKADDR_IN_LEN) == 0)
            {
                ret = true;
                DOUT("TCP client connected to " << host << ":" << port);
            }
            else
            {
                DOUT("TCP client could not connect to " << host << ":" << port);
            }
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}
