#ifndef OSEFUNIXSERVER_H
#define OSEFUNIXSERVER_H

#include "StreamEndpoint.h"

namespace OSEF
{
    class UnixServer : public StreamEndpoint
    {
    public:
        explicit UnixServer(const int32_t& sock);
        ~UnixServer() override = default;

        UnixServer(const UnixServer&) = delete;  // copy constructor
        UnixServer& operator=(const UnixServer&) = delete;  // copy assignment
        UnixServer(UnixServer&&) = delete;  // move constructor
        UnixServer& operator=(UnixServer&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;
    };
}  // namespace OSEF

#endif /* OSEFUNIXSERVER_H */
