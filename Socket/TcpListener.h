#ifndef OSEFTCPLISTENER_H
#define OSEFTCPLISTENER_H

#include "SocketTypes.h"  // IPv4AddressString

#include <string>

namespace OSEF
{
    class TcpListener
    {
    public:
        TcpListener(const std::string& hst, const std::string& prt);
        virtual ~TcpListener();

        bool acceptClient(int32_t& clientSocket);

        TcpListener(const TcpListener&) = delete;  // copy constructor
        TcpListener& operator=(const TcpListener&) = delete;  // copy assignment
        TcpListener(TcpListener&&) = delete;  // move constructor
        TcpListener& operator=(TcpListener&&) = delete;  // move assignment

    private:
        bool setUpListener();

        bool acceptStreamSocket(int32_t& clientSocket, std::string& clientAddress) const;

        const int32_t OSEF_LISTEN_MAX_CLIENT = 1;  // OSEF_LISTEN_MAX_CLIENT connections requests will be queued before further requests are refused

        std::string host;
        std::string port;
        int32_t listenSocket;
    };
}  // namespace OSEF

#endif /* OSEFTCPLISTENER_H */
