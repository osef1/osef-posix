#ifndef OSEFUNIXCLIENT_H
#define OSEFUNIXCLIENT_H

#include "StreamEndpoint.h"

#include <string>

namespace OSEF
{
    class UnixClient : public StreamEndpoint
    {
    public:
        explicit UnixClient(const std::string& nm);
        ~UnixClient() override;

        UnixClient(const UnixClient&) = delete;  // copy constructor
        UnixClient& operator=(const UnixClient&) = delete;  // copy assignment
        UnixClient(UnixClient&&) = delete;  // move constructor
        UnixClient& operator=(UnixClient&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;

        std::string name;
    };
}  // namespace OSEF

#endif /* OSEFUNIXCLIENT_H */
