#include "TcpListenerThread.h"
#include "TcpServer.h"
#include "SocketToolbox.h"
#include "Debug.h"

OSEF::TcpListenerThread::TcpListenerThread(const std::string& hst, const std::string& prt, const size_t& threads, TcpApplication* app)
    :listener(hst, prt),
    threader(threads),
    application(app) {}

bool OSEF::TcpListenerThread::spawnListenRoutine()
{
    const bool ret = threader.spawn(listenRoutine, &listener, &threader, application);
    return ret;
}

void* OSEF::TcpListenerThread::listenRoutine(TcpListener* lstnr, Threader* thrdr, TcpApplication* app)
{
    if (lstnr != nullptr)
    {
        if (thrdr != nullptr)
        {
            int32_t clientSocket = -1;
            do
            {
                if (lstnr->acceptClient(clientSocket))
                {
                    if (not thrdr->spawn(serveRoutine, clientSocket, app))
                    {
                        OSEF::SocketToolbox::closeSocket(clientSocket);
                    }
                }
            }while (clientSocket != -1);
        }
        else
        {
            DERR("threader null");
        }
    }
    else
    {
        DERR("listener null");
    }

    return nullptr;
}

void OSEF::TcpListenerThread::serveRoutine(int32_t clientSocket, TcpApplication* app)
{
    if (app != nullptr)
    {
        TcpServer server(clientSocket);
        if (not app->serve(server))
        {
            DERR("error serving socket " << clientSocket);
        }
    }
    else
    {
        DERR("application null");
    }
}
