#ifndef OSEFSOCKETTYPES_H
#define OSEFSOCKETTYPES_H

#include <sys/socket.h>  // socklen_t sa_family_t
#include <linux/if_packet.h>  // sockaddr_ll
#include <netinet/in.h>  // sockaddr_in

namespace OSEF
{
    // socket.h
    // Protocol families.
    const sa_family_t OSEF_AF_INET = 2U;  // #define PF_INET 2 /* IP protocol family.  */
    const uint8_t OSEF_PACKET_OTHERHOST = 3U;  // #define PACKET_OTHERHOST 3 /* To someone else */
    const int32_t OSEF_AF_PACKET = 17;  // #define PF_PACKET 17 /* Packet family.  */

    // Packet types
    const int32_t OSEF_SOL_SOCKET = 1;  // #define SOL_SOCKET 1

    // For setsockopt(2)
    const int32_t OSEF_SO_REUSEADDR = 2;  // #define SO_REUSEADDR 2
    const int32_t OSEF_SO_RCVTIMEO = 20;  // #define SO_RCVTIMEO 20
    const int32_t OSEF_SO_SNDTIMEO = 21;  // #define SO_SNDTIMEO 21
    const int32_t OSEF_SO_BINDTODEVICE = 25;  // #define SO_BINDTODEVICE 25

    // SHUT_RDWR
    const int32_t OSEF_SHUT_RDWR = 2;  // enum{SHUT_RD = 0, SHUT_WR, SHUT_RDWR};

    // socket_type.h
    // Types of sockets.
    const int32_t OSEF_SOCK_STREAM = 1;  // SOCK_STREAM = 1 /* Sequenced, reliable, connection-based byte streams.  */
    const int32_t OSEF_SOCK_DGRAM = 2;  // SOCK_DGRAM = 2 /* Connectionless, unreliable datagrams of fixed maximum length. */
    const int32_t OSEF_SOCK_RAW = 3;  // SOCK_RAW = 3 /* Raw protocol interface. */

    // linux/sockios.h
    // Socket configuration controls.
    const uint64_t OSEF_SIOCGIFFLAGS = 0x8913U;  // #define SIOCGIFFLAGS 0x8913 /* get flags */
    const uint64_t OSEF_SIOCSIFFLAGS = 0x8914U;  // #define SIOCSIFFLAGS 0x8914 /* set flags */
    const uint64_t OSEF_SIOCGIFADDR =  0x8915U;  // #define SIOCGIFADDR 0x8915 /* get PA address */
    const uint64_t OSEF_SIOCGIFHWADDR = 0x8927U;  // #define SIOCGIFHWADDR 0x8927 /* Get hardware address */
    const uint64_t OSEF_SIOCGIFINDEX = 0x8933U;  // #define SIOCGIFINDEX 0x8933 /* name -> if_index mapping */

    // in.h
    // Standard well-defined IP protocols.
    const int32_t OSEF_IPPROTO_TCP = 6;  // IPPROTO_TCP = 6 /* Transmission Control Protocol.  */
    const int32_t OSEF_IPPROTO_RAW = 255;  // IPPROTO_RAW = 255 /* Raw IP packets.  */

    // tcp.h
    // User-settable options (used with setsockopt).
    const int32_t OSEF_TCP_NODELAY = 1;  // #define TCP_NODELAY 1  /* Don't delay send to coalesce packets  */

    // if.h
    // define socklen_t before size_t as it is smaller ...
    const socklen_t OSEF_IF_NAME_LEN = 16U;  // #define IF_NAMESIZE 16
    // Length of interface name.
    const size_t OSEF_IF_NAMESIZE = OSEF_IF_NAME_LEN;  // #define IF_NAMESIZE 16

    // IFF_PROMISC
    const uint16_t OSEF_IFF_PROMISC = 0x100U;  // IFF_PROMISC = 0x100 /* Receive all packets.  */

    // linux/if_ther.h
    // IEEE 802.3 Ethernet magic constants.  The frame sizes omit the preamble and FCS/CRC (frame check sequence).
    const size_t OSEF_ETH_ALEN = 6U;  // #define ETH_ALEN 6 /* Octets in one ethernet addr */
    const size_t OSEF_ETH_HLEN = 14U;  // #define ETH_HLEN 14 /* Total octets in header. */
    const size_t OSEF_ETH_ZLEN = 60U;  // #define ETH_ZLEN 60 /* Min. octets in frame sans FCS */
    const size_t OSEF_ETH_FRAME_LEN = 1514U;  // #define ETH_FRAME_LEN 1514 /* Max. octets in frame sans FCS */

    // These are the defined Ethernet Protocol ID's.
    const uint16_t OSEF_ETH_P_IP = 0x0800U;  // #define ETH_P_IP 0x0800 /* Internet Protocol packet */

    // un.h
    const size_t SUN_PATH_MAX_SIZE = 108U;  // from sockaddr_un

    // netdb.h
    const socklen_t OSEF_NI_MAX_HOST = 1025U;  // #  define NI_MAXHOST      1025

    const socklen_t OSEF_SOCKADDR_LL_LEN = sizeof(sockaddr_ll);
    const socklen_t OSEF_SOCKADDR_IN_LEN = sizeof(sockaddr_in);

    const size_t OSEF_SOCK_NAME_LEN = 108U;  // sun_path maximum size
}  // namespace OSEF

#endif  // OSEFSOCKETTYPES_H
