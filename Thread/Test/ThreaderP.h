#ifndef OSEFTHREADERP_H
#define OSEFTHREADERP_H

#include "gtest/gtest.h"

namespace OSEF
{
    typedef struct
    {
        uint32_t maxThreads;
    }ThreaderParam;

    class ThreaderP : public ::testing::TestWithParam<ThreaderParam>
    {
        protected:
            ThreaderP() {}
            ~ThreaderP() override {}
        public:
            void SetUp() override {}
            void TearDown() override {}
    };
}  // namespace OSEF

#endif  // OSEFTHREADERP_H
