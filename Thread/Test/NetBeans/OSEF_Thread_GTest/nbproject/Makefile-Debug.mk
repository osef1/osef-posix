#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/3c163524/gtest-all.o \
	${OBJECTDIR}/_ext/29dd86f/ThreaderP.o \
	${OBJECTDIR}/_ext/29dd86f/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../../../NetBeans/OSEF_POSIX/dist/Debug/GNU-Linux/libosef_posix.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_thread_gtest

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_thread_gtest: ../../../../NetBeans/OSEF_POSIX/dist/Debug/GNU-Linux/libosef_posix.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_thread_gtest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_thread_gtest ${OBJECTFILES} ${LDLIBSOPTIONS} -lpthread -lrt

${OBJECTDIR}/_ext/3c163524/gtest-all.o: ../../../../googletest/googletest/src/gtest-all.cc
	${MKDIR} -p ${OBJECTDIR}/_ext/3c163524
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../MsgQ -I../../../../osef-log/Debug -I../.. -I../../../../Time -I../../../../Random -I../../../../Mutex -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3c163524/gtest-all.o ../../../../googletest/googletest/src/gtest-all.cc

${OBJECTDIR}/_ext/29dd86f/ThreaderP.o: ../../ThreaderP.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../MsgQ -I../../../../osef-log/Debug -I../.. -I../../../../Time -I../../../../Random -I../../../../Mutex -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/ThreaderP.o ../../ThreaderP.cpp

${OBJECTDIR}/_ext/29dd86f/main.o: ../../main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../../../googletest/googletest/include -I../../../../googletest/googletest -I../../.. -I../../../../MsgQ -I../../../../osef-log/Debug -I../.. -I../../../../Time -I../../../../Random -I../../../../Mutex -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/main.o ../../main.cpp

# Subprojects
.build-subprojects:
	cd ../../../../NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../../../../NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
