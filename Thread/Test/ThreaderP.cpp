#include "ThreaderP.h"
#include "Threader.h"
#include "Mutex.h"
#include "TimeOut.h"
#include "Debug.h"

namespace OSEF
{
    INSTANTIATE_TEST_SUITE_P(Dual,
                             ThreaderP,
                             testing::Values(ThreaderParam({2})));

    INSTANTIATE_TEST_SUITE_P(MaxCapacity,
                             ThreaderP,
                             testing::Values(ThreaderParam({10})));

    TEST_P(ThreaderP, doNothing)
    {
        OSEF::Threader threader(GetParam().maxThreads);
        EXPECT_EQ(threader.getOccupation(), 0U);
    }

    void testRoutine(OSEF::Mutex& mutex, uint32_t& ini, timespec timeout, uint32_t& fin)
    {
        if (mutex.lock())
        {
            ini++;
            if (not mutex.unlock())
            {
                EXPECT_TRUE(false);
            }
        }

        OSEF::sleep(timeout);

        if (mutex.lock())
        {
            fin++;
            if (not mutex.unlock())
            {
                EXPECT_TRUE(false);
            }
        }
    }

    void waitSharedCounter(uint32_t& counter, OSEF::Mutex& mutex)
    {
        bool counterPositive = false;
        do  // wait threads to start
        {
            OSEF::sleep(OSEF::T10ms);
            if (mutex.lock())
            {
                if (counter >= 1U)
                {
                    counterPositive = true;
                }

                if (not mutex.unlock())
                {
                    EXPECT_TRUE(false);
                }
            }
        }while (not counterPositive);
    }

    TEST_P(ThreaderP, freeSpawn)
    {
        OSEF::Threader threader(GetParam().maxThreads);

        OSEF::Mutex mutex;
        uint32_t ini = 0;
        timespec timeout = OSEF::T0s;
        uint32_t fin = 0;

        EXPECT_TRUE(OSEF::Threader::freeSpawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        waitSharedCounter(ini, mutex);

        waitSharedCounter(fin, mutex);

        EXPECT_EQ(ini, 1U);
        EXPECT_EQ(fin, 1U);
    }

    TEST_P(ThreaderP, spawn)
    {
        OSEF::Threader threader(GetParam().maxThreads);

        OSEF::Mutex mutex;
        uint32_t ini = 0;
        timespec timeout = OSEF::T0s;
        uint32_t fin = 0;

        EXPECT_TRUE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        EXPECT_EQ(threader.getOccupation(), 1U);

        do  // wait thread to complete
        {
            OSEF::sleep(OSEF::T10ms);
        }while (threader.getOccupation() > 0);

        EXPECT_EQ(ini, 1U);
        EXPECT_EQ(fin, 1U);
    }

    TEST_P(ThreaderP, respawn)
    {
        OSEF::Threader threader(GetParam().maxThreads);

        OSEF::Mutex mutex;
        uint32_t ini = 0;
        timespec timeout = OSEF::T0s;
        uint32_t fin = 0;

        EXPECT_TRUE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        EXPECT_EQ(threader.getOccupation(), 1U);

        do  // wait thread to complete
        {
            OSEF::sleep(OSEF::T10ms);
        }while (threader.getOccupation() > 0);

        EXPECT_EQ(ini, 1U);
        EXPECT_EQ(fin, 1U);

        EXPECT_TRUE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        EXPECT_EQ(threader.getOccupation(), 1U);

        do  // wait thread to complete
        {
            OSEF::sleep(OSEF::T10ms);
        }while (threader.getOccupation() > 0);

        EXPECT_EQ(ini, 2U);
        EXPECT_EQ(fin, 2U);
    }

    TEST_P(ThreaderP, spawnFullCapacity)
    {
        OSEF::Threader threader(GetParam().maxThreads);

        OSEF::Mutex mutex;
        uint32_t ini = 0;
        // give some time to reach full capacity
        timespec timeout = OSEF::T50ms;
        uint32_t fin = 0;

        for (size_t i = 0; i < GetParam().maxThreads; i++)
        {
            EXPECT_TRUE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));
        }

        EXPECT_FALSE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        EXPECT_EQ(threader.getOccupation(), GetParam().maxThreads);

        do  // wait threads to complete
        {
            OSEF::sleep(OSEF::T10ms);
        }while (threader.getOccupation() > 0);

        EXPECT_EQ(ini, GetParam().maxThreads);
        EXPECT_EQ(fin, GetParam().maxThreads);
    }

    TEST_P(ThreaderP, cancel)
    {
        OSEF::Threader threader(GetParam().maxThreads);

        OSEF::Mutex mutex;
        uint32_t ini = 0;
        // give some time to cancel thread
        timespec timeout = OSEF::T1s;
        uint32_t fin = 0;

        EXPECT_TRUE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        EXPECT_EQ(threader.getOccupation(), 1U);

        bool iniOk = false;
        do  // wait thread to start
        {
            OSEF::sleep(OSEF::T10ms);
            if (mutex.lock())
            {
                if (ini == 1U)
                {
                    iniOk = true;
                }

                if (not mutex.unlock())
                {
                    EXPECT_TRUE(false);
                }
            }
        }while (not iniOk);

        EXPECT_EQ(ini, 1U);
        EXPECT_EQ(fin, 0U);
    }

    TEST_P(ThreaderP, cancelFullCapacity)
    {
        OSEF::Threader threader(GetParam().maxThreads);

        OSEF::Mutex mutex;
        uint32_t ini = 0;
        // give some time to cancel
        timespec timeout = OSEF::T1s;
        uint32_t fin = 0;

        for (size_t i = 0; i < GetParam().maxThreads; i++)
        {
            EXPECT_TRUE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));
        }

        EXPECT_FALSE(threader.spawn(testRoutine, std::ref(mutex), std::ref(ini), timeout, std::ref(fin)));

        EXPECT_EQ(threader.getOccupation(), GetParam().maxThreads);

        bool iniOk = false;
        do  // wait threads to start
        {
            OSEF::sleep(OSEF::T10ms);
            if (mutex.lock())
            {
                if (ini == GetParam().maxThreads)
                {
                    iniOk = true;
                }

                if (not mutex.unlock())
                {
                    EXPECT_TRUE(false);
                }
            }
        }while (not iniOk);

        EXPECT_EQ(ini, GetParam().maxThreads);
        EXPECT_EQ(fin, 0U);
    }
}  // namespace OSEF
