#ifndef OSEFTHREADER_H
#define OSEFTHREADER_H

#include "MsgQNonBlockingTypeSender.h"
#include "MsgQNonBlockingTypeReceiver.h"
#include "Debug.h"

#include <thread>
#include <list>
#include <string>

namespace OSEF
{
    struct ThreadSubMsg
    {
        std::thread::id monitorId;
        std::thread* user{nullptr};
    };

    using ThreadSubSender = MsgQNonBlockingTypeSender<ThreadSubMsg>;
    using ThreadSubReceiver = MsgQNonBlockingTypeReceiver<ThreadSubMsg>;

    using Thread_Container = std::list<std::thread>;

    class Threader
    {
    public:
        explicit Threader(const size_t& max);
        virtual ~Threader();

        template<typename Routine, typename... Args>
        bool spawn(Routine&& routine, Args&&... args);

        template<typename Routine, typename... Args>
        static bool freeSpawn(Routine&& routine, Args&&... args);

        size_t getOccupation();

        Threader(const Threader&) = delete;  // copy constructor
        Threader& operator=(const Threader&) = delete;  // copy assignment
        Threader(Threader&&) = delete;  // move constructor
        Threader& operator=(Threader&&) = delete;  // move assignment

    private:
        static void monitorRoutine(std::thread& user, const std::string& unsubscribeMsgQName);

        void checkUnsubscription();
        void unsubscribe(const ThreadSubMsg& msg);

        static void cancel(std::thread& thread);

        Thread_Container users;  ///< user threads
        Thread_Container monitors;  ///< monitor threads
        size_t capacity;  ///< maximum simultaneous thread subscriptions
        ThreadSubReceiver unsubscribeMsgQ;  ///< automatic thread unsubscription message queue
    };
}  // namespace OSEF

template<typename Routine, typename... Args>
bool OSEF::Threader::freeSpawn(Routine&& routine, Args&&... args)
{
    bool ret = false;

    std::thread t(routine, args...);
    if (t.native_handle() != std::thread().native_handle())
    {
        try
        {
            t.detach();
            ret = true;
        }
        catch (const std::system_error& e)
        {
            DERR("exception detaching user thread " << e.what() << " (" << e.code() << ")");
        }
    }
    else
    {
        DERR("error creating thread");
    }

    return ret;
}

template<typename Routine, typename... Args>
bool OSEF::Threader::spawn(Routine&& routine, Args&&... args)
{
    bool ret = false;

    // release potential slots before reserving new one
    checkUnsubscription();

    if (users.size() < capacity)
    {
        std::string msgQName;
        if (unsubscribeMsgQ.getName(msgQName))
        {
            auto user = users.insert(users.end(), std::thread(routine, args...));
            if (user->native_handle() != std::thread().native_handle())
            {
                auto monitor = monitors.insert(monitors.end(), std::thread(Threader::monitorRoutine, std::ref(*user), msgQName));
                if (monitor->native_handle() != std::thread().native_handle())
                {
                    ret = true;
                }
                else
                {
                    DERR("error creating monitor thread");
                    monitors.erase(monitor);
                    cancel(*user);
                    users.erase(user);
                }
            }
            else
            {
                DERR("error creating user thread");
                users.erase(user);
            }
        }
        else
        {
            DERR("error getting unsubscribe message queue name");
        }
    }
    else
    {
        DWARN("warning maximum threads capacity reached");
    }

    return ret;
}

#endif /* OSEFTHREADER_H */
