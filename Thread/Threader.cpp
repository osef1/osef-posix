#include "Threader.h"

OSEF::Threader::Threader(const size_t& max)
    :capacity(max),
    unsubscribeMsgQ(max) {}

OSEF::Threader::~Threader()
{
    while (not monitors.empty() && not users.empty())
    {
        checkUnsubscription();  // check already unsubscribed threads

        auto m = monitors.begin();
        if (m != monitors.end())
        {
            // avoids monitors to cancel themselves and fail to send unsubscribe message
            cancel(*m);
            monitors.pop_front();
        }

        auto u = users.begin();
        if (u != users.end())
        {
            cancel(*u);
            users.pop_front();
        }
    }
}

size_t OSEF::Threader::getOccupation()
{
    checkUnsubscription();
    const size_t ret = users.size();
    return ret;
}

void OSEF::Threader::monitorRoutine(std::thread& user, const std::string& unsubscribeMsgQName)
{
    try
    {
        user.join();
    }
    catch (const std::system_error& e)
    {
        DERR("exception joining user thread " << e.what() << " (" << e.code() << ")");
    }

    ThreadSubSender sender(unsubscribeMsgQName);

    ThreadSubMsg msg;
    msg.monitorId = std::this_thread::get_id();
    msg.user = &user;

    if (not sender.sendMsg(msg))  // unsubscribe from thread handler
    {
        DERR("error unsubscribing user's " << user.native_handle());
    }
}

void OSEF::Threader::checkUnsubscription()
{
    ThreadSubMsg msg;
    while (unsubscribeMsgQ.receiveMsg(msg))
    {
        unsubscribe(msg);
    }
}

void OSEF::Threader::unsubscribe(const ThreadSubMsg& msg)
{
    auto m = monitors.begin();
    while (m != monitors.end())
    {
        if (m->get_id() == msg.monitorId)
        {
            //  monitor must be joined before erasing it
            try
            {
                m->join();
            }
            catch (const std::system_error& e)
            {
                DERR("exception joining monitor thread " << e.what() << " (" << e.code() << ")");
            }

            m = monitors.erase(m);
        }
        else
        {
            ++m;
        }
    }

    auto u = users.begin();
    while (u != users.end())
    {
        if (&(*u) == msg.user)
        {
            //  user thread was already joined by monitor
            u = users.erase(u);
        }
        else
        {
            ++u;
        }
    }
}

void OSEF::Threader::cancel(std::thread& thread)
{
    const int32_t cancelret = pthread_cancel(thread.native_handle());
    if (cancelret == 0)
    {
        try
        {
            thread.join();
        }
        catch (const std::system_error& e)
        {
            DERR("exception joining cancelled thread " << e.what() << " (" << e.code() << ")");
        }
    }
    else
    {
        DERR("error requesting thread " << thread.native_handle() << " cancellation (" << cancelret << ": " << strerror(cancelret) << ")");
    }
}
