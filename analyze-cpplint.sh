#!/bin/bash

wget https://gitlab.com/osef1/osef-ref/-/raw/master/cpplint/swag-cpplint.sh?inline=false

analyze-cpplint.sh "$@"

rm analyze-cpplint.sh
