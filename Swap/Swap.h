#ifndef OSEFSWAP_H
#define OSEFSWAP_H

#include <cstdint>

namespace OSEF
{
    static inline uint16_t swap16(uint16_t x)
    {
        uint16_t ret =  static_cast<uint16_t>(static_cast<uint16_t>(x & static_cast<uint16_t>(0x00ff)) << 8U) |
                        static_cast<uint16_t>(static_cast<uint16_t>(x & static_cast<uint16_t>(0xff00)) >> 8U);
        return ret;
    }

    static inline uint32_t swap32(uint32_t x)
    {
        uint16_t ret =  static_cast<uint32_t>((x & static_cast<uint32_t>(0xff000000)) >> 24U) |
                        static_cast<uint32_t>((x & static_cast<uint32_t>(0x00ff0000)) >>  8U) |
                        static_cast<uint32_t>((x & static_cast<uint32_t>(0x0000ff00)) <<  8U) |
                        static_cast<uint32_t>((x & static_cast<uint32_t>(0x000000ff)) << 24U);
        return ret;
    }

    static inline uint64_t swap64(uint64_t x)
    {
        uint16_t ret =  static_cast<uint64_t>((x & static_cast<uint64_t>(0xff00000000000000)) >> static_cast<uint64_t>(56)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x00ff000000000000)) >> static_cast<uint64_t>(40)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x0000ff0000000000)) >> static_cast<uint64_t>(24)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x000000ff00000000)) >> static_cast<uint64_t>(8)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x00000000ff000000)) << static_cast<uint64_t>(8)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x0000000000ff0000)) << static_cast<uint64_t>(24)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x000000000000ff00)) << static_cast<uint64_t>(40)) |
                        static_cast<uint64_t>((x & static_cast<uint64_t>(0x00000000000000ff)) << static_cast<uint64_t>(56));
        return ret;
    }

    static inline uint16_t hton16(const uint16_t& s)
    {
        # if __BYTE_ORDER == __BIG_ENDIAN
            return s;
        # else
        #  if __BYTE_ORDER == __LITTLE_ENDIAN
            return swap16(s);
        #endif
        #endif
    }

    static inline uint32_t hton32(const uint32_t& s)
    {
        # if __BYTE_ORDER == __BIG_ENDIAN
            return s;
        # else
        #  if __BYTE_ORDER == __LITTLE_ENDIAN
            return swap32(s);
        #endif
        #endif
    }

    static inline uint64_t hton64(const uint64_t& s)
    {
        # if __BYTE_ORDER == __BIG_ENDIAN
            return s;
        # else
        #  if __BYTE_ORDER == __LITTLE_ENDIAN
            return swap64(s);
        #endif
        #endif
    }
}  // namespace OSEF

#endif /* OSEFSWAP_H */
