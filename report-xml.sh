#!/bin/bash

cd build/release

r="-r ../../../../File/Test/OSEF_File_UT_read"
o="-o ../../../../File/Test/OSEF_File_UT_overwrite"
a="-a ../../../../File/Test/OSEF_File_UT_append"

cd File/Test
./osef-file-gtest --gtest_output="xml:osef-file-gtest-report.xml" $r $o $a

cd ../../Mutex/Test
./osef-mutex-gtest --gtest_output="xml:osef-mutex-gtest-report.xml"

cd ../../Socket/Test
./osef-socket-gtest --gtest_output="xml:osef-socket-gtest-report.xml"

cd ../Thread/Test
./osef-thread-gtest --gtest_output="xml:osef-thread-gtest-report.xml"

cd ../MsgQ/Test
./osef-msgq-gtest --gtest_output="xml:osef-msgq-gtest-report.xml"

cd ../Random/Test
./osef-random-gtest --gtest_output="xml:osef-random-gtest-report.xml"

cd ../../Time/Test
./osef-time-gtest --gtest_output="xml:osef-time-gtest-report.xml"
